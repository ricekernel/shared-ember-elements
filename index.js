/* jshint node: true */
'use strict';

module.exports = {
  name: 'shared-ember-elements',

  addonPath: function(path) {
      if (!this.isDevelopingAddon()) {
        path = ['node_modules', this.name, path].join('/');
      }
      return path;
    },

  included: function(app) {

    app.import('vendor/modernizr/modernizr-custom.min.js');
    //app.import(app.bowerDirectory + '/blueimp-md5/js/md5.js');

    var emberInlineSvgOptions = app.options.svg || {};
    if (!emberInlineSvgOptions.paths) {
      emberInlineSvgOptions.paths = ['public']; // TODO: use ember-inline-svg's defaults directly: https://github.com/minutebase/ember-inline-svg/pull/19
    }
    var ourPublicPath = this.addonPath('public');
    if (emberInlineSvgOptions.paths.indexOf(ourPublicPath) === -1) {
      emberInlineSvgOptions.paths.push(ourPublicPath);
    }
    app.options.svg = emberInlineSvgOptions;

    this._super.included(app);

    this.addons.forEach(function(addon){
      if (['ember-inline-svg'].indexOf(addon.name) > -1) {
        addon.included.apply(addon, [app]);
      }
    });

  },

};
