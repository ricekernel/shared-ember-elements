`import Ember from 'ember'`
`import ENV from '../../config/environment'`
#A mixin to include on any input or button that has a help message.
AwsDocumentMixin = Ember.Mixin.create(

  session: Ember.inject.service('session')

  viewDocument: ->
    modelPath = @serialize().data.type
    signedURLEndPoint = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{modelPath}/" + @get('id') + '/generate_location'
    xhttp = new XMLHttpRequest

    notify = @get('notify')

    @get('session').authorize('authorizer:oauth2', (header,token)->
      xhttp.open("GET", signedURLEndPoint, true)
      xhttp.setRequestHeader('authorization', token)
      xhttp.onreadystatechange = ->
        if xhttp.readyState == 4 and xhttp.status == 200
          responseJSON = JSON.parse(xhttp.responseText)
          presignedURL = responseJSON.presigned_url
          open = window.open(presignedURL)
          if open == null or typeof(open) == 'undefined'
            alert "Your browser is currently blocking popups. You'll need to turn off your pop-up blocker to view this document"
        else
          if xhttp.status > 399
            notify.error 'We are unable to access the document at this time. It could be gremlins..'

      xhttp.send()
    )
)


`export default AwsDocumentMixin`
