`import Ember from 'ember'`
`import DS from 'ember-data'`

ModelFinancialForexMixin = Ember.Mixin.create(


  #TODO: rename this as now it has the override save
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')
  forex: Ember.computed.alias('session.forex')

  valueIsNzd: DS.attr('boolean', {defaultValue: true})
  rate: DS.attr('number')
  currency: DS.belongsTo 'currency'


  ready: ->
    self = @
    unless @get 'valueIsNzd'
      self.set 'forex', true
    @_super(arguments...)

  #computeds
  nzdVal: (->
    unless @get('valueIsNzd')
      (Math.round(@get('numberOfInterest') * @get('rate')*100))/100
    else
      @get('numberOfInterest')
    ).property 'numberOfInterest', 'rate'


  #observers:
  currencyObserver: Ember.observer('currency', ->
    unless @get('isDeleted')
      Ember.run.once(this, 'conversion')
    )


  #functions
  conversion: ->
    if @get('currency')
      unless @get('currency.name') == 'NZD'
        rate = @get('currency.nzdBasedRate')
        @set('valueIsNzd', false)
        rate = (Math.round(rate*100))/100
        @set 'rate', rate

  ###
  didCreate: ->
    modelName = @get('constructor.modelName')
    notify = @get 'notify'
    notify.success "New #{modelName} saved!"

  didDelete: ->
    modelName = @get('constructor.modelName')
    capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
    notify = @get 'notify'
    notify.success "#{capitalisedModelName} deleted!"

  didUpdate: ->
    modelName = @get('constructor.modelName')
    capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
    notify = @get 'notify'
    notify.success "#{capitalisedModelName} updated!"
  ###


)


`export default ModelFinancialForexMixin`
