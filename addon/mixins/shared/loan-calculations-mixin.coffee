`import Ember from 'ember'`

LoanCalculationsMixin = Ember.Mixin.create(

  calculateLoanRepayment: (balance, term, rate, paymentsPerYear, isInterestOnly) ->
    #term should be in months
    unless balance > 0 and term > 0 and rate > 0 and paymentsPerYear > 0
      return 0

    term = (term / 12) * paymentsPerYear

    if isInterestOnly

      return (balance * rate) / paymentsPerYear

    else
      numerator = (rate/paymentsPerYear) * balance
      denominator = 1- Math.pow(1+(rate/paymentsPerYear), -term)
      pmt = numerator/denominator
      payment = (Math.ceil(pmt * 100))/100
      return payment

      numerator = (rate/frequency) * amount
      denominator = 1- Math.pow(1+(rate/12), -term)
      pmt = numerator/denominator
      payment = (Math.ceil(pmt * 100))/100
      return payment


  calculateCostToRepayForPeriod: (balance, term, rate, paymentsPerYear, isInterestOnly, fixedRateMonths, defaultRate, period) ->
    unless period
      period = term
    payment = @calculateLoanRepayment(balance, term, rate, 12, isInterestOnly)

    if fixedRateMonths
      firstPeriod = @utilAmortiseLoanForPeriod(balance, fixedRateMonths, rate, payment)
      totalCost = parseFloat(firstPeriod.cost)
      remainingPayment = @calculateLoanRepayment(firstPeriod.remaining, term - fixedRateMonths, defaultRate, 12, isInterestOnly)

      secondPeriod = @utilAmortiseLoanForPeriod(firstPeriod.remaining, term - fixedRateMonths, defaultRate, remainingPayment)
      totalCost += parseFloat(secondPeriod.cost)

    else
      totalCost = @utilAmortiseLoanForPeriod(balance, period, rate, payment).cost

    return totalCost



  calculateLoanLifeTimeCost: (balance, term, rate, paymentsPerYear, isInterestOnly, fixedRateMonths, defaultRate) ->
    payment = @calculateLoanRepayment(balance, term, rate, 12, isInterestOnly)

    if fixedRateMonths
      firstPeriod = @utilAmortiseLoanForPeriod(balance, fixedRateMonths, rate, payment)
      totalCost = firstPeriod.cost
      remainingPayment = @calculateLoanRepayment(firstPeriod.remaining, term - fixedRateMonths, defaultRate, 12, isInterestOnly)

      secondPeriod = @utilAmortiseLoanForPeriod(firstPeriod.remaining, term - fixedRateMonths, defaultRate, remainingPayment)
      totalCost += secondPeriod.cost

    else
      totalCost = @utilAmortiseLoanForPeriod(balance, term, rate, payment).cost
    return totalCost


  calculateLVR: (loanAmount, securities) ->
    if loanAmount == 0 or securities == 0
      return 0
    return (Math.floor((loanAmount/securities) * 10000))/10000 || 0


  calculateUMI: (income, expenses, repayments) ->
    return income - expenses - repayments



  utilAmortiseLoanForPeriod: (balance, period, rate, payment) ->
    i = 0
    totalCost = 0
    while i < period
      balance = balance - payment + ((balance) * (rate / 12))
      #balance = balance + (balance * (rate / 12)) - payment
      totalCost += payment
      i++
    return {remaining: balance, cost: totalCost}




  calculateRemainingTerm: (balance, rate, payment, paymentsPerYear) ->
    unless balance > 0 && rate > 0 && payment > 0 && paymentsPerYear > 0
      return 0

    monthlyRate = rate / 12
    monthlyPayment = payment * paymentsPerYear / 12

    numerator = Math.log(monthlyPayment) - Math.log(monthlyPayment - (balance * monthlyRate))
    denominator = Math.log(1 + monthlyRate)

    term = numerator / denominator
    return term


)


`export default LoanCalculationsMixin`
