`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


PricingOffer = DS.Model.extend(LoanCalculationsMixin,
  name: DS.attr('string')
  cashContribution: DS.attr('number') #the value of cash up front (deduct from loan bal for comparison)
  nonCashIncentives: DS.attr('number')
  recurringDiscounts: DS.attr('number')
  monthlyFees: DS.attr('number')

  requestedCashContribution: DS.attr('number')

  requestNotes: DS.attr('string') #the advisers notes to the bank


#for loan app:
  offerExpiryDate: DS.attr()
  status: DS.attr 'enum', {caps: 'first', defaultValue: 'Pending submission'}  # enum: ['pending submission', 'submitted', 'resubmitted', 'pending acceptance', 'accepted', 'expired', 'withdrawn']
  #bank: DS.belongsTo 'bank'
  approval: DS.belongsTo 'approval'


#relationships:
  bank: DS.belongsTo 'bank'
  #loanStructure: DS.belongsTo 'loanStructure'
  pricingOfferRates: DS.hasMany 'pricingOfferRates'#, {async: false}

#computeds:

  pricingLiabilities: Ember.computed.mapBy('pricingOfferRates', 'liability')




  ###
  lifetimeCost: Ember.computed('pricingOfferRates.@each.limit', 'pricingOfferRates.@each.liabilityType', 'pricingOfferRates.@each.fixedRateMonths', 'pricingOfferRates.@each.interestRate', 'loanStructure.content.remainingMonths', 'loanStructure.defaultRate', ->
    self = @
    totalCost = 0
    term = @get('loanStructure.content.remainingMonths')
    defaultRate = @get('loanStructure.defaultRate')
    @get('pricingOfferRates').forEach (rate) ->
      rateLiability = rate.get('liability')
      lifetimeCost = self.calculateLoanLifeTimeCost(rate.get('limit'), term, rate.get('interestRate'), 12, false, rate.get('fixedRateMonths'), defaultRate)
      totalCost += lifetimeCost
    return totalCost
    )

  adjLifeTimeCost: Ember.computed('pricingOfferRates.@each.limit', 'pricingOfferRates.@each.liabilityType', 'pricingOfferRates.@each.fixedRateMonths', 'pricingOfferRates.@each.interestRate', 'loanStructure.content.remainingMonths', 'loanStructure.defaultRate', 'cashContribution', 'nonCashIncentives', 'recurringDiscounts', ->
    self = @
    totalCost = 0
    term = @get('loanStructure.content.remainingMonths')
    defaultRate = @get('loanStructure.defaultRate')
    nonCashIncentives = @get('nonCashIncentives') || 0
    recurringDiscounts = @get('recurringDiscounts') || 0
    cashSpent = false
    @get('pricingOfferRates').forEach (rate) ->
      rateLiability = rate.get('liability')
      value = rate.get('limit')
      if self.get('cashContribution') and !cashSpent
        value = value - parseFloat(self.get('cashContribution'))
        cashSpent = true
      lifetimeCost = self.calculateLoanLifeTimeCost(value, term, rate.get('interestRate'), 12, false, rate.get('fixedRateMonths'), defaultRate)
      totalCost += lifetimeCost
    totalCost = totalCost - nonCashIncentives - (recurringDiscounts * (term / 12))
    return totalCost
    )

  adjMidTermCost: Ember.computed('loanStructure.content.defaultPricingComparisonMonths', 'pricingOfferRates.@each.limit', 'pricingOfferRates.@each.liabilityType', 'pricingOfferRates.@each.fixedRateMonths', 'pricingOfferRates.@each.interestRate', 'loanStructure.content.remainingMonths', 'loanStructure.defaultRate', 'cashContribution', 'nonCashIncentives', 'recurringDiscounts', ->
    self = @
    totalCost = 0
    term = @get('loanStructure.content.remainingMonths')
    midTermPeriod = @get('loanStructure.content.defaultPricingComparisonMonths')
    defaultRate = @get('loanStructure.defaultRate')
    nonCashIncentives = @get('nonCashIncentives') || 0
    recurringDiscounts = @get('recurringDiscounts') || 0
    cashSpent = false
    @get('pricingOfferRates').forEach (rate) ->
      rateLiability = rate.get('liability')
      value = rate.get('limit')
      if self.get('cashContribution') and !cashSpent
        value = value - parseFloat(self.get('cashContribution'))
        cashSpent = true
      lifetimeCost = self.calculateCostToRepayForPeriod(value, term, rate.get('interestRate'), 12, false, rate.get('fixedRateMonths'), defaultRate, midTermPeriod)
      totalCost += lifetimeCost
    totalCost = totalCost - nonCashIncentives - (recurringDiscounts * (term / 12))
    return totalCost
  )

  totalPayments: Ember.computed('loanStructure.content.remainingMonths', 'pricingOfferRates.@each.liabilityType', 'pricingOfferRates.@each.limit',  'pricingOfferRates.@each.interestRate', ->
    self = @
    payments = 0
    term = @get('loanStructure.content.remainingMonths')
    @get('pricingOfferRates').forEach (lia) ->
      isRevolvingCredit = false
      if lia.get('liabilityType.name') == 'Revolving credit'
        isRevolvingCredit = true
      payment = self.calculateLoanRepayment(lia.get('limit'), term, lia.get('interestRate'), 12, isRevolvingCredit)
      payments += payment
    return payments
    )

  missingRates: Ember.computed.gt('missingRateCount', 0)
  missingRateCount: Ember.computed('pricingOfferRates.@each.interestRate', ->
    missingRates = 0
    @get('pricingOfferRates').forEach (rate) ->
      unless rate.get('interestRate')
        missingRates++
    return missingRates
    )

  ###


  proposedLiabilities: Ember.computed.alias('loanStructure.content.liabilities')


  createDefaultSetOfRates: ->
    self = @
    ['Revolving credit', 'Floating home loan'].forEach (type) ->
      liaType = self.store.peekAll('liabilityType').findBy('name', type)
      newRate = self.store.createRecord('pricingOfferRate',
        liabilityType: liaType
        fixedRateMonths: 0
      )
      self.get('pricingOfferRates').pushObject(newRate)
    [6, 12, 18, 24, 36, 48, 60].forEach (fixedTerm) ->
      liaType = self.store.peekAll('liabilityType').findBy('name', 'Fixed home loan')
      newRate = self.store.createRecord('pricingOfferRate',
        liabilityType: liaType
        fixedRateMonths: fixedTerm
      )
      self.get('pricingOfferRates').pushObject(newRate)


  utilGetPricingDetails: (lia) ->
    self = @
    liabilityType = lia.get('liabilityType')
    fixedRateMonths = lia.get('fixedRateMonths')
    value = lia.get('limit')
    matchingRate = self.get('pricingOfferRates').filterBy('liabilityType', liabilityType).findBy('fixedRateMonths', fixedRateMonths)
    intRate = 0
    if matchingRate
      intRate = matchingRate.get('interestRate')
    return {value: value, rate: intRate, fixedRateMonths: fixedRateMonths}


  #function overrrides:
  deleteRecord: ->
    #have to push these into an array and then delete them as enumerators get fucked when destroying content
    self = @
    linkedRateIds = []
    @get('pricingOfferRates').forEach (rate) ->
      linkedRateIds.pushObject(rate.get('id'))
    linkedRateIds.forEach (id) ->
      rate = self.store.peekRecord('pricingOfferRate', id)
      rate.destroyRecord()
    @_super()




)

`export default PricingOffer`
