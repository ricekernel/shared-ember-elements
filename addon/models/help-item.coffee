`import DS from 'ember-data'`

HelpItem = DS.Model.extend
  app: DS.attr 'string'
  title: DS.attr 'string'
  description: DS.attr 'string'
  imagePath: DS.attr 'string'
  videoPath: DS.attr 'string'
  priority: DS.attr 'number'


  release: DS.belongsTo 'release'

`export default HelpItem`
