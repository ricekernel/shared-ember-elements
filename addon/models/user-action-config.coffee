`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`


UserActionConfig = DS.Model.extend(ModelNotifyServerActionsMixin,
  actionType: DS.belongsTo 'actionType'
  actionTrigger: DS.belongsTo 'actionTrigger'
  altModelName: 'programmed action'
  description: DS.attr 'string'
  isGlobal: DS.attr 'boolean'
  isAutomatic: DS.attr 'boolean'
  isActive: DS.attr 'boolean'
  sendTime: DS.attr 'date' #time of day to send auto messages
  surfaceLeadDays: DS.attr 'number', defaultValue: 1 #days ahead of event to surface the action
  executeLeadDays: DS.attr 'number', defaultValue: 0 #days ahead of event to automatically send message

  owner: DS.belongsTo 'user'
  template: DS.belongsTo 'template' #template to send


  status: Ember.computed('isActive', ->
    if @get('isActive')
      return 'active'
    else
      return 'inactive'
    )

  leadDaysObs: Ember.observer('surfaceLeadDays', 'executeLeadDays', ->
    if @get 'isAutomatic'
      surfaceLeadDays = @get('surfaceLeadDays')
      executeLeadDays = @get('executeLeadDays')
      if surfaceLeadDays < executeLeadDays
        alert "We can't send the message before we create the reminder. Please set the 'remind me' time to before the 'send message' time"
    )

  didUpdate: ->
    if @get('template.message')
      if @get('template.hasDirtyAttributes')
        @get('template').then (template) ->
          template.save()

  ###
  overrideSave: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      if self.get('executeLeadDays') > self.get('surfaceLeadDays')
        alert "The message cannot be sent before we create your reminder.  Please ensure the 'remind me' days is no less than the 'send message' days"
        resolve self
      else
        self.save().then (->
          resolve self
        ), (error) ->
          reject error
    )
    ###

)
`export default UserActionConfig`
