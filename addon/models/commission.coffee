`import DS from 'ember-data'`
#`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`

Commission = DS.Model.extend(#LoanCalculationsMixin,

  amountPaid: DS.attr('number') #how much was actually paid

  #these three set at create based on commissionRate
  flatFee: DS.attr 'number' #if a flat fee is paid
  upFrontRate: DS.attr 'number' #a decimal representing the upfront commission rate paid by the bank
  trailRate: DS.attr 'number' #if the bank pays commission as trail, the (trail rate)

  firmPaymentDate: DS.attr() #the date the firm was paid by the bank/aggregator
  userPaymentDate: DS.attr() #the date the firm paid the adviser

  ownerAmount: DS.attr('number') #the value of the commission due to the owner
  secondaryAmount: DS.attr('number') #the value of the commission due to the secondary owner (if there is one)
  firmAmount: DS.attr('number') #the value of the commission due to the firm

  state: DS.attr() #forecast (before settlement), due (after settlement), checked (user has checked against bank payment advice), paid (principal has paid)

  liability: DS.belongsTo 'liability'


  estimatedValue: Ember.computed('liability.commissionRate', 'liability.owing', ->
    if @get('commissionValue')
      return @get('commissionValue')
    if @get 'isTrail'
      lifeTimeValueOfTrail = @util_CalculateLifetimeTrail()
      return lifeTimeValueOfTrail
    else
      return parseFloat(@get('commissionRate')) * parseFloat(@get('loan.owing'))
    )


#functions:


  util_CalculateLifetimeTrail: ->
    loanValue = parseFloat(@get('loan.owing'))
    loanTerm = parseFloat(@get('loan.remainingMonths'))
    loanRate = parseFloat(@get('loan.interestRate'))
    pmt = @calculateLoanRepayment(loanValue, loanTerm, loanRate, 12)
    trailRate = parseFloat(@get('commissionRate'))

    if loanValue == 0 or loanTerm == 0 or loanRate == 0 or pmt == 0 or trailRate == 0
      return 0

    i = 0
    lifetimeTrail = 0
    while i < loanTerm
      lifetimeTrail += loanValue * trailRate
      loanValue = (loanValue * (1 + (loanRate/12))) - pmt
      i++
    if i == loanTerm
      return lifetimeTrail


)

`export default Commission`
