`import DS from 'ember-data'`

ActionType = DS.Model.extend
  name: DS.attr 'string'
  description: DS.attr 'string'

`export default ActionType`
