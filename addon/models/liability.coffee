`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import accounting from 'accounting'`


Liability = DS.Model.extend(LoanCalculationsMixin, ModelFinancialForexMixin,

  description: DS.attr('string')
  fixedRateExpiry: DS.attr()
  fixedRateMonths: DS.attr 'number', {defaultValue: 0}
  identifier: 'liability'
  interestOnly: DS.attr('boolean', {defaultValue: false})
  interestOnlyExpiry: DS.attr()
  interestOnlyMonths: DS.attr 'number', {defaultValue: 0}
  interestRate: DS.attr('number')
  limit: DS.attr('number')
  owing: DS.attr('number')
  rate: DS.attr('number')
  remainingMonths: DS.attr('number', defaultValue: 360)
  repayments: DS.attr('number')
  repaymentStartDate: DS.attr()
  status: DS.attr('string', {defaultValue: 'current'}) #:pending, :current, :expired, :paid
  toolId: DS.attr('number') #used to link to a tool in the toolkit

  totalFlatFeeCommission: DS.attr('number')
  totalTrailCommission: DS.attr('number')
  totalUpFrontCommission: DS.attr('number')


#relationships
  bank: DS.belongsTo 'bank', {async: false}
  clients: DS.hasMany 'clients'
  commissions: DS.hasMany 'commissions'
  commissionRate: DS.belongsTo 'commissionRate'
  currency: DS.belongsTo 'currency'
  frequency: DS.belongsTo 'frequency', {async: false}
  liabilityType: DS.belongsTo 'liabilityType', {async: false}
  loanApplications: DS.hasMany 'loanApplications'
  loanPartyLiabilityLink: DS.belongsTo 'loanPartyLiabilityLink', {async: false}
  loanStructure: DS.belongsTo 'loanStructure'
  settlement: DS.belongsTo 'settlement'
  pricingOfferRates: DS.hasMany 'pricingOfferRates'


#computeds
  limitAfterDrawDown: Ember.computed.alias('loanPartyLiabilityLink.limitAfterDrawDown')
  liabilitySettlementAction: Ember.computed.alias('loanPartyLiabilityLink.liabilitySettlementAction')

  monthlyPayments: Ember.computed('frequency', 'repayments', ->
    pmt = @get('repayments')
    return pmt * @get('frequency.yearConversion') / 12
    )

  hasLimit: Ember.computed('liabilityType', ->
    liabilityType = @get('liabilityType.name')
    if ['Credit card', 'Revolving credit', 'Store card', 'Overdraft'].includes(liabilityType)
      return true
      )


  type: Ember.computed.alias('liabilityType')#used by client.NewFinancial()

  name :Ember.computed('description', 'liabilityType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('description') && @get('liabilityType.name')
      return "#{@get('description')} (#{@get('liabilityType.name')})"
    if @get('liabilityType.name')
      return @get('liabilityType.name')
    else
      return 'Liability'
  )

  altModelName: Ember.computed.alias('name') #remove once rid of all references

  balanceLimitAfterDDPlaceholder: Ember.computed('noLimit', ->
    if @get 'noLimit'
      return 'balance after new loan'
    else
      return 'limit after new loan'
  )


  estimatedRepayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', 'frequency.yearConversion', ->
    unless @get('remainingMonths') and @get('frequency') and @get('interestRate') > 0
      return 0

    if @get('status') == 'pending'
      amount = parseFloat(@get('limit')) || 0
    else
      amount = parseFloat(@get('owing')) || 0
    term = @get('remainingMonths')
    rate = parseFloat(@get('interestRate')) || 0
    frequency = @get('frequency.yearConversion')

    if @get('interestOnlyMonths')
      return @calculateLoanRepayment(amount, term, rate, frequency, true)

    else
      return @calculateLoanRepayment(amount, term, rate, frequency, false)
    )


  isSecured: Ember.computed.alias('liabilityType.isSecured')


  noLimit: (->
    if @get('liabilityType')
      liabilityType = @get('liabilityType').get('name')
      switch liabilityType #done this as a switch rather than if, as may need more complexity later
        when 'Term home loan' then true
        when 'Fixed home loan' then true
        when 'Floating home loan' then true
        when 'Personal loan' then true
        when 'Student loan' then true
        when 'Hire purchase' then true
        when 'Other' then true
        else false
    else
      false
    ).property 'liabilityType'


  paymentIsCalculated: Ember.computed('liabilityType', 'interestOnly', ->
    if @get('interestOnly') || @get('liabilityType.name') == 'Revolving credit'
      return true
  )


  requiresFixedTermExpiry: (->
    return false unless @get('liabilityType')
    liabilityType = @get('liabilityType.name')
    switch liabilityType #done this as a switch rather than if, as may need more complexity later
      when 'Fixed home loan' then true
      else false
    ).property 'liabilityType'


  requiresInterestRate: Ember.computed('liabilityType', ->
    if @get('liabilityType.isSecured')
      return true
    if ['Personal loan', 'Other loan'].includes(@get('liabilityType.name'))
      return true
    )


  requiresTerm: Ember.computed('liabilityType', ->
    if ['Personal loan', 'Other loan', 'Floating home loan', 'Fixed home loan'].includes(@get('liabilityType.name'))
      return true
    )


  summary: Ember.computed('liabilityType', 'fixedRateMonths', 'interestRate', ->
    rate = Math.round(@get('interestRate') * 10000) / 100
    unless @get('liabilityType.name') == 'Fixed home loan'
      if @get('interestOnly')
        return "#{@get('liabilityType.name')} @ #{rate}% (int only)"
      else
        return @get('liabilityType.name') + " @ #{rate}%"
    else

      term = parseInt(@get('fixedRateMonths'))
      if term > 23
        term = "#{Math.floor(term/12)}yrs"
        if term % 12 > 0
          term = term + " #{term % 12}mths"
      else
        term = term + ' months'

      if @get('interestOnly')
        return "Fixed @ #{rate}% for #{term} (int only)"
      else
        return "Fixed @ #{rate}% for #{term}"
    )


  termString: Ember.computed('fixedRateMonths', 'requiresFixedTermExpiry', ->
    unless @get('requiresFixedTermExpiry')
      return 'floating'
    termMonths = @get('fixedRateMonths')
    if termMonths < 24
      return termMonths + 'mths'
    else
      mthsAfterYrs = termMonths %12
      mthString =  mthsAfterYrs + 'mths'
      yrString = (termMonths - mthsAfterYrs)/12 + 'yrs '
      if mthsAfterYrs > 0
        return yrString + mthString
      else
        return yrString
    )

  totalCommission: Ember.computed('totalTrailCommission', 'totalUpFrontCommission', 'totalFlatFeeCommission', ->
    return parseFloat(@get('totalTrailCommission')) + parseFloat(@get('totalUpFrontCommission')) + parseFloat(@get('totalFlatFeeCommission'))
    )



  weeklyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    weeklyFrequency = @store.peekAll('frequency').findBy('name', 'Weekly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 52, @get('interestOnly'))
    )


  fortnightlyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    fortnightlyFrequency = @store.peekAll('frequency').findBy('name', 'Fortnightly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 26, @get('interestOnly'))
    )


  monthlyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 12, @get('interestOnly'))
    )


  numberOfInterest: Ember.computed.alias('owing')


  sortOrder: Ember.computed('liabilityType', 'fixedRateMonths', ->
    liabilityType = @get('liabilityType.name')
    switch liabilityType
      when "Revolving credit"
        return 100
      when 'Floating home loan'
        return 200
      when 'Fixed home loan'
        return 300 + @get('fixedRateMonths')
    )


#observers:
  owingObs: Ember.observer('owing', 'hasLimit', 'liabilityType', 'status', ->
    if @get('liabilityType.name') #this stops it firing on load before the hasLimit is known
      unless @get('hasLimit') || @get('status') == 'pending'
        owing = @get('owing')
        @set('limit', owing)
    )



##functions:
  init: ->
    @_super()
    unless @get('frequency')
      monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
      @set('frequency', monthlyFrequency)


  util_createCommissionObjects: ->
    commissionRate = @get('commissionRate')
    unless @get('session.currentPosting.commissionRetentionRate')
      firmRetention = @get('session.currentFirm.commissionRetentionRate')
    else
      firmRetention = @get('session.currentPosting.commissionRetentionRate')

)

`export default Liability`
