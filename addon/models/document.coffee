`import DS from 'ember-data'`
#`import AwsDocumentMixin from '../mixins/shared/aws-document-mixin'`



Document = DS.Model.extend(
  notify: Ember.inject.service('notify')

  identifier: 'document'
  checkedByAdviser: DS.attr('boolean', {defaultValue: false}) #adviser needs to check each doc is appropriate for need
  #sign: DS.attr()
  name: DS.attr()
  documentContentType: DS.attr('string')
  documentFileName: DS.attr('string')
  documentFileSize: DS.attr()
  #location: DS.attr()
  #tag: DS.attr()
  #key: DS.attr()
  #documentContentType: DS.attr()
  #size: DS.attr()
  createdAt: DS.attr()
  updatedAt: DS.attr('date')

#for uploader
  uploadProgress: 100
  isUploading: Ember.computed('uploadProgress', ->
    if @get('uploadProgress') < 100
      return true
    )
  notifyUploadError: ->
    name = @get('name')
    notify = @get('notify')
    notify.error("there was a problem uploading your document '#{name}'. Please try again")



  #generateUrl: DS.belongsTo 'generateUrl',
    #async: true

  activities: DS.hasMany 'activities'
  approvals: DS.hasMany 'approvals'
  bank: DS.belongsTo 'bank'
  clients: DS.hasMany 'clients'
  documentPurpose: DS.belongsTo 'documentPurpose',
    async: false
  firm: DS.belongsTo 'firm'
  loanApplications: DS.hasMany 'loanApplications', {inverse: 'documents'}
  owner: DS.belongsTo 'owner'
  user: DS.belongsTo 'user'

  lastModified: (->
    updated = @get 'updatedAt'
    return moment(updated).fromNow()
  ).property 'updatedAt'


  iconPath: Ember.computed('documentContentType', ->
    docType = @get 'documentContentType'
    unless docType
      return 'icons/file-icons/file-text'

    if docType.includes('pdf')
      return 'icons/file-icons/file-acrobat'
    if docType.includes('img') or docType.includes('image')
      return 'icons/file-icons/file-picture'
    if docType.includes('excel') or docType.includes('xls')
      return 'icons/file-icons/file-excel'
    if docType.includes('msWord') or docType.includes('docX') or docType.includes('word')
      return 'icons/file-icons/file-word'
    if docType.includes('powerpoint') or docType.includes('ppt')
      return 'icons/file-icons/file-powerpoint'
    if docType.includes('photoshop') or docType.includes('psd')
      return 'icons/file-icons/file-ps'
    else
      return 'icons/file-icons/file-text'
    )


  pdfOrder: Ember.computed('documentPurpose.name', -> #sort order for pdf
    name = @get('documentPurpose.name')
    switch name
      when 'Application form' then 1
      when 'Signed Adviser Declaration' then 5
      when 'Bank forms' then 10
      when 'Identification' then 15
      when 'Proof of income' then 20
      when 'Bank statements' then 30
      when 'Proof of savings' then 40
      when 'Gift certificate' then 50
      when 'Proof of kiwisaver withdrawal amount' then 60
      when 'Loan statements' then 70
      when 'Sale & Purchase' then 75
      when 'Trust related documents' then 80
      when 'Company related documents' then 90
      when 'Guarantee' then 100
      when 'Other' then 110
      else 120
    )


  ###
  viewDocument: ->
    modelPath = @serialize().data.type
    signedURLEndPoint = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{modelPath}/" + @get('id') + '/generate_location'
    xhttp = new XMLHttpRequest

    notify = @get('notify')

    @get('session').authorize('authorizer:oauth2', (header,token)->
      xhttp.open("GET", signedURLEndPoint, true)
      xhttp.setRequestHeader('authorization', token)
      xhttp.onreadystatechange = ->
        if xhttp.readyState == 4 and xhttp.status == 200
          responseJSON = JSON.parse(xhttp.responseText)
          presignedURL = responseJSON.presigned_url
          open = window.open(presignedURL)
          if open == null or typeof(open) == 'undefined'
            alert "Your browser is currently blocking popups. You'll need to turn off your pop-up blocker to view this document"
        else
          if xhttp.status > 399
            notify.error 'We are unable to access the document at this time. It could be gremlins..'

      xhttp.send()
    )
  ###
)

`export default Document`
