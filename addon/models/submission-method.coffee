`import DS from 'ember-data'`

SubmissionMethod = DS.Model.extend {
  name: DS.attr 'string'
}

`export default SubmissionMethod`
