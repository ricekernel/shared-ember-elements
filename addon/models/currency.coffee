`import DS from 'ember-data'`

Currency = DS.Model.extend (
  name: DS.attr()
  rate: DS.attr()
  nzdBasedRate: DS.attr()

  isNZD: Ember.computed('name', ->
    if @get('name') == 'NZD'
      return true
    )
)




`export default Currency`
