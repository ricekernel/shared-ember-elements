`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


LoanApplicationBlob = DS.Model.extend(LoanCalculationsMixin,
  session: Ember.inject.service('session')

  applicationType: DS.attr('string') #an enum of long typed types - see 'applicationTypes'

  applicationTypes: ['', 'New Loan', 'Topup', 'Refinance']


  blob: DS.attr() #stores the loan structure

  wizardStarted: DS.attr('boolean', {defaultValue: false})
  wizardComplete: DS.attr('boolean', {defaultValue: false})
  setUpWizardComplete: DS.attr('boolean', {defaultValue: false})

  assessmentInterestRate: DS.attr('number', {defaultValue: 0.075})


  isBusinessBanking: DS.attr('boolean', {defaultValue: false})

  loanApplication: DS.belongsTo('loanApplication', {async: false})
  loanStructure: DS.belongsTo('loanStructure')#, {async: false})



  borrowerParties: Ember.computed.filterBy('loanApplication.loanParties', 'partyType', 'Borrower')
  borrowerParty: Ember.computed.alias('borrowerParties.firstObject')
  borrowerNames: DS.attr('string')


  retainedLiabilities: Ember.computed('loanApplication.borrowerParty.liabilities.@each.liabilityType', 'loanApplication.borrowerParty.loanPartyLiabilityLinks.@each.liabilitySettlementAction', ->
    @get('loanApplication.borrowerParty.liabilities').filter (lia) ->
      if ['Kept (will not refinance)', 'Kept (will refinance if lender changes)'].includes( lia.get('loanPartyLiabilityLink.liabilitySettlementAction') ) && lia.get('liabilityType.isSecured')
        return true
    )

  totalRetainedLiabilities: Ember.computed('retainedLiabilities.@each.limitAfterDrawDown', ->
    return @get('retainedLiabilities').mapBy('limitAfterDrawDown').compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    )

  newLending: Ember.computed.alias('loanApplication.applicationValue')


  refinanceValue: DS.attr('number', {defaultValue: 0.0})
  refiValueSetter: Ember.observer('retainedLiabilities.@each.liabilitySettlementAction', 'retainedLiabilities.@each.limitAfterDrawDown', ->
    refinanceValue = @get('retainedLiabilities').filterBy('loanPartyLiabilityLink.willRefinance').mapBy('limitAfterDrawDown').compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    @set('refinanceValue', refinanceValue)
    )


  lendingProposition: Ember.computed('totalRetainedLiabilities', 'newLending', ->
    return parseFloat( @get('totalRetainedLiabilities') ) + parseFloat( @get('newLending') )
    )


  retainedSecurities: Ember.computed('loanApplication.borrowerParty.securities.[]', 'loanApplication.borrowerParty.loanPartySecurityLinks.@each.securitySettlementAction', ->
    @get('loanApplication.borrowerParty.securities').filter (sec) ->
      unless sec.get('loanPartySecurityLink.securitySettlementAction') == "Sold"
        return true
    )

  totalRetainedSecurities: Ember.computed('retainedSecurities.@each.value', ->
    return @get('retainedSecurities').mapBy('value').compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    )


  proposedSecurities: Ember.computed.alias('loanApplication.securities')

  totalProposedSecurities: Ember.computed('proposedSecurities.@each.value', ->
    securities = @get('proposedSecurities')
    value = @util_CalculateTotalFinancials(securities, 'value')
    return parseFloat(value)
    )

  totalSecurityProposition: Ember.computed('totalRetainedSecurities', 'totalProposedSecurities', ->
    return parseFloat( @get('totalRetainedSecurities') ) + parseFloat( @get('totalProposedSecurities') )
    )


  lvr: Ember.computed('totalSecurityProposition', 'lendingProposition', ->
    return @get('lendingProposition') / @get('totalSecurityProposition')
    )





  loanParties: Ember.computed.alias('loanApplication.loanParties')




  isNewLoan: Ember.computed('applicationType', ->
    purposes = @get('applicationTypes')
    if @get('applicationType') == purposes[1]
      return true
    )

  isTopup: Ember.computed('applicationType', ->
    purposes = @get('applicationTypes')
    if @get('applicationType') == purposes[2]
      return true

    )

  isRefinance: Ember.computed('applicationType', ->
    purposes = @get('applicationTypes')
    if @get('applicationType') == purposes[3]
      return true
    )



  isSelfEmployed: Ember.computed('loanApplication.clients.@each.employmentStatus', ->
    se = false
    @get('loanApplication.clients').forEach (client) ->
      if client.get('employmentStatus.name') == 'Self employed'
        se = true
    return se
    )

  isPropertyInvestor: Ember.computed('loanApplication.securities.@each.securityUseType', 'loanApplication.loanParties.@each.isPropertyInvestor',->
    propInv = false
    @get('loanApplication.securities').forEach (sec) ->
      if sec.get('securityUseType.name') == 'Investment'
        se = true
    @get('loanApplication.loanParties').forEach (party) ->
      if party.get('isPropertyInvestor')
        se = true
    return propInv
    )

  applicationValue: Ember.computed.alias('loanApplication.applicationValue')



  umi: Ember.computed('applicationValue', 'assessmentInterestRate', 'loanParties.@each.residualIncome', ->
    balance = @get('applicationValue')
    rate = @get('assessmentInterestRate')
    pmt = @calculateLoanRepayment(balance, 360, rate, 12, false)
    residualIncome = 0
    @get('loanParties').forEach (party) ->
      residualIncome += parseFloat(party.get('residualIncome'))
    return residualIncome - pmt
    )






#observers
  refinancingSecuritiesObs: Ember.observer('refinancingSecurities.@each.isLoaded', ->
    @get('refinancingSecurities.content').forEach (sec) ->
      refinanceAction = sec.get('securitySettlementActions')[1]
      #sec.set('securitySettlementAction', refinanceAction)
    )


  clientContributionObs: Ember.observer('clientContribution', 'proposedSecurityValue', ->
    applicationType = @get('applicationType')
    applicationTypes = @get('applicationTypes')
    if applicationType == applicationTypes[1]
      applicationValue = parseFloat(@get('proposedSecurityValue')) - parseFloat(@get('clientContribution'))
      @set('applicationValue', applicationValue)

    )


  applicationTypeObs: Ember.observer('applicationType', ->
    applicationType = @get('applicationType')
    applicationTypes = @get('applicationTypes')

    if applicationType == applicationTypes[3]
      @setProperties(
        disposedSecurityValue: 0
        proposedSecurityValue: 0
        disposedLendingValue: 0
        clientContribution: 0
      )

    if applicationType == applicationTypes[2]
      @setProperties(
        clientContribution: 0
      )

    if applicationType == applicationTypes[1]
      @setProperties(
        existingSecurityValue: 0
        disposedSecurityValue: 0
        existingLendingValue: 0
        disposedLendingValue: 0
      )

    return false
    )









  didLoad: ->
    @_super()

    self = @
    blob = @get('blob')
    if blob
      if blob.loanStructure
        @store.pushPayload(blob.loanStructure)

    #migrating old application types to new application types (added 06/04/17) TODO: remove 06/04/18
    applicationType = @get('applicationType')
    switch applicationType
      when 'New Lending - No Existing Lending/Securities'
        self.set('applicationType', 'New Loan')
      when 'New Lending - Existing Lending/Securities (includes Buy + Sell)'
        self.set('applicationType', 'New Loan')
      when 'Topup or Refinance - No New Securities'
        self.set('applicationType', 'Topup')




  serialize: (options) ->
    loanStructure = @get('loanStructure.content')
    jsonLoanStructure = loanStructure.serialize(includeId: true)
    blob = {
      loanStructure: jsonLoanStructure
    }
    @set('blob', blob)
    @_super(options)



  util_CalculateTotalFinancials: (list, valueAttribute) ->
    return 0 unless list
    values = list.mapBy(valueAttribute)
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue

  )

`export default LoanApplicationBlob`
