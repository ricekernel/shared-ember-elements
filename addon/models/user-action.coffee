`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`

DSdate = DS.attr "date"

UserAction = DS.Model.extend(ModelNotifyServerActionsMixin,
  altModelName: 'action/reminder'
  eventDate: DSdate
  isAcknowledged: DS.attr "boolean"
  notes: DS.attr "string"
  priority: DS.attr "number"

  #NEW:
  title: DS.attr "string"
  path: DS.attr "string"
  pathText: DS.attr "string"


  #displayDate: Ember.computed.reads('eventDate')

#relationships
  actionStatus: DS.belongsTo "actionStatus",
    async: false
  client: DS.belongsTo 'client'
  loanApplication: DS.belongsTo 'loanApplication'
  approval: DS.belongsTo 'approval'
  user: DS.belongsTo 'user'
  userActionConfig: DS.belongsTo 'userActionConfig'


#computeds
  executeLeadDays: Ember.computed.alias('userActionConfig.executeLeadDays') #how many days before the event the user-action-config is set to send a message
  executeDate: Ember.computed('eventDate', 'executeLeadDays', ->
    @get('eventDate') - @get('executeLeadDays')
    )


  isPending: Ember.computed.equal('actionStatus.name', 'Pending')
  isNotAcknowledged: Ember.computed.not('isAcknowledged')
  isLive: Ember.computed.and('isPending', 'isNotAcknowledged')

  isDue: Ember.computed('eventDate', ->
    dueDate = moment(@get('eventDate'))
    dueDate.startOf('day')

    if moment().startOf('day').isSame(dueDate)
      return true
    )

  isOverdue: (->
    dueDate = moment(@get('eventDate'))
    dueDate.startOf('day')
    if moment().subtract(1, 'days').isAfter dueDate
      return true
    ).property 'eventDate'


  momentFromNow: Ember.computed('eventDate', -> #used by the table to stratify by date
    dueDate = moment(@get('eventDate'))
    dueDate.startOf('day')
    today = moment()
    today.startOf('day')
    return dueDate.from(today)
    )


#functions:
  completeAction: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      if self.get('executeDate')
        self.set('isAcknowledged', true)
      else
        completedStatus = self.store.peekAll('actionStatus').findBy('name', 'Completed')
        self.set('actionStatus', completedStatus)
      self.save().then (->
        resolve self
      ), (error) ->
        reject error
    )



  delay: (n, t)->
    dateMoment = moment(@get('eventDate'))
    dateMoment.add n, t
    @set "eventDate", dateMoment.toDate()


  #temp fix to correct an erroneous path in some actions.  TODO: Remove after 1 May 17
  ready: ->
    @_super()
    if @get('path')
      if @get('path').includes('undefined')
        path = @get('path').replace('undefined', 'https://la.socket.co.nz')
        @set('path', path)


  ###
  save: -> #displayDate differs so we can set it, and the action not move until saved
    displayDate = @get('displayDate')
    eventDate = @get('eventDate')
    console.log displayDate, eventDate
    unless displayDate == eventDate
      @set('eventDate', displayDate)
    @_super()
    ###

)

`export default UserAction`
