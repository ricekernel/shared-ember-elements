`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`


Template = DS.Model.extend(ModelNotifyServerActionsMixin,
  name: DS.attr "string"
  message: DS.attr "string"
  templateType: DS.belongsTo 'templateType'
  subject: DS.attr "string"
  isSentFromMe: DS.attr('boolean', {defaultValue: true})
  isHtml: DS.attr('boolean', {defaultValue: true})
  isSms: DS.attr
  requiresSample: DS.attr('boolean', {defaultValue: false})
  ownerType: DS.attr('string') # :user :firm

  firm: DS.belongsTo 'firm'
  user: DS.belongsTo 'user'

#computeds:
  dependsOnModels: Ember.computed('message', ->
    if @get('message').indexOf('{{') == -1
      return null
    else
      templateFields = @store.peekAll('templateField')
      templateFieldStringArray = @get('message').match(/{{[a-z\_]*/g).uniq()

      templateFieldArray = templateFieldStringArray.map((item) ->
        newString = item.substring(2) #get rid of the opening {{
        templateFieldUsed = templateFields.find( (item, index) ->
          return item.get('name') == newString
        )
        return templateFieldUsed
      )

      #build an array of the models we need:
      requiredModelArray = templateFieldArray.compact().mapBy('baseModel')
      return requiredModelArray


  )

)

`export default Template`
