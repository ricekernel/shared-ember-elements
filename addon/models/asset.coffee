`import DS from 'ember-data'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`

Asset = DS.Model.extend(ModelFinancialForexMixin, ModelNotifyServerActionsMixin,

  description: DS.attr()
  identifier: 'asset'
  value: DS.attr('number')
  rate: DS.attr('number')

  #relates to securities:
  isSecurity: DS.attr("boolean", {defaultValue: false})
  isProposed: DS.attr("boolean", {defaultValue: false})


#relationships
  address: DS.belongsTo 'address'
  assetType: DS.belongsTo 'assetType', {async: false}
  bank: DS.belongsTo 'bank', {async: false}
  clients: DS.hasMany 'clients'
  currency: DS.belongsTo 'currency'
  loanApplications: DS.hasMany 'loanApplications'


  securityOwnershipType: DS.belongsTo 'securityOwnershipType', {async: false}
  securityType: DS.belongsTo 'securityType', {async: false}
  securityUseType: DS.belongsTo 'securityUseType', {async: false}
  settlement: DS.belongsTo 'settlement'

  loanPartySecurityLink: DS.belongsTo('loanPartySecurityLink', {async: false})


#computeds
  name :Ember.computed('description', 'assetType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('isSecurity')
      return 'Security'
    if @get('description') && @get('assetType.name')
      return "#{@get('description')} (#{@get('assetType.name')})"
    if @get('assetType.name')
      return @get('assetType.name')
    else
      return 'Asset'
  )

  altModelName: Ember.computed.alias('name')

  numberOfInterest: Ember.computed.alias('value') #used by mixin to convert to NZD

  type: Ember.computed.alias('assetType')#used by client.NewFinancial()

  isProperty: Ember.computed.equal('assetType.name', 'Property')

  deleteRecord: ->
    if @get('loanPartySecurityLink')
      link = @get('loanPartySecurityLink')
      link.deleteRecord()
    @_super()

)





`export default Asset`
