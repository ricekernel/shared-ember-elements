`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`
`import Owner from "./owner"`

Firm = Owner.extend(ModelNotifyServerActionsMixin,
  commissionRetentionRate: DS.attr 'number', {defaultValue: 0.2} #how much of its advisers commission a firm keeps
  email: DS.attr 'string'
  phone: DS.attr 'string'
  name: DS.attr 'string'
  plans: DS.attr() #a string array of the plans the firm has subscribed to


  account: DS.belongsTo 'account'
  commissionRates: DS.hasMany 'commissionRates'
  #currentBrokerPostings: DS.hasMany 'brokerPostings'
  #documents: DS.hasMany 'documents'
  physicalAddress: DS.belongsTo 'address'
  postalAddress: DS.belongsTo 'address'
  templates: DS.hasMany 'templates'
  users: DS.hasMany 'users'

  altModelName: Ember.computed('name', ->
    if @get('name')
      return @get('name')
    else
      return 'firm'
  )


  hasLoanApp: Ember.computed('plans', ->
    if @get('plans').includes('Loan Application')
      return true
    )

  hasToolkit: Ember.computed('plans', ->
    if @get('plans').includes('Toolkit')
      return true
    )

  hasCRM: Ember.computed('plans', ->
    if @get('plans').includes('CRM')
      return true
    )


  #liveBrokerPostings: Ember.computed.filterBy('currentBrokerPostings', 'startDate')
  #pendingBrokerPostings: Ember.computed.filterBy('currentBrokerPostings', 'status', 'pending')

  #users: Ember.computed.mapBy('liveBrokerPostings', 'user')


  save: (proper) ->
    if proper
      return @_super()
    self = @
    saveArray = []

    @eachRelationship (name, descriptor) ->
      if descriptor.kind == 'belongsTo'
        if self.get(name).get('hasDirtyAttributes')
          record = self.get(name)
          if record.get('content')
            record = record.get('content')
          saveArray.pushObject(record.save())
    Ember.RSVP.all(saveArray).then ->
      self.save(true)


)
`export default Firm`
