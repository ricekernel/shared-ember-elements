`import DS from 'ember-data'`

Frequency = DS.Model.extend(
  name: DS.attr()
  yearConversion: DS.attr()

  sortOrder: Ember.computed('name', ->
    name = @get('name')
    switch name
      when 'Weekly' then 10
      when 'Fortnightly' then 20
      when 'Monthly' then 30
      when 'Quarterly' then 40
      when 'Half-Yearly' then 50
      when 'Annually' then 60
  )
)




`export default Frequency`
