`import DS from 'ember-data'`

LiabilityType = DS.Model.extend {
  name: DS.attr 'string'
  isSecured: DS.attr 'boolean'

  isFixed: Ember.computed.equal('name', 'Fixed home loan')
  isInterestOnly: Ember.computed.equal('name', 'Revolving credit')

  shortenedName: Ember.computed('name', ->
    @get('name').split(' ')[0]
    )

  sortOrder: Ember.computed('name', -> #for sorting loans like in pricing offers
    name = @get('name')
    switch name
      when 'Floating home loan' then 10
      when 'Revolving credit' then 20
      when 'Fixed home loan' then 30
      when 'Personal loan' then 40
      when 'Student/Tertiary loan' then 50
      when 'Credit card' then 60
      when 'Store card' then 70
      when 'Hire purchase' then 80
      when 'Overdraft' then 90
      when	'Other loan' then 100
      else 110
      )

  iconPath: Ember.computed('name', -> #for sorting loans like in pricing offers
    name = @get('name')
    switch name
      when 'Floating home loan' then "icons/home-loan-floating"
      when 'Revolving credit' then "icons/home-loan-revolving"
      when 'Fixed home loan' then "icons/home-loan-fixed"
      else "icons/home-loan-floating"
      )
}

`export default LiabilityType`
