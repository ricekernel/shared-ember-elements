`import DS from 'ember-data'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`


Income = DS.Model.extend(ModelFinancialForexMixin, ModelNotifyServerActionsMixin,

  description: DS.attr()
  hasStudentLoan: DS.attr('boolean', {defaultValue: false})
  identifier: 'income'
  isNet: DS.attr('boolean', {defaultValue: true})
  kiwisaver: DS.attr('number')
  #monthly: DS.attr('number') #used by server. Set it with netmonthly computed
  rate: DS.attr('number')
  value: DS.attr('number')



#relationships
  clients: DS.hasMany 'clients'
  currency: DS.belongsTo 'currency'
  frequency: DS.belongsTo 'frequency',
    async: false
  incomeType: DS.belongsTo 'incomeType', {async: false}
  loanApplications: DS.hasMany 'loanApplications'


#computeds
  name :Ember.computed('description', 'incomeType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('description') && @get('incomeType.name')
      return "#{@get('description')} (#{@get('incomeType.name')})"
    if @get('incomeType.name')
      return @get('incomeType.name')
    else
      return 'Income'
  )

  altModelName: Ember.computed.alias('name')


  isSalary: (->
    if @get('incomeType')
      if @get('incomeType').get('name') == 'Salary/Wage'
        true
    ).property 'incomeType'




  netMonthly: Ember.computed('value', 'isNet', 'frequency', 'kiwisaver', 'hasStudentLoan', ->

    unless @get('value') > 0 && @get('frequency')
      return 0

    ann = @get('frequency.yearConversion') * @get('value')
    if @get('incomeType.isTaxedAtPersonalRate') && !@get('isNet')
      netMonthly = @calculateNetMonthly(ann, true)
    else
      netMonthly = ann/12
    return netMonthly
    )

  netMonthlyBeforeDeductions: Ember.computed('value', 'isNet', 'frequency', 'kiwisaver', 'hasStudentLoan', ->
    unless @get('value') > 0 && @get('frequency')
      return 0
    ann = @get('frequency.yearConversion') * @get('value')
    if @get('incomeType.isTaxedAtPersonalRate') && !@get('isNet')
      netMonthly = @calculateNetMonthly(ann, false)
    else
      netMonthly = ann/12
    return netMonthly
    )



  numberOfInterest: Ember.computed.alias('netMonthly') #used by mixin to convert to NZD

  ###
  nzdVal: (->
    unless @get('currency.isNZD')
      (Math.round(@get('netMonthly')*@get('rate')*100))/100
    else
      @get('netMonthly')
    ).property 'netMonthly', 'rate'
  ###

  type: Ember.computed.alias('incomeType')


  netIncomeObs: Ember.observer('incomeType', ->
    incomeType = @get('incomeType.name') || ''
    if ['Board (Room only)', 'Board (All inclusive)', 'Other'].includes(incomeType)
      @set('isNet', true)
    )


#functions
  calculateGrossAnnual:  ->
    if @get('value') < 1 or !@get('value') or !@get('frequency')
      return 0

    annualNet = @get('value') * @get('frequency.yearConversion')

    if @get('incomeType.isTaxedAtPersonalRate') && @get('isNet')
      taxBands = @store.peekAll('taxBand')

      grossAnnual = annualNet * 2
      loop
        estimate = @calculateNetMonthly(grossAnnual, true) * 12
        grossAnnual = grossAnnual - (estimate - annualNet)
        if estimate >= (annualNet - 1) && estimate <= (annualNet + 1) then break
      return Math.round(grossAnnual)
    else
      return annualNet



  calculateNetMonthly: (ann, includeDeductions) ->
    incomeType = @get('incomeType.name')
    taxBands = @store.peekAll('taxBand')

    netAnnual = 0
    paye = taxBands.filterBy('name', 'PAYE')
    paye.forEach (marginalRate) ->
      if marginalRate.get('lower') < ann and (marginalRate.get('upper') >= ann or not marginalRate.get('upper'))
        lowerBound = parseInt(marginalRate.get('lower'))
        rate = parseFloat(marginalRate.get('rate'))
        netBelowBand = parseFloat(marginalRate.get('lowerNet')) || 0
        netAnnual = ((ann - lowerBound) * (1-rate)) + netBelowBand

    if includeDeductions
      if @get('hasStudentLoan')
        slTax = taxBands.filterBy('name', 'STUDENT').filter  (rate) ->
          rate.get('lower') > 1
        slTax = slTax.get('firstObject')

        slThreshold = parseFloat(slTax.get('lower'))
        if ann > slThreshold
          slRate = parseFloat(slTax.get('rate'))
          slRepayment = (ann - slThreshold) * slRate
          netAnnual = netAnnual - slRepayment

      if @get('kiwisaver')
        netAnnual = netAnnual - (ann * @get('kiwisaver'))

    val = netAnnual/12
    return (Math.round(val*100))/100



  calculateAnnualStudentLoan: ->
    taxBands = @store.peekAll('taxBand')
    slTax = taxBands.filterBy('name', 'STUDENT').filter  (rate) ->
      rate.get('lower') > 1
    slTax = slTax.get('firstObject')
    annualGrossIncome = @calculateGrossAnnual()
    slThreshold = parseFloat(slTax.get('lower'))
    slRate = parseFloat(slTax.get('rate'))
    if annualGrossIncome > slThreshold
      slRepayment = (annualGrossIncome - slThreshold) * slRate
    return slRepayment


  overrideDelete: ->
    self = @
    @get('applicationIncomeLinks').then (links) ->
      link = links.get('firstObject')
      link.destroyRecord().then ->
        self.destroyRecord()

)



`export default Income`
