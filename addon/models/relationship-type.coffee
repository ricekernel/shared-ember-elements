`import DS from 'ember-data'`

RelationshipType = DS.Model.extend
  left: DS.attr 'string'
  right: DS.attr 'string'

  name: Ember.computed('left', 'right', ->
    return "#{@get('left')} - #{@get('right')}"
    )

`export default RelationshipType`
