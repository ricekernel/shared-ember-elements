`import DS from 'ember-data'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`

#`import Asset from "./asset"`

Security = DS.Model.extend(ModelFinancialForexMixin, ModelNotifyServerActionsMixin,
#Security = Asset.extend(ModelFinancialForexMixin, ModelNotifyServerActionsMixin,

  description: DS.attr('string')
  identifier: 'security'
  isProposed: DS.attr('boolean')
  rate: DS.attr('number')
  recordAsset: DS.attr('boolean', {defaultValue: false})

  value: DS.attr('number')
  valueSource: DS.attr()


#relationships
  address: DS.belongsTo 'address'
  asset: DS.belongsTo 'asset'
  bank: DS.belongsTo 'bank', {async: false}
  clients: DS.hasMany 'clients'
  currency: DS.belongsTo 'currency'
  loanApplications: DS.hasMany 'loanApplications'
  loanPartySecurityLink: DS.belongsTo('loanPartySecurityLink', {async: false})

  securityOwnershipType: DS.belongsTo 'securityOwnershipType', {async: false}
  securityType: DS.belongsTo 'securityType', {async: false}
  securityUseType: DS.belongsTo 'securityUseType', {async: false}
  settlement: DS.belongsTo 'settlement'




#computeds:
  name :Ember.computed('addressDescription', 'securityType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('addressDescription')
      return "#{@get('addressDescription')}"
    else
      return 'Security'
  )

  altModelName: Ember.computed.alias('name')

  numberOfInterest: Ember.computed.alias('value') #used by mixin to convert to NZD

  type: Ember.computed.alias('securityType')

  addressDescription: Ember.computed.alias('address.content.abbreviatedAddress') #used by observers to change description on security and asset

  securitySettlementAction: Ember.computed.alias('loanPartySecurityLink.securitySettlementAction')

#observers:
  addressObserver: Ember.observer('addressDescription', ->
    # Change the description in response to address changes
    unless @get('isDeleted')
      @set('description', @get('addressDescription'))
    )

  ###
  assetLoaded: Ember.observer('asset.content', ->
    #once the asset is loaded, give it a context of its related security (cant have two way 'belongsTo'):
    self = @
    @get('asset').then (asset) ->
      if asset
        asset.set('security', self)
    )

  assetUpdater: Ember.observer('value', 'address', ->
    #update the asset value or description in response to changes to the security:
    self = @
    @get('asset').then (asset) ->
      if asset
        unless asset.get('isDeleted')
          unless asset.get('value') == self.get('value')
            asset.set('value', self.get('value'))
          unless asset.get('description') == self.get('description')
            asset.set('description', self.get('description'))
    )


#functions:
  createRelatedAsset: -> #create a related asset when loading an existing security
    self = @
    securityAssetType = @store.peekAll('assetType').findBy('name', 'Property')
    newAsset = self.store.createRecord('asset',
      assetType: securityAssetType
      clients: self.get('clients')
      value: self.get('value')
      description: self.get('description')
      security: self
      loanApplications: self.get('loanApplications')
      )
    self.set('asset', newAsset)
    return newAsset


  didCreate: ->
    unless @get('isProposed')
      @createRelatedAsset()
    @_super()

  ###
  save: ->

    self = @
    @_super().then (security) ->
      return new Ember.RSVP.Promise((resolve, reject) ->
        firstRoundSaves = []
        if self.get('address.hasDirtyAttributes')
          self.get('address').then (address) ->
            firstRoundSaves.pushObject(address.save())

        Ember.RSVP.all(firstRoundSaves).then (->
          resolve security
        ), (error) ->
          reject error

      )





  setDefaults: ->
    house = @store.peekAll('securityType').findBy('name', 'House')
    personalOwnership = @store.peekAll('securityOwnershipType').findBy('name', 'Personal')
    ownerOcc = @store.peekAll('securityUseType').findBy('name', 'Owner occupied')
    @setProperties(
      securityOwnershipType: personalOwnership
      securityType: house
      securityUseType: ownerOcc
    )





  ###
  overrideDelete: ->
    self = @
    notify = @get('notify')
    if @get('hasAsset') #this gone
      @get('asset').then (asset) ->
        asset.destroyRecord().then ( ->
          self.destroyRecord()
        ), (error) ->
          notify.error "Uhoh - the server said: #{error}"
    else
      @destroyRecord()
  ###



)
###
  didLoad: ->
    @_super()
    self = @

    theAss = @get('asset')
    @get('asset').then (asset) ->
      if asset
        asset.set('security', self)




#functions


  createRelatedAsset: -> #used for loan settlement
    self = @
    securityAssetType = @store.peekAll('assetType').findBy('name', 'Property')
    securityAsset = self.store.createRecord('asset',
      assetType: securityAssetType
      clients: clients
      value: self.get('value')
      description: self.get('description')
      security: self
      )
    self.set('asset', newAsset)
    return securityAsset



  createRelatedAssetForLoanApplication: (loanApplication) ->
    self = @
    securityAssetType = @store.peekAll('assetType').findBy('name', 'Property')
    secAsset = loanApplication.addFinancial('asset', securityAssetType)
    if @get('address.street.length') > 0
      secAsset.set('description', @get('address.abbreviatedAddress'))
    if @get('value')
      secAsset.set('value', @get('value'))
    @set 'asset', secAsset
    return secAsset

  deleteRecord: ->
    if @get('asset')
      unless @get('asset.isDeleted')
        relatedAsset = @get('asset')
        relatedAsset.deleteRecord()
    @_super()

  save: ->
    self = @
    @_super().then ->
      if self.get('asset.hasDirtyAttributes')
        self.get('asset').save()
      if self.get('address.hasDirtyAttributes')
        self.get('address.content').save()



###



`export default Security`
