`import DS from 'ember-data'`

Tool = DS.Model.extend
  name: DS.attr 'string'
  toolType: DS.attr 'enum', {caps: 'all'}
  createdAt: DS.attr()
  updatedAt: DS.attr()
  blob: DS.attr()



#relationships
  clients: DS.hasMany 'clients'
  loanApplication: DS.belongsTo 'loanApplication'
  firm: DS.belongsTo 'firm'
  owner: DS.belongsTo 'user'
  plan: DS.belongsTo 'loanPlan', {async: false}
  users: DS.hasMany 'users'


  canShare: Ember.computed('toolType', ->
    toolType = @get('toolType')
    if ['Loan Planner'].includes(toolType) then return true
    )


  saveIfNoId: -> #this function used to only save the tool if its not been saved
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      unless self.get('isNew')
        resolve self
      else
        self.save().then (->
          resolve self
        ), (error) ->
          reject error
      )



  saveTool: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      toolType = self.get('toolType')
      saveFunctions = []

      switch toolType
        when 'Loan Planner'
          plan = self.get('plan')
          saveFunctions.pushObject( plan.saveTool( self ) )
        else return false

      Ember.RSVP.all( saveFunctions ).then ((response) ->
        resolve response

      ), (error) ->
        reject error
    )







  util_UnblobBlob: ->
    self = @
    type = @get('toolType')
    blobString = @get('blob')
    ###
    JSON.parse(blob).forEach (relatedModel) ->
      if relatedModel.data.id
        self.store.pushPayload(relatedModel)
    ###
    if blobString
      blob = JSON.parse(blobString)
      switch type
        when 'Loan Planner'
          if blob.loanPlan
            self.store.pushPayload(blob.loanPlan)
            loanPlanId = blob.loanPlan.data.id
            loanPlan = self.store.peekAll('loanPlan').findBy('id', loanPlanId)
            self.set('plan', loanPlan)
        else return false









`export default Tool`
