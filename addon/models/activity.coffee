`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`

Activity = DS.Model.extend(#ModelNotifyServerActionsMixin,
  isServerGenerated: DS.attr 'boolean'
  date: DS.attr 'date'
  description: DS.attr "string"
  activityType: DS.attr 'string'

  clients: DS.hasMany 'clients'
  documents: DS.hasMany 'documents'
  message: DS.belongsTo 'message'
  user: DS.belongsTo 'user'
  approval: DS.belongsTo 'approval'
  loanApplication: DS.belongsTo 'loanApplication'


  displayDate: Ember.computed('date', ->
    date = @get('date')
    return moment(date).format("D MMM YYYY")
    )

  displayName: Ember.computed.alias('activityType')

  iconPath: Ember.computed('activityType', ->

    switch @get('activityType')
      when 'phone' then 'icons/activity/activity-type-phone'
      when 'email' then 'icons/activity/activity-type-email'
      when 'sms' then 'icons/activity/activity-type-sms'
      when 'interview' then 'icons/activity/activity-type-interview'
    )
)

`export default Activity`
