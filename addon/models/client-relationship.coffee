`import DS from 'ember-data'`

ClientRelationship = DS.Model.extend
  leftRelationshipType: DS.attr() #:borrower :guarantor
  rightRelationshipType: DS.attr() #:borrower :guarantor


  leftClient: DS.belongsTo 'client',
    inverse: null
  rightClient: DS.belongsTo 'client',
    inverse: null

  relationshipType: DS.belongsTo 'relationshipType', {async: false}




`export default ClientRelationship`
