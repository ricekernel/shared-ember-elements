`import DS from 'ember-data'`

Invite = DS.Model.extend
  loanApplication: DS.belongsTo 'loanApplication'
  individual: DS.belongsTo 'individual'

`export default Invite`
