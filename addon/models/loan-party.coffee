`import DS from 'ember-data'`


LoanParty = DS.Model.extend(
  session: Ember.inject.service('session')
  blob: DS.attr()
  partyType: DS.attr('string') #enum
  partyTypes: ['', 'Borrower', 'Guarantor', 'Related Party']


#relationships:
  clients: DS.hasMany 'clients'
  loanPartyLiabilityLinks: DS.hasMany 'loanPartyLiabilityLinks', {async: false}
  loanPartySecurityLinks: DS.hasMany 'loanPartySecurityLinks', {async: false}
  loanApplication: DS.belongsTo 'loanApplication', {async: false}


#computeds

  assets: Ember.computed('clients.@each', ->
    request = @getArrayForFin('assets', {key: 'isProposed', value: false})
    return DS.PromiseArray.create(
      promise: request
        )
  )


  liabilities: Ember.computed('clients.@each', ->
    request = @getArrayForFin('liabilities', {key: 'status', value: 'current'})
    return DS.PromiseArray.create(
      promise: request
        )
  )


  expenses: Ember.computed('clients.@each', ->
    request = @getArrayForFin('expenses')
    return DS.PromiseArray.create(
      promise: request
        )
  )

  incomes: Ember.computed('clients.@each', ->
    request = @getArrayForFin('incomes')
    return DS.PromiseArray.create(
      promise: request
        )
  )



  ###
  securities: Ember.computed('clients.@each', ->
    request = @getArrayForFin('securities', {key: 'isProposed', value: false})
    return DS.PromiseArray.create(
      promise: request
        )
  )


  proposedSecurities: Ember.computed('clients.@each', ->
    request = @getArrayForFin('securities', {key: 'isProposed', value: true})
    return DS.PromiseArray.create(
      promise: request
        )
  )
  ###



  getArrayForFin: (fin, filter) ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      list = []
      calls = []
      self.get('clients').then (clients) ->
        clients.forEach (client) ->
          calls.pushObject(client.get(fin))
        Ember.RSVP.all(calls).then (results) ->
          results.forEach (resultArray) ->
            resultArray.forEach (result) ->
              unless list.includes(result) or result.get('isDeleted')
                if filter
                  if result.get(filter.key) == filter.value
                    list.pushObject(result)
                else
                  list.pushObject(result)
          resolve list
      )




  nameObs: Ember.observer('name', ->
    name = @get('name')
    if @get('loanApplication.loanApplicationBlob.borrowerParty') == this
      @set('loanApplication.loanApplicationBlob.borrowerNames', name)
    )


  name: Ember.computed('clients.@each.firstName', 'clients.@each.lastName', ->

    clientsLength = @get('clients.length')
    switch clientsLength

      when 0 then return 'New party (no people)'
      when 1 then return "#{@get('clients.firstObject.firstName') || ''} #{ @get('clients.firstObject.lastName')  || ''}"
      when 2
        clientLastNames = @get('clients').mapBy('lastName')
        if clientLastNames.uniq().get('length') == 1
          longNames =  "#{@get('clients.firstObject.firstName')} & #{@get('clients.lastObject.firstName')} #{@get('clients.firstObject.lastName')}"
          if longNames.length < 25
            return longNames
          else
            return "#{(@get('clients.firstObject.firstName') || ' ').charAt(0)} & #{(@get('clients.lastObject.firstName') || ' ').charAt(0)} #{@get('clients.firstObject.lastName')}"

        else
          return "#{(@get('clients.firstObject.firstName') || ' ').charAt(0)} #{@get('clients.firstObject.lastName')} & #{(@get('clients.lastObject.firstName') || ' ').charAt(0)} #{@get('clients.lastObject.lastName')}"
      else
        name = ''
        i = 0
        @get('clients').forEach (client) ->
          if i > 0
            name += ", #{(client.get('firstName') || ' ').charAt(0)} #{client.get('lastName') || ''}"
          else
            name += "#{(client.get('firstName') || ' ').charAt(0)} #{client.get('lastName') || ''}"
          i++
        return name
  )


  isSelected: Ember.computed('session.selectedParty', ->
    if @get('session.selectedParty.id') == @get('id')
      return true
    )


  netPosition: Ember.computed('totalAssets', 'totalLiabilities', ->
    return parseFloat(@get('totalAssets')) - parseFloat(@get('totalLiabilities'))
    )


  residualIncome: Ember.computed('totalIncome', 'totalExpenses', ->
    return parseFloat(@get('totalIncome')) - parseFloat(@get('totalExpenses'))
    )

  securities: Ember.computed.filterBy('assets', 'isProperty')

  #financials totals
  totalLiabilities: Ember.computed('liabilities.@each.limit', ->
    return @util_CalculateTotalFinancials('liabilities', 'limit')
    )

  totalLimitsAfterDrawDown: Ember.computed('loanPartyLiabilityLinks.@each.limitAfterDrawDown', ->
    return @util_CalculateTotalFinancials('loanPartyLiabilityLinks', 'limitAfterDrawDown')
    )

  totalSecurities: Ember.computed('securities.@each.value', ->
    return @util_CalculateTotalFinancials('securities', 'value')
    )


  totalAssets: Ember.computed('assets.@each.value', ->
    return @util_CalculateTotalFinancials('assets', 'value')
    )


  totalExpenses: Ember.computed('expenses.@each.monthly', ->
    return @util_CalculateTotalFinancials('expenses', 'monthly')
    )


  totalIncome: Ember.computed('incomes.@each.netMonthly', ->
    return @util_CalculateTotalFinancials('incomes', 'netMonthly')
    )



  removeClient: (client) ->
    self = @
    @get('clients').removeObject(client)
    @get('loanApplication.clients').removeObject(client)
    return false


  addAsset: (asset) ->
    asset = @util_AddFinancial('asset', asset)
    return asset


  addClient: (client) ->
    self = @
    @get('clients').pushObject(client)
    loanApplication = @get('loanApplication')
    loanApplication.get('clients').pushObject(client)
    client.get('loanApplications').pushObject(loanApplication)
    firm = loanApplication.get('firm')
    owner = loanApplication.get('owner')
    client.set('owner', owner)
    client.set('firm', firm)
    client.get('liabilities').then (lias) ->
      lias.forEach (lia) ->
        limit = lia.get('limit')
        if lia.get('liabilityType.isSecured') then willRefinance = true else willRefinance = false
        newLink = self.store.createRecord('loanPartyLiabilityLink',
          liability: lia
          loanParty: self
          limitAfterDrawDown: limit
          willRefinance: willRefinance
        )
        self.get('loanPartyLiabilityLinks').pushObject(newLink)
    client.get('assets').then (assets) ->
      assets.forEach (asset) ->
        if asset.get('isProperty')
          newLink = self.store.createRecord('loanPartySecurityLink',
            loanParty: self
            security: asset
          )
          self.get('loanPartySecurityLinks').pushObject(newLink)
    #this to sort temp permissions problems:
    @util_AddClientToLaSecuritiesAndLiabilities(loanApplication, client)
    return client

  addExpense: (expense) ->
    expense = @util_AddFinancial('expense', expense)
    return expense

  addIncome: (income) ->
    income = @util_AddFinancial('income', income)
    return income

  addLiability: (lia) ->
    lia = @util_AddFinancial('liability', lia)
    newLiaLink = @store.createRecord('loanPartyLiabilityLink'
      loanParty: this
      liability: lia
      status: 'current'
      )
    @get('loanPartyLiabilityLinks').pushObject(newLiaLink)
    return lia


  addSecurity: (sec) ->
    sec = @util_AddFinancial('asset', sec)
    sec.set('isSecurity', true)
    propertyType = @store.peekAll('assetType').findBy('name', "Property")
    sec.set('assetType', propertyType)
    newSecLink = @store.createRecord('loanPartySecurityLink'
      loanParty: this
      security: sec
      )
    @get('loanPartySecurityLinks').pushObject(newSecLink)
    return sec



  util_AddFinancial: (type, item) ->
    inflector = new Ember.Inflector(Ember.Inflector.defaultRules)
    relationship = inflector.pluralize(type) #how sexy is this?!
    unless item
      clients = @get('clients')
      item = @store.createRecord(type,
        clients: clients
      )
    @get(relationship).then (rel) ->
      rel.pushObject(item)
    return item


  util_CalculateTotalFinancials: (relationship, valueAttribute) ->
    values = @get(relationship).mapBy(valueAttribute)
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue


  util_AddClientToLaSecuritiesAndLiabilities: (loanApplication, client) ->
    loanApplication.get('assets').then (secs) ->
      secs.forEach (sec) ->
        sec.get('clients').pushObject(client)
        sec.save()
    loanApplication.get('liabilities').then (lias) ->
      lias.forEach (lia) ->
        lia.get('clients').pushObject(client)
        lia.save()



  loanApplicationObs: Ember.observer('loanApplication', ->
    self = @
    #migration for some legacy loan apps that didn't have the client linked: 08/04/17
    loanApplication = @get('loanApplication')

    if loanApplication
      @get('clients').then (clients) ->
        clients.forEach (client) ->
          unless loanApplication.get('clients').includes(client)
            loanApplication.get('clients').pushObject(client)
          loanApplication.get('assets').then (assets) ->
            secs = assets.filterBy('isProperty')
            secs.forEach (sec) ->
              unless sec.get('clients').includes(client)
                sec.get('clients').pushObject(client)
          loanApplication.get('liabilities').then (secs) ->
            secs.forEach (sec) ->
              unless sec.get('clients').includes(client)
                sec.get('clients').pushObject(client)
    )


  #Observer and function to create missing links if lias or securities have been pulled in from outside
  financialsObs: Ember.observer('liabilities.[]', 'securities.[]', ->
    Ember.run.once(this, 'util_CreateMissingLinks')
    )




  util_CreateMissingLinks: ->
    self = @
    missingLinks = 0
    @get('liabilities').forEach (lia) ->
      unless lia.get('loanPartyLiabilityLink') || lia.get('status') == 'pending'
        newLink = self.store.createRecord('loanPartyLiabilityLink',
          loanParty: self
          liability: lia
        )
        self.get('loanPartyLiabilityLinks').pushObject(newLink)
        missingLinks++

    @get('securities').forEach (asset) ->
      unless asset.get('loanPartySecurityLink') #|| !asset.get('isProperty')
        newLink = self.store.createRecord('loanPartySecurityLink',
          loanParty: self
          security: asset
        )
        self.get('loanPartySecurityLinks').pushObject(newLink)
        missingLinks++


)

`export default LoanParty`
