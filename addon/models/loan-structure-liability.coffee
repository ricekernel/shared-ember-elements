`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import accounting from 'accounting'`


LoanStructureLiability = DS.Model.extend(LoanCalculationsMixin, ModelFinancialForexMixin,

  description: DS.attr('string')
  fixedRateMonths: DS.attr 'number', {defaultValue: 0}
  identifier: 'liability'
  interestOnly: DS.attr('boolean', {defaultValue: false})
  interestOnlyMonths: DS.attr 'number', {defaultValue: 0}
  interestRate: DS.attr('number')
  limit: DS.attr('number')
  owing: DS.attr('number')
  rate: DS.attr('number')
  remainingMonths: DS.attr('number', defaultValue: 360)
  repayments: DS.attr('number')
  repaymentStartDate: DS.attr()
  toolId: DS.attr('number') #used to link to a tool in the toolkit


#relationships
  bank: DS.belongsTo 'bank', {async: false}
  clients: DS.hasMany 'clients'
  currency: DS.belongsTo 'currency'
  frequency: DS.belongsTo 'frequency', {async: false}
  liabilityType: DS.belongsTo 'liabilityType', {async: false}
  loanStructure: DS.belongsTo 'loanStructure'
  settlement: DS.belongsTo 'settlement'

#computeds
  hasLimit: Ember.computed('liabilityType', ->
    liabilityType = @get('liabilityType.name')
    if ['Credit card', 'Revolving credit', 'Store card', 'Overdraft'].includes(liabilityType)
      return true
      )

  noLimit: Ember.computed.not('hasLimit')

  type: Ember.computed.alias('liabilityType')#used by client.NewFinancial()

  name :Ember.computed('description', 'liabilityType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('description') && @get('liabilityType.name')
      return "#{@get('description')} (#{@get('liabilityType.name')})"
    if @get('liabilityType.name')
      return @get('liabilityType.name')
    else
      return 'Liability'
  )

  paymentIsInterestOnly: Ember.computed('liabilityType', 'interestOnly', ->
    if @get('interestOnly') || @get('liabilityType.name') == 'Revolving credit'
      return true
  )

  repaymentSummary: Ember.computed('paymentIsInterestOnly', 'repayments', 'frequency', ->
    if @get('paymentIsInterestOnly')
      return 'interest only'
    else
      if @get('frequency') && @get('repayments')
        return "#{accounting.formatMoney(@get('repayments'), "$", 2)}/#{@get('frequency.name')}"
    return 'repayments unknown'
    )


  typeSummary: Ember.computed('liabilityType', 'fixedRateMonths', -> #this is used to match pricing offer rates
    type = @get('liabilityType.name')
    months = @get('fixedRateMonths')
    switch type
      when 'Floating home loan' then return 'Floating'
      when 'Revolving credit' then return 'Revolving'
      when 'Fixed home loan'
        if months % 12 == 0
          return "#{months/12}Y"
        else
          return "#{months}M"
      else
        return type
    )

  typeSummary2: Ember.computed('liabilityType', 'fixedRateMonths', -> #this is used to match pricing offer rates
    type = @get('liabilityType.name')
    months = @get('fixedRateMonths')
    switch type
      when 'Floating home loan' then return 'Floating'
      when 'Revolving credit' then return 'Revolving Credit'
      when 'Fixed home loan'
        if months % 12 == 0
          return "Fixed: #{months/12}Y"
        else
          return "Fixed: #{months}M"
      else
        return type
    )



  #TODO: remove this computed
  paymentIsCalculated: Ember.computed('liabilityType', 'interestOnly', ->
    if @get('interestOnly') || @get('liabilityType.name') == 'Revolving credit'
      return true
  )


  requiresFixedTermExpiry: (->
    return false unless @get('liabilityType')
    liabilityType = @get('liabilityType.name')
    switch liabilityType #done this as a switch rather than if, as may need more complexity later
      when 'Fixed home loan' then true
      else false
    ).property 'liabilityType'

  requiresInterestRate: Ember.computed('liabilityType', ->
    if @get('liabilityType.isSecured')
      return true
    if ['Personal loan', 'Other loan'].includes(@get('liabilityType.name'))
      return true
    )


  requiresTerm: Ember.computed('liabilityType', ->
    if ['Personal loan', 'Other loan', 'Floating home loan', 'Fixed home loan'].includes(@get('liabilityType.name'))
      return true
    )


  summary: Ember.computed('liabilityType', 'fixedRateMonths', 'interestRate', ->
    rate = Math.round(@get('interestRate') * 10000) / 100
    unless @get('liabilityType.name') == 'Fixed home loan'
      if @get('interestOnly')
        return "#{@get('liabilityType.name')} @ #{rate}% (int only)"
      else
        return @get('liabilityType.name') + " @ #{rate}%"
    else

      term = parseInt(@get('fixedRateMonths'))
      if term > 23
        term = "#{Math.floor(term/12)}yrs"
        if term % 12 > 0
          term = term + " #{term % 12}mths"
      else
        term = term + ' months'

      if @get('interestOnly')
        return "Fixed @ #{rate}% for #{term} (int only)"
      else
        return "Fixed @ #{rate}% for #{term}"
    )



  termString: Ember.computed('fixedRateMonths', 'requiresFixedTermExpiry', ->
    unless @get('requiresFixedTermExpiry')
      return 'floating'
    termMonths = @get('fixedRateMonths')
    if termMonths < 24
      return termMonths + 'mths'
    else
      mthsAfterYrs = termMonths %12
      mthString =  mthsAfterYrs + 'mths'
      yrString = (termMonths - mthsAfterYrs)/12 + 'yrs '
      if mthsAfterYrs > 0
        return yrString + mthString
      else
        return yrString
    )


  weeklyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    weeklyFrequency = @store.peekAll('frequency').findBy('name', 'Weekly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 52, @get('interestOnly'))
    )

  fortnightlyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    fortnightlyFrequency = @store.peekAll('frequency').findBy('name', 'Fortnightly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 26, @get('interestOnly'))
    )

  monthlyPayment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', ->
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), 12, @get('interestOnly'))
    )


  payment: Ember.computed('owing', 'remainingMonths', 'interestRate', 'interestOnly', 'frequency', ->
    pmtsPerYr = @get('frequency.yearConversion')
    return @calculateLoanRepayment(@get('owing'), @get('remainingMonths'), @get('interestRate'), pmtsPerYr, @get('interestOnly'))
    )



  numberOfInterest: Ember.computed.alias('owing')

  sortOrder: Ember.computed('liabilityType', 'fixedRateMonths', ->
    liabilityType = @get('liabilityType.name')
    switch liabilityType
      when "Revolving credit"
        return 100
      when 'Floating home loan'
        return 200
      when 'Fixed home loan'
        return 300 + @get('fixedRateMonths')
    )


  typeOberver: Ember.observer('liabilityType', ->
    unless @get('liabilityType.isFixed')
      @set('fixedRateMonths', 0)

    )




##functions:
  duplicateFrom: (lia) ->
    lia = lia.get('content') || lia
    @setProperties(
      description: lia.get('description')
      fixedRateMonths: lia.get('fixedRateMonths')
      identifier: lia.get('identifier')
      interestOnly: lia.get('interestOnly')
      interestOnlyMonths: lia.get('interestOnlyMonths')
      interestRate: lia.get('interestRate')
      limit: lia.get('owing')
      owing: lia.get('owing')
      rate: lia.get('rate')
      remainingMonths: lia.get('remainingMonths')
      repayments: lia.get('repayments')
      repaymentStartDate: lia.get('repaymentStartDate')
      toolId: lia.get('toolId')
    #relationships
      bank: lia.get('bank')
      currency: lia.get('currency')
      frequency: lia.get('frequency')
      liabilityType: lia.get('liabilityType')
      loanStructure: lia.get('loanStructure')
      settlement: lia.get('settlement')
    )

  createLiability: ->
    lia = @
    newLia = @store.createRecord('liability',
      description: lia.get('description')
      fixedRateMonths: lia.get('fixedRateMonths')
      identifier: lia.get('identifier')
      interestOnly: lia.get('interestOnly')
      interestOnlyMonths: lia.get('interestOnlyMonths')
      interestRate: lia.get('interestRate')
      limit: lia.get('owing')
      owing: lia.get('owing')
      rate: lia.get('rate')
      remainingMonths: lia.get('remainingMonths')
      repayments: lia.get('repayments')
      repaymentStartDate: lia.get('repaymentStartDate')
      status: "current"
      toolId: lia.get('toolId')
    #relationships
      bank: lia.get('bank')
      currency: lia.get('currency')
      frequency: lia.get('frequency')
      liabilityType: lia.get('liabilityType')
      #loanStructure: lia.get('loanStructure')
      settlement: lia.get('settlement')
    )
    return newLia



  init: ->
    @_super()
    unless @get('frequency')
      monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
      @set('frequency', monthlyFrequency)





)

`export default LoanStructureLiability`
