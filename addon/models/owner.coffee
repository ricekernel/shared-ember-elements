`import DS from 'ember-data'`

Owner = DS.Model.extend(

  documents: DS.hasMany 'documents'
  templates: DS.hasMany 'templates'

)
`export default Owner`
