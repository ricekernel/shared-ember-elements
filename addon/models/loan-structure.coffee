`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


LoanStructure = DS.Model.extend(LoanCalculationsMixin,
  #session: Ember.inject.service('session')
  name: DS.attr('string')
  remainingMonths: DS.attr('number', {defaultValue: 360})
  defaultRate: DS.attr('number', {defaultValue: 0.05})
  defaultPricingComparisonMonths: DS.attr('number', {defaultValue: 60})

  isLocked: DS.attr('boolean', {defaultValue: false}) #we lock a loan structure when a settlement is loaded so a client can't play with it any more

  blob: DS.attr()
  ###
  adviserCommentary: DS.attr() #old commentary property
  applicationValue: DS.attr('number', {defaultValue: 0})
  blob: DS.attr()
  commentary: DS.attr() #new commentary property. Store JSON with 3 objects: purpose, background and recommendation

  documentOrder: DS.attr() #an array listing the order to load related documents into the pdf (by id)
  interestRate: DS.attr('number', {defaultValue: 0.05})
  settlementValue: DS.attr('number')
  settlementDate: DS.attr('date')
  expectedCommission: DS.attr()
  firmPercentage: DS.attr('number') #the calculated proportion of comm due to the firm
  actualCommission: DS.attr()
  generatePdf: DS.attr('boolean', {defaultValue: false}) #when set to true and saved, server creates the pdf
  loanPurpose: DS.attr('string')
  ownerPercentage: DS.attr('number') #the calculated proportion of comm due to the owner
  progress: DS.attr('number')
  secondaryOwnerPercentage: DS.attr('number') #the calculated proportion of comm due to the secondary owner
  secondaryPercentage: DS.attr('number', {defaultValue: 0}) #the share of adviser-apportioned commission the secondary adviser will receive - this is the value that is set

  status: DS.attr('string') #string value for status ('started', 'submitted', 'pre-approved', 'approved', 'unconditional', 'pending settlement', 'settled', 'finished', 'withdrawn')
  term: DS.attr('number', {defaultValue: 360})
  title: DS.attr('string') #compulsory on server
  ###

  toolId: DS.attr('number') #used to link to a tool in the toolkit

#relationships:
  loanApplication: DS.belongsTo 'loanApplication'
  loanApplicationBlob: DS.belongsTo 'loanApplicationBlob', {async: false}

  loanStructureLiabilities: DS.hasMany 'loanStructureLiabilities', {async: false}

  liabilities: DS.hasMany 'liabilities'
  pricingOffers: DS.hasMany 'pricingOffers'

  tools: DS.hasMany 'tools'



  sortedLiabilities:  Ember.computed('liabilities.content.@each.sortOrder', ->
    return @get('liabilities.content').sortBy('sortOrder')
    )
  ###
  pricingOfferRatesObs: Ember.observer('pricingOfferRates.content.@each.sortOrder', ->
    Ember.run.once(this, 'setSortedRates')
  )
  setSortedRates: ->
    sorted =  @get('pricingOfferRates.content').sortBy('sortOrder')
    @set('sortedPricingOfferRates', sorted)
  ###



  offerSortingLifeTime: ['missingRateCount', 'adjLifeTimeCost']
  sortedOffersLifeTime: Ember.computed.sort('pricingOffers.content', 'offerSortingLifeTime')

  offerSortingMidTerm: ['missingRateCount', 'adjMidTermCost']
  sortedOffersMidTerm: Ember.computed.sort('pricingOffers.content', 'offerSortingMidTerm')


  lowestLifeTimeCost: Ember.computed.alias('lowestLifeTimeCostOffer.adjLifeTimeCost')
  lowestLifeTimeCostOffer: Ember.computed('sortedOffersLifeTime.firstObject', ->
    return @get('sortedOffersLifeTime.firstObject')
    )

  lowestMidTermCost: Ember.computed.alias('lowestMidTermCostOffer.adjMidTermCost')
  lowestMidTermCostOffer: Ember.computed('sortedOffersMidTerm.firstObject', ->
    return @get('sortedOffersMidTerm.firstObject')
    )

  totalLending: Ember.computed('liabilities.content.@each.limit', ->
    values = @get('liabilities.content').mapBy('limit')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    if totalValue == 0
      totalValue = '0'
    return totalValue
    )


  totalOwing: Ember.computed('loanStructureLiabilities.@each.owing', ->
    values = @get('loanStructureLiabilities').mapBy('owing')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue
    )
  ###
  totalOwing: Ember.computed('liabilities.content.@each.owing', ->
    values = @get('liabilities.content').mapBy('owing')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    if totalValue == 0
      totalValue = '0'
    return totalValue
    )
  ###

  structureTerms: Ember.computed.mapBy('liabilities.content', 'remainingMonths')
  structureTerm: Ember.computed.max('structureTerms')


  addLoan: ->
    type = @store.peekAll('liabilityType').findBy('name', 'Revolving credit')
    newLiability = @store.createRecord('liability',
      status: 'pending'
      type: type
    )
    if @get('loanApplication')
      laClients = @get('loanApplication.clients')
      if laClients
        newLiability.set('clients', laClients)
    @get('liabilities').pushObject(newLiability)
    if @get('pricingOffers.length') > 0
      @get('pricingOffers').forEach (offer) ->
        offer.addRateForLiability(newLiability)
    return newLiability



  addStructureLoan: (targetTotal)->
    type = @store.peekAll('liabilityType').findBy('name', 'Revolving credit')
    newLiability = @store.createRecord('loanStructureLiability',
      status: 'pending'
      type: type
    )
    if targetTotal > 0
      totalOwing = @get('totalOwing')
      remaining = targetTotal - totalOwing
      newLiability.set('owing', remaining)

    @get('loanStructureLiabilities').pushObject(newLiability)
    return newLiability



  removeLoan: (liability) ->
    liabilityOffers = liability.get('pricingOfferRates.content').toArray()
    liabilityOffers.forEach (rate) ->
      rate.destroyRecord()
    liability.destroyRecord()


  addPricingOffer: ->
    self = @
    newOffer = @store.createRecord('pricingOffer')
    @get('liabilities').forEach (lia) ->
      self.store.createRecord('pricingOfferRate',
        liability: lia
        pricingOffer: newOffer
      )
    @get('pricingOffers').pushObject(newOffer)
    return newOffer


  removePricingOffer: (offer) ->
    @get('pricingOffers').removeObject(offer)
    offer.destroyRecord()


  serialize: (options) ->
    blob = {}
    lias = []
    @get('loanStructureLiabilities').forEach (lia) ->
      jsonLia = lia.serialize(includeId: true)
      lias.pushObject(jsonLia)
    blob.loanStructureLiabilities = lias
    @set('blob', blob)
    return @_super(options)




  didLoad: ->
    self = @
    blob = @get('blob')
    if blob
      if blob.loanStructureLiabilities
        blob.loanStructureLiabilities.forEach (lia) ->
          self.store.pushPayload(lia)
      else
        @get('liabilities').then (lias) ->
          lias.forEach (lia) ->
            loanStructureLiability = self.store.createRecord('loanStructureLiability')
            loanStructureLiability.duplicateFrom(lia)
            self.get('loanStructureLiabilities').pushObject(lia)






)

`export default LoanStructure`
