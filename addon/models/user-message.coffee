`import DS from 'ember-data'`

UserMessage = DS.Model.extend(
  session: Ember.inject.service('session')

  message: DS.attr 'string'
  route: DS.attr 'string' #either a 'http:' route or ember route
  mode: DS.attr 'string' #'force (prevent action), display (blue notify), 'notify' (badge only)
  isRead: DS.attr 'boolean',
    defaultValue: false

  identifier: Ember.computed('id', ->
    if @get('id')
      return @get('id')
    else
      return Math.floor((Math.random() * 10000) + 1)
    )

  followRoute: ->
    messageRoute = @get('route')
    return false unless messageRoute
    unless messageRoute.substring(0,4) == 'http'
      @get('session.applicationController').transitionToRoute(messageRoute)
    else
      window.open(messageRoute, '_blank')
    @set 'isRead', true
    #message.save()

)
`export default UserMessage`
