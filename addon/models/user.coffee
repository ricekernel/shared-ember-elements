`import DS from 'ember-data'`
`import Owner from "./owner"`

User = Owner.extend( # DS.Model.extend(ModelNotifyServerActionsMixin,
  appLogins: DS.attr() #A JSON object storing login count. Eg {crm: {last-login: [date], login-count: 5}, la: {last-login: [date], login-count: 5}. Used by apps to show tutorials
  altModelName: 'user details'
  defaultAssessmentInterestRate: DS.attr('number')
  createdAt: DS.attr()
  email: DS.attr()
  minimumUmi: DS.attr('number')
  name: DS.attr()
  pendingInvites: DS.attr('number') #the number of pending invites a user has from firms
  phone: DS.attr()
  uploadCode: DS.attr()
  userView: DS.attr() #string array of user IDs for the view filter
  userType: DS.attr('string') #0 = admin, 1=customer, 2=adviser

  currentPosting: DS.belongsTo 'brokerPosting'
  firm: DS.belongsTo 'firm'
  logins: DS.hasMany 'appLogins', {async: false}
  userMessages: DS.hasMany 'userMessages'
  userActions: DS.hasMany 'userActions'



#computeds
  ###
  currentPosting: Ember.computed('brokerPostings.[]', ->
    @get('brokerPostings').rejectBy('endDate').findBy('status', 'accepted')
    )
    ###

  ###
  postingInvites: Ember.computed('brokerPostings.@each.status', ->
    @get('brokerPostings').filterBy('status', 'pending')
    )
  ###
  #currentFirm: Ember.computed.alias('currentPosting.firm')

  unreadUserMessages: Ember.computed.filterBy('userMessages.content', 'isRead', false)

  forceUserMessages: Ember.computed.filterBy('unreadUserMessages', 'mode', 'force')
  displayUserMessages: Ember.computed.filterBy('unreadUserMessages', 'mode', 'display')



  createUserMessage: (message, route, mode) ->
    newMessage = @store.createRecord('userMessage',
      message: message
      route: route
      mode: mode
    )
    @get('userMessages').pushObject(newMessage)
    return newMessage


  ###
  getCurrentFirm: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      self.get('currentPosting').then ((posting) ->
        #currentPosting = self.get('currentPosting')
        posting.get('firm').then ((firm) ->
          resolve firm
          ), (error) ->
            reject error
        #currentPosting
        ), (error) ->
          reject error
      )
      ###



  registerLogin: (app) ->
    self = @
    if @get('appLogins').logins
      appLogins = @get('appLogins').logins
      appLogins.forEach (login) ->
        if login.data.id
          self.store.pushPayload(login)

    thisAppLogin = @get('logins').findBy('app', app)
    unless thisAppLogin
      thisAppLogin = @store.createRecord('appLogin',
        app: app
        user: this
        lastLogin: null
        loginCount: 0
      )
      @get('logins').pushObject(thisAppLogin)

    today = moment(new Date()).format('DDMMYY')
    unless thisAppLogin.get('lastLogin') == today
      loginCount = thisAppLogin.get('loginCount') + 1
      thisAppLogin.set('lastLogin', today)
      thisAppLogin.set('loginCount', loginCount)
      @save()


  save: ->
    @serializeLogins()
    return @_super()


  serializeLogins: ->
    loginData = {}
    loginArray = []
    @get('logins').forEach (login) ->
      loginJSON = login.serialize(includeId: true)
      loginArray.pushObject(loginJSON)
    loginData.logins = loginArray
    @set('appLogins', loginData)

)
`export default User`
