`import DS from 'ember-data'`


LoanPartyLiabilityLink = DS.Model.extend(
  session: Ember.inject.service('session')

  liabilitySettlementAction: DS.attr 'string', {defaultValue: 'Kept (will refinance if lender changes)'}
  liabilitySettlementActions: ['', 'Kept (will not refinance)', 'Kept (will refinance if lender changes)', 'Fully Repaid', 'Partially Repaid']


  willRefinance: DS.attr("boolean", {defaultValue: true})

  #willRefinance changed from computed to attribute on 180617. This migration to be removed:
  #TODO: Remove on 180618
  didLoad: ->
    @_super()
    if @get('liabilitySettlementAction') == 'Kept (will refinance if lender changes)'
      @set('willRefinance', true)


  settlementActionObs: Ember.observer('willRefinance', ->
    if @get('willRefinance')
      @set('liabilitySettlementAction', 'Kept (will refinance if lender changes)')
    else
      @set('liabilitySettlementAction', 'Kept (will not refinance)')
    )



  willKeep: Ember.computed.equal('liabilitySettlementAction', 'Kept (will not refinance)')
  #willRefinance: Ember.computed.equal('liabilitySettlementAction', 'Kept (will refinance if lender changes)')
  willRepay: Ember.computed.equal('liabilitySettlementAction', 'Fully Repaid')
  willPartialRepay: Ember.computed.equal('liabilitySettlementAction', 'Partially Repaid')

  limitAfterDrawDown: DS.attr('number')

#relationships:

  liability: DS.belongsTo 'liability'
  loanParty: DS.belongsTo 'loanParty', {async: false}


#obs
  liabilitySettlementActionObs: Ember.observer('liabilitySettlementAction', ->
    unless @get('isDeleted')
      if @get('willRepay')
        @set('limitAfterDrawDown', 0)

    )


  liabilityIdObs: Ember.observer('liability.id', ->
    #removes any linkages for liabilities that have been deleted in the CRM
    self = @
    if @get('liability.id') == undefined && @get('loanParty.id')
      self.get('loanParty.loanPartyLiabilityLinks').removeObject(self)
    )

  liabilityIsSecuredObserver: Ember.observer('liability.liabilityType.isSecured', ->
    if !@get('liability.liabilityType.isSecured') && @get('liabilitySettlementAction') == 'Kept (will refinance if lender changes)'
      @set('liabilitySettlementAction', 'Kept (will not refinance)')
    )



)

`export default LoanPartyLiabilityLink`
