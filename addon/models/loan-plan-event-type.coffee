`import DS from 'ember-data'`

LoanPlanEventType = DS.Model.extend (
  description: DS.attr 'string'
  eventOrder: DS.attr 'number'
  iconPath: DS.attr 'string'
  name: DS.attr 'string'
  notes: DS.attr 'string'
)


`export default LoanPlanEventType`
