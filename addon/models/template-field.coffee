`import DS from 'ember-data'`

TemplateField = DS.Model.extend (
  baseModel: DS.attr() #model name required for this TemplateField. eg('individual')
  fieldType: DS.attr()
  name: DS.attr()
  supportModel: DS.attr()
)


`export default TemplateField`
