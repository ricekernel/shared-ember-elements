`import DS from 'ember-data'`

BrokerPosting = DS.Model.extend (
  email: DS.attr 'string' #used to create a posting for the purpose of inviting new team member
  endDate: DS.attr 'date'
  firmRetentionRate: DS.attr 'number' #only used if overriding the firm.commissionRetentionRate.  null to not override
  name: DS.attr 'string' #full name
  postingType: DS.attr 'string', #:admin, :member  - defines if this user is an admin on this firm
    defaultValue: 'member'
  startDate: DS.attr 'date'
  status: DS.attr 'string'


  firm: DS.belongsTo 'firm'
  user: DS.belongsTo 'user'

  isAdmin: Ember.computed('postingType',
    get: (key) ->
      if @get('postingType') == 'admin'
        return true
      else
        return false

    set: (key, value) ->
      if value == true
        @set 'postingType', 'admin'
      else
        @set 'postingType', 'member'
      @save()
      return value
    )
  startDateMoment: Ember.computed('startDate', ->
    return moment(@get('startDate')).format('DD MMM YYYY')

  )

  isCurrent: Ember.computed('startDate', 'endDate', 'status', ->
    return false unless @get('status') == 'accepted'
    if moment(@get('startDate')).isBefore(moment(new Date))
      if moment(@get('endDate')).isAfter(moment(new Date)) or @get('endDate') == null
        return true
    )

  isPending: Ember.computed.equal('status', 'pending')

)


`export default BrokerPosting`
