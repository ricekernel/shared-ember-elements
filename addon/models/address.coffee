`import DS from 'ember-data'`

Address = DS.Model.extend {
  identifier: 'address'
  street: DS.attr 'string'
  suburb: DS.attr 'string'
  city: DS.attr 'string'
  country: DS.attr 'string'
  postCode: DS.attr 'string'

  #relationships:
  security: DS.belongsTo 'security'

  client: DS.belongsTo 'client', {inverse: null}
  #previousClients: DS.hasMany 'clients'
  #currentClients: DS.hasMany 'clients'

  abbreviatedAddress: Ember.computed('street', 'suburb', 'city', 'country', ->
    self = @
    addressArray = []
    ['street', 'suburb', 'city'].forEach (addressSegment) ->
      if self.get(addressSegment)
        addressArray.pushObject(" #{self.get(addressSegment)}")

    if self.get('country')
      nzArray = ['nz', ' nz', 'new zealand', ' new zealand']
      unless nzArray.includes(self.get('country').toLowerCase())
        addressArray.pushObject(" #{self.get('country')}")

    if addressArray.length > 0
      return addressArray.toString()
    else
      return 'Address unknown'
    )




  copyAddress: ->
    copiedAddress = @store.createRecord('address',
      street: @get('street')
      suburb: @get('suburb')
      city: @get('city')
      country: @get('country')
      postCode: @get('postCode')
    )
    return copiedAddress

}

`export default Address`
