`import DS from 'ember-data'`

AssetType = DS.Model.extend {
  name: DS.attr 'string'
}

`export default AssetType`
