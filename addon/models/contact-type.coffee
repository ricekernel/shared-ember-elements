`import DS from 'ember-data'`

ContactType = DS.Model.extend
  name: DS.attr 'string'
  placeholder: Ember.computed('name', ->
    @get('name').toLowerCase()
    )

`export default ContactType`
