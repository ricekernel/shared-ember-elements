`import DS from 'ember-data'`

DSdate = DS.attr "date"

TaxYear = DS.Model.extend (
  name: DS.attr()
  startDate: DSdate
  endDate: DSdate
  taxBands: DS.hasMany 'taxBands'

)


`export default TaxYear`
