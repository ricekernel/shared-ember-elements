`import DS from 'ember-data'`

CommissionRate = DS.Model.extend(

  flatFee: DS.attr 'number' #if a flat fee is paid
  name: DS.attr 'string' #to identify in dropdowns
  upFrontRate: DS.attr 'number' #a decimal representing the upfront commission rate paid by the bank
  trailRate: DS.attr 'number' #if the bank pays commission as trail, the (trail rate)

  firm: DS.belongsTo 'firm'
  bank: DS.belongsTo 'bank', {async: false}

  #With trail - there are two rates. A lower upfront rate, and a trail rate.  This will actually create two commission entries when loading a loan in a settlement

)

`export default CommissionRate`
