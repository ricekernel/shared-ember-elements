`import DS from 'ember-data'`

AppLogin = DS.Model.extend(
  #must use underscore to stop problems when saving the JSON
  app: DS.attr('string') #['crm', 'la', 'toolkit', 'portal']
  lastLogin: DS.attr()
  loginCount: DS.attr('number')
  tutorialRun: DS.attr('boolean', {defaultValue: false})
  lastestReleaseNotified: DS.attr('number')
  customData: DS.attr()
  user: DS.belongsTo 'user', {async: false}

)
`export default AppLogin`
