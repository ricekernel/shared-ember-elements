`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`
#`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


LoanApplication = DS.Model.extend(LoanCalculationsMixin,
  session: Ember.inject.service('session')

  statusProxy: null #this used for editing loan state so it doesn't affect table sorts until after saving record

  adviserCommentary: DS.attr() #old commentary property
  applicationValue: DS.attr('number', {defaultValue: 0})
  blob: DS.attr()
  commentary: DS.attr() #new commentary property. Store JSON with 3 objects: purpose, background and recommendation
  createdAt: DS.attr()
  documentOrder: DS.attr() #an array listing the order to load related documents into the pdf (by id)
  interestRate: DS.attr('number', {defaultValue: 0.05})
  settlementValue: DS.attr('number')
  settlementDate: DS.attr('date')
  expectedCommission: DS.attr()
  firmPercentage: DS.attr('number') #the calculated proportion of comm due to the firm
  actualCommission: DS.attr()
  generatePdf: DS.attr('boolean', {defaultValue: false}) #when set to true and saved, server creates the pdf
  loanPurpose: DS.attr('string')
  loanPurposes: ['', 'New Loan', 'Topup', 'Refinance']
  ownerPercentage: DS.attr('number') #the calculated proportion of comm due to the owner
  progress: DS.attr('number')
  secondaryOwnerPercentage: DS.attr('number') #the calculated proportion of comm due to the secondary owner
  secondaryPercentage: DS.attr('number', {defaultValue: 0}) #the share of adviser-apportioned commission the secondary adviser will receive - this is the value that is set

  status: DS.attr 'enum', {caps: 'all'} #string value for status ('started', 'submitted', 'pre approved', 'approved', 'unconditional', 'pending settlement', 'settled', 'finished', 'withdrawn', 'declined', 'deferred')
  statusOptions: ['Started', 'Submitted', 'Pre Approved', 'Approved', 'Unconditional', 'Pending Settlement', 'Settled', 'Finished', 'Withdrawn', 'Declined', 'Deferred']
  term: DS.attr('number', {defaultValue: 360})
  title: DS.attr('string') #compulsory on server
  updatedAt: DS.attr()



  #new fields:
  isLocked: DS.attr('boolean', {defaultValue: false}) #when adviser has locked it so it cant be edited
  hasForex: DS.attr('boolean', {defaultValue: false}) #used to turn on forex rates for this application




#These attributes set by observers so they get sent to db:

  totalAssets: DS.attr('number', {defaultValue: 0})
  totalExpenses: DS.attr('number', {defaultValue: 0})
  totalIncomes: DS.attr('number', {defaultValue: 0})
  totalLiabilities: DS.attr('number', {defaultValue: 0})
  totalLiabilitiesBalance: DS.attr('number', {defaultValue: 0})
  totalCurrentSecurities: DS.attr('number', {defaultValue: 0})
  totalProposedSecurities: DS.attr('number', {defaultValue: 0})
  totalRefinancingSecurities: DS.attr('number', {defaultValue: 0})
  totalSecurityProposition: DS.attr('number', {defaultValue: 0})
  currentPosition: DS.attr('number', {defaultValue: 0})
  estimatedMonthlyRepayment: DS.attr('number', {defaultValue: 0}) #this needs to be set as an attr so persisted to server for pdf
  proposedLvr: DS.attr('number', {defaultValue: 0})
  residualIncome: DS.attr('number', {defaultValue: 0})
  umi: DS.attr('number', {defaultValue: 0})




#relationships:
  activities: DS.hasMany 'activities'
  assets: DS.hasMany 'assets'
  approvals: DS.hasMany 'approvals'
  clients: DS.hasMany 'clients'
  #borrowers: DS.hasMany 'clients', {inverse: 'borrowerLoanApplications'}
  documents: DS.hasMany 'documents'
  expenses: DS.hasMany 'expenses'
  firm: DS.belongsTo 'firm'
  #guarantors: DS.hasMany 'clients', {inverse: 'guarantorLoanApplications'}
  incomes: DS.hasMany 'incomes'
  liabilities: DS.hasMany 'liabilities'

  loanStructures: DS.hasMany 'loanStructures'
  owner: DS.belongsTo 'user'
  pdfDocument: DS.belongsTo 'document', {inverse: null}
  secondaryOwner: DS.belongsTo 'user'
  #securities: DS.hasMany 'assets'
  settlementBank: DS.belongsTo 'bank', {async: false}

  settlements: DS.hasMany 'settlements'

  loanParties: DS.hasMany('loanParties'), {async: false}
  loanApplicationBlob: DS.belongsTo 'loanApplicationBlob', {async: false}


  loanStructure: Ember.computed.alias('loanApplicationBlob.loanStructure.content')



#computeds:

  borrowerParty: Ember.computed('loanParties.@each.partyType', ->
    @get('loanParties').findBy('partyType', 'Borrower')
    )

  isApproved: Ember.computed.equal('status', 'Approved')
  isPendingSettlement: Ember.computed.equal('status', 'Pending Settlement')
  isSettled: Ember.computed.equal('status', 'Settled')

  name: Ember.computed.alias('title') #used for dropdowns, such as document owner in message-add-attachments


  securities: Ember.computed('assets.content.@each.isSecurity', ->
    return @get('assets.content').filterBy('isSecurity')
    )

  totalSettlementValue: Ember.computed('settlements.content.@each.settlementValue', ->
    values = @get('settlements.content').mapBy('settlementValue')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue
    )

  totalSecurities: Ember.computed('securities.@each.value', ->
    values = @get('securities').mapBy('value')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue
    )

  applicationType: Ember.computed.alias('blob.loanAppBlob.data.attributes.application_type')


  reportingValue: Ember.computed('loanPurpose', 'applicationValue', ->
    unless @get('loanPurpose') == 'Refinance'
      return @get('applicationValue')
    else
      refiVal = @get('blob.loanAppBlob.data.attributes.refinance_value') || 0
      return parseFloat( refiVal ) + parseFloat( @get('applicationValue') )
    )

  didLoad: ->
    @_super()
    unless @get('loanPurpose')
      applicationType = @get('blob.loanAppBlob.data.attributes.application_type')
      if applicationType
        if @get('loanPurposes').includes(applicationType)
          @set('loanPurpose', applicationType)
        if applicationType == 'New Lending - Existing Lending/Securities (includes Buy + Sell)'
          @set('loanPurpose', "New Loan")
        if applicationType == 'New Lending - No Existing Lending/Securities'
          @set('loanPurpose', "New Loan")
        if applicationType == 'Topup or Refinance - No New Securities' && @get('applicationValue') < 1
          @set('loanPurpose', "New Loan")
        if applicationType == 'Topup or Refinance - No New Securities' && @get('applicationValue') > 1
          @set('loanPurpose', "Topup")

      else
        @set('loanPurpose', "New Loan")
      @save()



  save: ->
    if @get('unblobbed') && !@get('isDeleted')
      @util_BlobBlob()
    return @_super()



  addProposedSecurity: (security) ->
    unless security
      houseType = @store.peekAll('securityType').findBy('name', "House")
      ownerOccupied = @store.peekAll('securityUseType').findBy('name', "Owner occupied")
      propertyType = @store.peekAll('assetType').findBy("name", "Property")
      newAddress = @store.createRecord('address')
      security = @store.createRecord('asset',
        assetType: propertyType
        isProposed: true
        isSecurity: true
        address: newAddress
        securityType: houseType
        securityUseType: ownerOccupied
      )
      security.get('loanApplications').pushObject(this)
    clients = @get('clients')
    security.set('clients', clients)
    #@get('securities').pushObject(security)
    return security


  util_BlobBlob: ->
    self = @
    blob = {}
    loanAppBlob = @get('loanApplicationBlob')
    if loanAppBlob
      JSONBlob = loanAppBlob.serialize(includeId: true)
      blob.loanAppBlob = JSONBlob


    if @get('loanParties.length') > 0
      parties = []
      links = []
      @get('loanParties').forEach (party) ->
        party.get('loanPartyLiabilityLinks').forEach (link) ->
          unless link.get('isDeleted')
            if link.get('liability')
              linkJSON = link.serialize(includeId: true)
              links.pushObject(linkJSON)
        party.get('loanPartySecurityLinks').forEach (link) ->
          unless link.get('isDeleted')
            if link.get('security')
              linkJSON = link.serialize(includeId: true)
              links.pushObject(linkJSON)

        JSONParty = party.serialize(includeId: true)
        parties.pushObject(JSONParty)

      blob.parties = parties
      blob.links = links
    self.set('blob', blob)




  util_UnblobBlob: ->
    self = @
    @set('unblobbed', true)
    if @get('loanApplicationBlob')
      return false

    blob = @get('blob')
    if blob.loanAppBlob
      self.store.pushPayload(blob.loanAppBlob)
      blobInStore = self.store.peekAll('loanApplicationBlob').findBy('id', blob.loanAppBlob.data.id)
      self.set('loanApplicationBlob', blobInStore)


      #TODO remove on 200518 this temp migration from when structures belonged to a loan app
      #if blobInStore.get()
      blobInStore.get('loanStructure').then (structure) ->
        unless blobInStore.get('loanStructure.id')
          self.get('loanStructures').then (structures) ->
            if structures.get('length') > 0
              blobInStore.set('loanStructure', self.get('loanStructures.firstObject'))
            else
              self.util_CreateNewLoanStructure()


    else
      @util_MigrateToFlowModel()
      return false


    if blob.parties
      blob.parties.forEach (party) ->
        self.store.pushPayload(party)
        partyInStore = self.store.peekAll('loanParty').findBy('id', party.data.id)
        self.get('loanParties').pushObject(partyInStore)

      if blob.links
        blob.links.forEach (link) ->
          if link.data.id
            relatedId = link.data.relationships.security || link.data.relationships.liability
            if relatedId
              if relatedId.data
                if relatedId.data.id
                  self.store.pushPayload(link)




  util_MigrateToFlowModel: ->

    self = @

    #create the blob model that we now keep some data in (mainly for the funding bit)
    newBlob = self.store.createRecord('loanApplicationBlob',
      loanApplication: self
      applicationType: 'New Lending - Existing Lending/Securities (includes Buy + Sell)'
      existingSecurityValue: self.get('totalCurrentSecurities')
      proposedSecurityValue: self.get('totalProposedSecurities')
      existingLendingValue: self.get('totalLiabilitiesBalance')
    )
    self.set('loanApplicationBlob', newBlob)

    self.util_CreateNewLoanStructure()

    #create a party for the borrowers
    newParty = self.store.createRecord('loanParty',
      loanApplication: self
      partyType: 'Borrower'
    )

    @set('session.selectedParty', newParty)

    self.get('loanParties').pushObject(newParty)

    self.get('clients').then (clients) ->
      newParty.get('clients').pushObjects(clients)

      clients.forEach (client) ->
        client.get('assets').then (assets) ->
          newParty.get('assets').pushObjects(assets)

        client.get('expenses').then (expenses) ->
          newParty.get('expenses').pushObjects(expenses)

        client.get('incomes').then (incomes) ->
          newParty.get('incomes').pushObjects(incomes)

        client.get('liabilities').then (lias) ->
          lias.forEach (lia) ->
            newLink = self.store.createRecord('loanPartyLiabilityLink',
              loanParty: newParty
              liability: lia
            )
            newParty.get('loanPartyLiabilityLinks').pushObject(newLink)
        ###
        client.get('securities').then (secs) ->
          secs.forEach (sec) ->
            unless sec.get('isProposed')
              newLink = self.store.createRecord('loanPartySecurityLink',
                loanParty: newParty
                security: sec
              )

              newParty.get('loanPartySecurityLinks').pushObject(newLink)
              ###

    #get all the financials out and link them to the party.  Should only be left with the proposed securities
    liabilitiesToRemove = []
    #assetsToRemove = []
    incomesToRemove = []
    expensesToRemove = []
    #securitiesToRemove = []

    ###
    self.get('securities').then (securities) ->
      securities.forEach (security) ->
        unless security.get('isProposed')
          newSecLink = self.store.createRecord('loanPartySecurityLink',
            security: security
            loanParty: newParty
            securitySettlementAction: 'Kept (will refinance if lender changes)'
          )
          newParty.get('loanPartySecurityLinks').pushObject(newSecLink)
          securitiesToRemove.pushObject(security)
      securitiesToRemove.forEach (security) ->
        self.get('securities').removeObject(security)
    ###


    self.get('liabilities').then (liabilities) ->
      liabilities.forEach (lia) ->
        liabilitiesToRemove.pushObject(lia)
        if lia.get('status') == 'current'
          newLiaLink = self.store.createRecord('loanPartyLiabilityLink',
            liability: lia
            loanParty: newParty

            limitAfterDrawDown: lia.get('limitAfterDrawDown')
          )
          if lia.get('willRefinance')
            newLiaLink.set('liabilitySettlementAction', 'Kept (will refinance if lender changes)')
          else
            newLiaLink.set('liabilitySettlementAction', 'Partially Repaid')

          newParty.get('loanPartyLiabilityLinks').pushObject(newLiaLink)



    self.get('expenses').then (expenses) ->
      expenses.forEach (exp) ->
        newParty.get('expenses').pushObject(exp)
        expensesToRemove.pushObject(exp)
      expensesToRemove.forEach (exp) ->
        self.get('expenses').removeObject(exp)

    self.get('incomes').then (incomes) ->
      incomes.forEach (inc) ->
        newParty.get('incomes').pushObject(inc)
        incomesToRemove.pushObject(inc)
      incomesToRemove.forEach (inc) ->
        self.get('incomes').removeObject(inc)






  setupApp: ->
    self = @
    self.set('unblobbed', true)
    return new Ember.RSVP.Promise((resolve, reject) ->
      self.save().then ((la) ->
        self.util_CreateNewParty()
        self.util_CreateNewBlob()
        self.util_CreateNewLoanStructure()
        self.save().then ((savedLa) ->
          resolve self
        ), (error) ->
          reject error
        ), (error) ->
          reject error
      )





  util_CreateNewBlob: ->
    newBlob = @store.createRecord('loanApplicationBlob')
    @set('loanApplicationBlob', newBlob)
    return newBlob


  util_CreateNewParty: ->
    newParty = @store.createRecord('loanParty',
      partyType: 'Borrower'
    )
    @get('loanParties').pushObject(newParty)
    return newParty


  util_CreateNewLoanStructure: ->
    self = @
    newStructure = self.store.createRecord('loanStructure',
      loanApplication: self
    )
    self.set('loanApplicationBlob.loanStructure', newStructure)

    ###
    return new Ember.RSVP.Promise((resolve, reject) ->
      loanApplicationBlob = self.get('loanApplicationBlob')
      newStructure = self.store.createRecord('loanStructure',
        loanApplication: self
      )
      self.get('loanStructures').pushObject(newStructure)
      newStructure.save().then (->
        loanApplicationBlob.set('loanStructure', newStructure)
        resolve newStructure
        ), (error) ->
          reject error
    )
    ###



)

`export default LoanApplication`
