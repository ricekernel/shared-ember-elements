`import DS from 'ember-data'`


LoanPartySecurityLink = DS.Model.extend(
  session: Ember.inject.service('session')

  securitySettlementAction: DS.attr('string', {defaultValue: 'Kept (will refinance if lender changes)'})
  securitySettlementActions: ["", 'Kept (will not refinance)', 'Kept (will refinance if lender changes)', 'Sold']
  #willRefinance: Ember.computed.equal('securitySettlementAction', 'Kept (will refinance if lender changes)')
  #willSell: Ember.computed.equal('securitySettlementAction', 'Sold')

  willSell: DS.attr("boolean", {defaultValue: false})
  willRefinance: DS.attr("boolean", {defaultValue: true})

  #willRefinance changed from computed to attribute on 180617. This migration to be removed:
  #TODO: Remove on 180618
  didLoad: ->
    @_super()
    if @get('securitySettlementAction') == 'Kept (will refinance if lender changes)'
      @set('willRefinance', true)
    if @get('securitySettlementAction') == 'Sold'
      @set('willSell', true)



  settlementActionObs: Ember.observer('willRefinance', 'willSell', ->
    if @get('willSell')
      @set('securitySettlementAction', "Sold")
      return false
    if @get('willRefinance')
      @set('securitySettlementAction', 'Kept (will refinance if lender changes)')
    else
      @set('securitySettlementAction', 'Kept (will not refinance)')
    )

#relationships:

  #security: DS.belongsTo 'security'

  loanParty: DS.belongsTo 'loanParty'
  security: DS.belongsTo 'asset'

  ###
  securityIdObs: Ember.observer('security.id', ->
    #removes any linkages for securities that have been deleted in the CRM
    self = @
    if @get('security.id') == undefined
      self.get('loanParty.loanPartySecurityLinks').removeObject(self)
    )
  ###

)

`export default LoanPartySecurityLink`
