`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


pricingOfferRate = DS.Model.extend(LoanCalculationsMixin,
  #session: Ember.inject.service('session')
  #elementType: DS.attr('string') #rate, cash, discount, other - one off, other - recurring
  interestRate: DS.attr('number')

  #fixedRateMonths: DS.attr('number'), defaultValue: 12


#For Pricing offers in Loan app.  Will need to revisit how this works for toolkit:
  liabilityType: DS.belongsTo 'liabilityType', {async: false}
  interestRateRequested: DS.attr('number')
  value: DS.attr('number') #the proposed value of this portion
  fixedRateMonths: DS.attr 'number', {defaultValue: 0}
  updatedAt: DS.attr()



  liabilityTypeAndTermAbbreviation: Ember.computed('liabilityType', 'fixedRateMonths', ->
    type = @get('liabilityType.name')
    fixedRateMonths = @get('fixedRateMonths')
    fixedRateYears = Math.floor((fixedRateMonths / 12))
    fixedRateRemainingMonths = fixedRateMonths % fixedRateYears

    switch type
      when 'Floating home loan' then return 'Floating'

      when 'Fixed home loan'
        if fixedRateMonths < 24
          return "Fixed: #{fixedRateMonths}M"
        else
          if fixedRateRemainingMonths > 0
            return "Fixed: #{fixedRateYears}Y #{fixedRateRemainingMonths}M"
          else
            return "Fixed: #{fixedRateYears}Y"
      else
        return type
    )


  ###



  adviserCommentary: DS.attr() #old commentary property
  applicationValue: DS.attr('number', {defaultValue: 0})
  blob: DS.attr()
  commentary: DS.attr() #new commentary property. Store JSON with 3 objects: purpose, background and recommendation

  documentOrder: DS.attr() #an array listing the order to load related documents into the pdf (by id)
  interestRate: DS.attr('number', {defaultValue: 0.05})
  settlementValue: DS.attr('number')
  settlementDate: DS.attr('date')
  expectedCommission: DS.attr()
  firmPercentage: DS.attr('number') #the calculated proportion of comm due to the firm
  actualCommission: DS.attr()
  generatePdf: DS.attr('boolean', {defaultValue: false}) #when set to true and saved, server creates the pdf
  loanPurpose: DS.attr('string')
  ownerPercentage: DS.attr('number') #the calculated proportion of comm due to the owner
  progress: DS.attr('number')
  secondaryOwnerPercentage: DS.attr('number') #the calculated proportion of comm due to the secondary owner
  secondaryPercentage: DS.attr('number', {defaultValue: 0}) #the share of adviser-apportioned commission the secondary adviser will receive - this is the value that is set

  status: DS.attr('string') #string value for status ('started', 'submitted', 'pre-approved', 'approved', 'unconditional', 'pending settlement', 'settled', 'finished', 'withdrawn')
  term: DS.attr('number', {defaultValue: 360})
  title: DS.attr('string') #compulsory on server
  ###



#relationships:
  #frequency: DS.belongsTo 'frequency'
  loanStructureLiability: DS.belongsTo 'loanStructureLiability'
  liability: DS.belongsTo 'liability'
  pricingOffer: DS.belongsTo 'pricingOffer'


#computeds:
  limit: Ember.computed.alias('liability.limit')
  fixedInterestMonths: Ember.computed.alias('liability.fixedInterestMonths')
  #liabilityType: Ember.computed.alias('liability.liabilityType')

  typeSummary: Ember.computed('liabilityType', 'fixedRateMonths', ->
    #used to match rates with terms in structure and offer comparator
    type = @get('liabilityType.name')
    months = @get('fixedRateMonths')
    switch type
      when 'Floating home loan' then return 'Floating'
      when 'Revolving credit' then return 'Revolving'
      when 'Fixed home loan'
        if months % 12 == 0
          return "#{months/12}Y"
        else
          return "#{months}M"
      else
        return type
    )


  termSummary: Ember.computed('liabilityType', 'fixedRateMonths', ->
    liabilityType = @get('liabilityType.name')
    fixedRateMonths = @get('fixedRateMonths')
    if fixedRateMonths < 19
      term = fixedRateMonths + 'M'
    else
      term = fixedRateMonths / 12 + 'Y'
    switch liabilityType
      when 'Revolving credit' then return liabilityType
      when 'Floating home loan' then return 'Floating'
      when 'Fixed home loan' then return "Fixed: #{term}"
      else return liabilityType
    )

  liaObs: Ember.observer('liability.liabilityType', ->
    liaType = @get('liability.liabilityType')
    @set('liabilityType', liaType)
    )

  sortOrder: Ember.computed('liabilityType', 'fixedRateMonths', ->
    liabilityType = @get('liabilityType.name')
    switch liabilityType
      when "Revolving credit"
        return 100
      when 'Floating home loan'
        return 200
      when 'Fixed home loan'
        return 300 + @get('fixedRateMonths')
    )
)

`export default pricingOfferRate`
