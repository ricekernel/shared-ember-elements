`import DS from 'ember-data'`

IncomeType = DS.Model.extend
  name: DS.attr 'string'

  isTaxedAtPersonalRate: Ember.computed('name', ->
    name = @get('name')
    if ['Salary/Wage', 'Interest', 'Dividends', 'Pension/Benefit', 'Self employed', 'Overtime/Allowances'].includes(name)
      return true
    )

  isRent: Ember.computed('name', ->
    if @get('name').includes('Rent')
      return true
    )

  isBoard: Ember.computed('name', ->
    if @get('name').includes('Board')
      return true
    )

  sortOrder: Ember.computed('name', ->
    name = @get("name")
    switch name
      when 'Salary/Wage' then return 100
      when 'Self employed' then return 200
      when 'Pension/Benefit' then return 300
      when 'Overtime/Allowances' then return 400
      when 'Commission' then return 500
      when 'Interest' then return 600
      when 'Dividends' then return 700
      when 'Rent (Current)' then return 800
      when 'Rent (Proposed)' then return 900
      when 'Board (Room only)' then return 1000
      when 'Board (All inclusive)' then return 1100
      when 'Other' then return 1200
      else return 1300
    )


`export default IncomeType`
