`import DS from 'ember-data'`

TaxBand = DS.Model.extend (
  name: DS.attr()
  lower: DS.attr()
  lowerNet: DS.attr()
  upper: DS.attr()
  rate: DS.attr()
)


`export default TaxBand`
