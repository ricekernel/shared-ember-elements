`import DS from 'ember-data'`


Settlement = DS.Model.extend(

  ###
  A settlement has a description, a value, a date and a state.

  It belongs to a bank
  It belongs to a commissionRate (user can set rate - this determines the commissions created per loan attached to this settlement)
  It has many liabilities and securities (new ones)
  It belongs to a loan application

  A user creates a settlement then ->
    selects the bank
    edits the commission rate (default bank rate to start)
    edits the split if necessary (default firm split to start)
    adds loans
     -> for each loan, one or more commission entries are created, based on the commission rate
    adds securities
  ###



  description: DS.attr() #in case there are multiple settlements and the adviser needs to differentiate
  settlementValue: DS.attr('number') #total value of loans settling (defaults to application value when created)
  settlementDate: DS.attr('date') #date settlement will/did occur
  status: DS.attr 'enum', {caps: 'first'} #pending, :settled, :cancelled

  approval: DS.belongsTo 'approval'
  assets: DS.hasMany 'assets'
  bank: DS.belongsTo 'bank', {async: false}  #might be able to remove this as linked to approval that has a bank
  liabilities: DS.hasMany 'liabilities'
  #securities: DS.hasMany 'securities'
  #securities: DS.hasMany 'assets'
  #loanStructure: DS.belongsTo 'loanStructure'
  loanApplication: DS.belongsTo 'loanApplication'

  userActions: DS.hasMany('userActions')


  securities: Ember.computed.alias('assets')



#computeds:

  isSettled: Ember.computed.equal('status', 'Settled')

  settlementMomentFromNow: Ember.computed('settlementDate', ->
    settlementDate = moment(@get('settlementDate'))
    settlementDate.startOf('day')
    today = moment()
    today.startOf('day')
    return settlementDate.from(today)
    )


  

  totalSettlementSecurities: Ember.computed('securities.@each.value', ->
    values = @get('securities').mapBy('value')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a) + parseFloat(b)).toFixed(2)
      ), 0
    return totalValue
    )




)


`export default Settlement`
