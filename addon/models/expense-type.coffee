`import DS from 'ember-data'`

ExpenseType = DS.Model.extend
  name: DS.attr()

  sortOrder: Ember.computed('name', ->
    name = @get('name')
    switch name
      when 'Household' then 10
      when 'Utilities' then 20
      when 'Rates' then 30
      when 'Food' then 40
      when 'Clothing' then 50
      when 'Entertainment' then 55
      when 'Vehicle' then 60
      when 'Healthcare' then 70
      when 'Education' then 80
      when 'Superannuation' then 90
      when 'Childcare' then 100
      when 'Child support' then 110
      when 'Home insurance' then 120
      when 'Life insurance' then 130
      when 'Other insurance' then 140
      when 'Body corp' then 150
      when 'Debt servicing' then 160
      when 'Rent' then 170
      when 'Board' then 180
      when 'Other' then 190
      else 300
  )


`export default ExpenseType`
