`import DS from 'ember-data'`

Referral = DS.Model.extend {
  createdAt: DS.attr 'date'
  customerName: DS.attr 'string'
  customerPhone: DS.attr 'string'
  customerMobile: DS.attr 'string'
  customerEmail: DS.attr 'string'
  comments: DS.attr 'string'
  value: DS.attr 'number'
  location: DS.attr 'string'


  firm: DS.belongsTo 'firm'
  # user: DS.belongsTo 'user'
  client: DS.belongsTo 'client'
  referrer: DS.belongsTo 'referrer'


  pickup: ->
    host = @store.adapterFor('application').get 'host'
    namespace = @store.adapterFor('application').get 'namespace'
    headers = @store.adapterFor('application').get 'headers'
    url = "#{host}/#{namespace}/referrals/#{@get('id')}/pickup"
    self = @
    return new Ember.RSVP.Promise (resolve, reject)->
      $.ajax
        type: 'POST'
        url: url
        headers: headers
        success: (data)->
          self.setProperties data.referral
          resolve data
        error: (data)->
          reject data
}

`export default Referral`
