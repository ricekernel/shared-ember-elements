`import DS from 'ember-data'`

LoanPlan = DS.Model.extend (
  name: DS.attr() #user can give each plan a name if they have multiples

  isPrimary: DS.attr 'boolean', {defaultValue: false} #if you have multiple plans, you can assign one as primary/base (to compare others to!)
  toolId: DS.attr 'number' #stores the ID of the tool it belongs to for finding by the loanplanner route
  blob: DS.attr()

  runningBalancesBaseline: [0, 0] #Set by an observer
  runningBalancesPlan: [0, 0] #Set by an observer
  runningPaymentsPlan: null #set by observer
  runningPaymentsPlanPrincipal: null #set by observer
  runningPaymentsPlanInterest: null #set by observer
  totalInterestBaseline: null
  totalInterestPlan: null



#relationships
  events: DS.hasMany 'loanPlanEvents', {async: false}
  loans: DS.hasMany 'liabilities', {async: false}
  loanStructure: DS.belongsTo 'loanStructure', {async: false}
  repaymentFrequency: DS.belongsTo 'frequency', {async: false}
  userMessages: DS.hasMany 'userMessages', {async: false}



#computeds:
  interestRate: Ember.computed('loanAmount', 'liabilities.@each.interestRate', 'liabilities.@each.owing', ->
    totalLoanAmount = parseFloat(@get('loanAmount'))
    intRate = 0
    @get('liabilities').forEach (loan) ->
      thisIntRate = parseFloat(loan.get('interestRate'))
      thisLoanAmount = parseFloat(loan.get('owing'))
      intRate += (thisLoanAmount/totalLoanAmount) * thisIntRate
    return intRate || 0
    )

  liabilities: Ember.computed.alias('loanStructure.loanStructureLiabilities')

  loanAmount: Ember.computed.alias('loanStructure.totalOwing')


  notes: Ember.computed('liabilities.@each.frequency', 'liabilities.@each.interestOnly', ->
    notesArray = []
    loanFrequencies = @get('liabilities').mapBy('frequency').uniq()
    if loanFrequencies.length == 1
      @set 'repaymentFrequency', loanFrequencies.get('firstObject')
    else
      notesArray.pushObject('You have multiple payment frequencies specified. We have calculated an approximate total repayment based on the different frequencies')

    if @get('liabilities').filterBy('interestOnly').length > 0
      notesArray.pushObject("As you have 1 or more interest-only loans, we've assumed you will begin paying principal on these loans once other loans are repaid. This will mean your term may be longer than originally planned, or if you have no principal + interest loans your loan balance may not reach zero")


    return notesArray
    )

  planInterestSavings: Ember.computed('totalInterestPlan', 'totalInterestBaseline', ->
    planInterest = parseFloat(@get('totalInterestPlan')) || 0
    baselineInterest = parseFloat(@get('totalInterestBaseline')) || 0
    return baselineInterest - planInterest

    )

  repayments: Ember.computed('liabilities.@each.payment', 'repaymentFrequency', ->
    annualRepayments = 0
    @get('liabilities').forEach (lia) ->
      pmt = lia.get('payment')
      annualRepayments += lia.get('frequency.yearConversion') * pmt
    return annualRepayments / @get('repaymentFrequency.yearConversion')
    )

  terms: Ember.computed.mapBy('liabilities','remainingMonths')
  term: Ember.computed.max('terms')


#observers:
  planCaller: Ember.observer('loanAmount', 'interestRate', 'repayments', 'repaymentFrequency', 'events.@each.eventValue', 'events.@each.recurrenceInterval', 'events.@each.years.[]', ->
    Ember.run.debounce(this, 'recalculatePlan', 300)

    )


#functions:
  addEvent: (year) ->
    newEvent = @store.createRecord('loanPlanEvent',
      year: year
    )
    @get('events').pushObject(newEvent)
    return newEvent


  addEventOfTypeToYear: (type, year) ->
    eventTypeName = type.get('name') #storing name so can find in next instance (not using id)
    newEvent = @store.createRecord('loanPlanEvent',
      years: [year]
      eventType: type
      eventTypeName: eventTypeName
    )
    @get('events').pushObject(newEvent)
    return newEvent



  addLoanPortion: ->
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    newLoan = @store.createRecord('loanStructureLiability',
      remainingMonths: 360
      interestRate: 0.05
      interestOnly: false
      frequency: monthlyFrequency
    )

    @get('liabilities').pushObject(newLoan)
    return newLoan



  init: ->
    @createLoanPlanEvents()
    unless @get('loanStructure')
      newStructure = @store.createRecord('loanStructure')
      @set('loanStructure', newStructure)
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    @set 'repaymentFrequency', monthlyFrequency
    @_super()


  createTrackingObject: ->
    object = {
      balance: parseFloat(@get('loanAmount'))
      borrowed: parseFloat(@get('loanAmount'))
      rate: Math.max(parseFloat(@get('interestRate')),  0.00000000000000001)
      pmt: @get('repayments')
      freq: @get('repaymentFrequency.yearConversion')
      term: Math.max(@get('term'), @get('runningBalancesPlan.length'))
      runningBalances: [parseFloat(@get('loanAmount'))]
      runningPayments: []
      runningPrincipalPayments: []
      runningInterestPayments: []
      principalPaid: 0
      interestPaid: 0
      rnd: 0
    }



  addYearsPaymentsToLoanPlan: (loanPlan) ->
    i = 0
    totalInterestPaidThisYear = 0
    totalPrincipalPaidThisYear = 0
    while i < loanPlan.freq and loanPlan.balance > 0

      periodInterest = (loanPlan.balance * loanPlan.rate) / loanPlan.freq
      periodPrincipal = loanPlan.pmt - periodInterest

      if loanPlan.balance < loanPlan.pmt
        periodInterest = (loanPlan.balance * (periodInterest / (periodInterest + periodPrincipal)))
        periodPrincipal = loanPlan.balance - periodInterest

      loanPlan.balance = loanPlan.balance + periodInterest - loanPlan.pmt
      loanPlan.principalPaid+= periodPrincipal
      loanPlan.interestPaid+= periodInterest

      totalPrincipalPaidThisYear+= periodPrincipal
      totalInterestPaidThisYear+= periodInterest

      i++

    if loanPlan.balance < 0
      loanPlan.balance = 0

    loanPlan.balance = Math.round(loanPlan.balance * 100) / 100
    loanPlan.runningBalances.pushObject(loanPlan.balance)

    loanPlan.runningPayments.pushObject(loanPlan.pmt)
    loanPlan.runningPrincipalPayments.pushObject(totalPrincipalPaidThisYear / loanPlan.freq)
    loanPlan.runningInterestPayments.pushObject(totalInterestPaidThisYear / loanPlan.freq)

    return loanPlan



  recalculatePlan: ->
    self = @
    baseline = @createTrackingObject()
    plan = @createTrackingObject()
    yr = 0 #used to identify this year's events

    while Math.max(baseline.balance, plan.balance) > 0 && yr < 50

      #for plan:
      thisYearsEvents = @get('events').filter (event) ->
        if event.get('years').includes(yr)
          return true

      if thisYearsEvents.length > 0
        index = 0
        thisYearsEvents = thisYearsEvents.sortBy('eventType.eventOrder')
        thisYearsEvents.forEach (event) ->
          plan = event.mutatePlanObject(plan)
          event.set 'yearIndex', index
          index++

      [baseline, plan].forEach (loanPlan) ->
        loanPlan = self.addYearsPaymentsToLoanPlan(loanPlan)
      yr++

    @setProperties(
      totalPrincipalBaseline: baseline.borrowed
      totalPrincipalPlan: plan.borrowed
      totalInterestBaseline: baseline.interestPaid + baseline.principalPaid - baseline.borrowed
      totalInterestPlan: plan.interestPaid + plan.principalPaid - plan.borrowed
      runningBalancesBaseline: baseline.runningBalances
      runningBalancesPlan: plan.runningBalances
      runningPaymentsPlan: plan.runningPayments
      runningPaymentsPlanPrincipal: plan.runningPrincipalPayments
      runningPaymentsPlanInterest: plan.runningInterestPayments
      )


  resetPlan: ->
    destroyArray = []
    @get('events').forEach (event) ->
      destroyArray.pushObject(event)
    destroyArray.forEach (event) ->
      event.destroyRecord()


  didLoad: ->
    @_super()
    @unblob()


  serialize: (options) ->
    planEvents = @get('events')
    loanStructure = @get('loanStructure')
    eventArray = []
    planEvents.forEach (event) ->
      eventArray.pushObject(event.serialize(includeId: true))
    jsonStructure = loanStructure.serialize(includeId: true)

    planBlob = {}
    planBlob.events = eventArray
    planBlob.structure = jsonStructure

    @set('blob', planBlob)
    @_super(options)




  saveTool: (tool) ->
    planEvents = @get('events')
    loanStructure = @get('loanStructure')
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      tool.saveIfNoId().then (->

        self.set('toolId', tool.get('id'))
        jsonPlan = self.serialize(includeId: true)
        toolBlob = {}
        toolBlob.loanPlan = jsonPlan
        toolString = JSON.stringify(toolBlob)
        tool.set('blob', toolString)

        tool.save().then ( ->
          resolve tool
        ), (error) ->
          reject error
      ), (error) ->
        reject error
    )


  unblob: ->
    self = @
    blob = @get('blob')
    if blob
      if blob.events
        blob.events.forEach (event) ->
          self.store.pushPayload(event)
      if blob.structure
        self.store.pushPayload(blob.structure)



  createLoanPlanEvents: ->
    if @store.peekAll('loanPlanEventType').get('length') == 0
      self = @
      eventTypes = [
        {
          id: 'loanplaneventlumpsumpayment'
          name: 'Lump sum payment'
          eventOrder: 3
          iconPath: 'icons/loan-event-lump-sum'
          description: "Make an additional contribution to your loan over and above your loan payments"
          notes: [
            'Some banks have restrictions on the number or value of lump sum payments you can make to certain loan types. Consult your adviser'
          ]
        }, {
          id: 'loanplaneventborrowmore'
          name: 'Borrow more'
          eventOrder: 2
          iconPath: 'icons/loan-event-borrow-more'
          description: "Borrow more money - perhaps to purchase an investment property or a Ferrari!"
          notes: [
            "New lending criteria will need to be met, although it may not always require a full application - consult your adviser"
          ]
        }, {
          id: 'loanplaneventroundup'
          name: 'Round up'
          iconPath: 'icons/loan-event-round-up'
          eventOrder: 9
          description: "Round your payments up to the nearest round figure - you'll be amazed how much you can save!"
          notes: [
            "All payments after this event will be rounded up, including if you increase or reduce payments"
          ]
        }, {
          id: 'loanplaneventincreasepayments'
          name: 'Increase payments'
          iconPath: 'icons/loan-event-increase-payments'
          eventOrder: 4
          description: "Increase your payments by a dollar figure or percentage to pay your loan off quicker"
          notes: [
            "Higher payments are a great way to pay your loan off faster - a good idea if your likely to need to reduce payments/take a payment holiday later"
          ]
        }, {
          id: 'loanplaneventreducepayments'
          name: 'Reduce payments'
          iconPath: 'icons/loan-event-reduce-payments'
          eventOrder: 5
          description: "Reduce your payments - a good option when affordability might be strained"
          notes: [
            "Lenders may only allow you to reduce your payments if you are ahead on your loan. Consult your adviser"
          ]
        }, {
          id: 'loanplaneventrecalculatepayments'
          name: 'Recalculate payments'
          iconPath: 'icons/loan-event-repayment-holiday'
          eventOrder: 6
          description: "Adjust your payments to be paid off by a certain year"
          notes: [
            "Choose a year you'd like to be paid off by and we'll adjust your payments to suit - rounding and recurring payment adjustments will still apply"
          ]
        }, {
          id: 'loanplaneventinterestonly'
          name: 'Interest only'
          iconPath: 'icons/loan-event-interest-only'
          eventOrder: 8
          description: "Reduce your payments to only cover the interest for a fixed period of time"
          notes: [
            "Lenders have strict criteria regarding interest-only loans, including maximum terms and qualification criteria. Consult your adviser"
          ]
        }, {
          id: 'loanplaneventratechange'
          name: 'Rate change'
          iconPath: 'icons/loan-event-rate-change'
          eventOrder: 7
          description: "Interest rates change like the weather. Include a provision for a higher (or lower) interest rate"
          notes: [
            "A higher interest rate may require higher payments - consider raising your payments to cover a higher interest rate"
          ]
        }, {
          id: 'loanplaneventpayoff'
          name: 'Pay off loan'
          iconPath: 'icons/loan-event-pay-off'
          eventOrder: 10
          description: "Leave this life of debt!  Pay your home loan off with an inheritance, property sale, or sell the Ferrari (don't!)"
          notes: [
            "Selecting payoff will terminate the loan at this point"
          ]
        }
      ]
      eventTypes.forEach (type) ->
        self.store.createRecord('loanPlanEventType',
          id: type.id
          name: type.name
          iconPath: type.iconPath
          eventOrder: type.eventOrder
          description: type.description
          notes: type.notes

        )

)


`export default LoanPlan`
