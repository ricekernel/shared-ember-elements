`import Model from 'ember-data/model';`
`import attr from 'ember-data/attr';`
`import DS from 'ember-data'`
`import { belongsTo, hasMany } from 'ember-data/relationships';`


Message = DS.Model.extend

  attachmentList: DS.attr() #json array of document IDs
  bcc: DS.attr "string"
  cc: DS.attr "string"
  compiledMessage: DS.attr "string" #a string of html to use for previewing messages.  Will have template_fields swapped for actual data
  clientList: DS.attr() #json array of client IDs
  isHtml: DS.attr('boolean', {defaultValue: true})
  from: DS.attr "string" #can use to override the user its from
  message: DS.attr "string"
  output: DS.attr() # 0: email, 1: sms
  sendAt: DS.attr()#'date' # data & time message to be sent from server. null = immediate
  sentAt: DS.attr 'date' # data & time message was sent
  status: DS.attr 'number' # 0: draft (won't send), 1: pending (pendings get queued to send. Can still edit), 2: sent (if client sets to 2, it won't send)
  subject: DS.attr "string"
  tagList: DS.attr() #json array of tag names
  templateData: DS.attr() #object containing models required for the template (eg loan apps, entities)
  to: DS.attr "string"  #can use to override recipient. Example send for this client to solicitor
  combinedAttachmentList: DS.attr() #attachments to be merged (mutually exclusive with attachmentList)
  combinedAttachmentName: DS.attr() #what to call the combined attachments


#relationships
  client: DS.belongsTo 'client'
  template: DS.belongsTo 'template'
  user: DS.belongsTo 'user'

`export default Message`
