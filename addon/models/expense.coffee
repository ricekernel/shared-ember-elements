`import DS from 'ember-data'`
`import ModelFinancialForexMixin from '../mixins/shared/model-financial-forex-mixin'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`

Expense = DS.Model.extend(ModelFinancialForexMixin, ModelNotifyServerActionsMixin,
  description: DS.attr()
  identifier: 'expense'
  #monthly: DS.attr 'number' #set for server to use by monthlyX
  rate: DS.attr('number')
  value: DS.attr('number')

#relationships:
  clients: DS.hasMany 'clients'
  currency: DS.belongsTo 'currency'
  expenseType: DS.belongsTo 'expenseType', {async: false}
  frequency: DS.belongsTo 'frequency',
    async: false
  loanApplications: DS.hasMany 'loanApplications'
  liability: DS.belongsTo 'liability'


#computeds:
  name :Ember.computed('description', 'expenseType.name', -> #used to display summary of item (such as in review financials for new loan app)
    if @get('description') && @get('expenseType.name')
      return "#{@get('description')} (#{@get('expenseType.name')})"
    if @get('expenseType.name')
      return @get('expenseType.name')
    else
      return 'Expense'
  )

  altModelName: Ember.computed.alias('name')  #to remove once all references gone

  numberOfInterest: Ember.computed.alias('monthly') #used by mixin to convert to NZD

  type: Ember.computed.alias('expenseType')#used by client.NewFinancial()


  monthly: Ember.computed('value', 'frequency', ->
    unless @get('value') && @get('frequency')
      return 0
    self = @
    val = (@get('value') * @get('frequency').get('yearConversion'))/12
    monthly = (Math.floor(val*100))/100
    #Ember.run.debounce(this, ->
      #self.set('monthly', monthly)
    #, 300)
    return monthly
    )
)


`export default Expense`
