`import DS from 'ember-data'`

Approval = DS.Model.extend
  notes: DS.attr('string') #adviser can record notes to self/client
  status: DS.attr 'enum', {caps: 'first'} #enum ['pending submission', 'submitted', 'pre-approved', 'approved', 'unconditional', 'awaiting pricing', 'accepted', 'settled']
  statusOptions: ['Pending submission', 'Submitted', 'Pre-approved', 'Approved', 'Unconditional', 'Awaiting pricing', 'Accepted', 'Pending settlement', 'Settled']

  submittedDate: DS.attr('date')
  #updatedAt: DS.attr('date')

  bankUid: DS.attr('string')

  narrativePurpose: DS.attr('string')
  narrativeBackground: DS.attr('string')
  narrativeRecommendation: DS.attr('string')

  umi: DS.attr('number', {defaultValue: 0}) #TODO: use bank calcs to work out, or allow user entry


  #isPropertyInvestor: DS.attr('boolean', {defaultValue: false})
  #isSelfEmployed: DS.attr('boolean', {defaultValue: false})
  #isPropertyInvestor: Ember.computed.alias('loanApplication.isPropertyInvestor')


  existingLendingWithLender: DS.attr('number', {defaultValue: 0})
  existingSecuritiesWithLender: DS.attr('number', {defaultValue: 0})

  existingLendingWithLenderToBeRepaid: DS.attr('number', {defaultValue: 0})
  existingSecuritiesWithLenderToBeSold: DS.attr('number', {defaultValue: 0})

  proposedNewLendingForLender: DS.attr('number', {defaultValue: 0})
  proposedNewSecuritiesForLender: DS.attr('number', {defaultValue: 0})

  proposedRefinancedLendingForLender: DS.attr('number', {defaultValue: 0})
  proposedRefinancedSecuritiesForLender: DS.attr('number', {defaultValue: 0})

  existingLendingWithLenderAtRisk: DS.attr('number', {defaultValue: 0})
  existingSecuritiesWithLenderAtRisk: DS.attr('number', {defaultValue: 0})




  #followUpDate: DS.attr() #can set a date to follow up
  #followUpAction: DS.attr('string') #actually, leave these for CRM

  activities: DS.hasMany 'activities'
  bank: DS.belongsTo 'bank', {async: false}
  approvalConditions: DS.hasMany 'approvalConditions'
  documents: DS.hasMany 'documents'
  settlements: DS.hasMany 'settlements' #for CRM
  userActions: DS.hasMany 'userActions' #for CRM

  loanApplication: DS.belongsTo 'loanApplication'
  #loanStructure: DS.belongsTo 'loanStructure'
  pricingOffer: DS.belongsTo 'pricingOffer'

  loanStructure: Ember.computed.alias('loanApplication.content.loanStructure')


  totalLendingProposition: Ember.computed('existingLendingWithLender', 'existingLendingWithLenderToBeRepaid', 'proposedNewLendingForLender', 'proposedRefinancedLendingForLender', ->
    return @get('existingLendingWithLender') - @get('existingLendingWithLenderToBeRepaid') + @get('proposedNewLendingForLender') + @get('proposedRefinancedLendingForLender')
    )

  totalSecurityProposition: Ember.computed('existingSecuritiesWithLender', 'existingSecuritiesWithLenderToBeSold', 'proposedNewSecuritiesForLender', 'proposedRefinancedSecuritiesForLender', ->
    return @get('existingSecuritiesWithLender') - @get('existingSecuritiesWithLenderToBeSold') + @get('proposedNewSecuritiesForLender') + @get('proposedRefinancedSecuritiesForLender')
    )

  proposedLvr: Ember.computed('totalLendingProposition', 'totalSecurityProposition', ->
    return parseFloat((@get('totalLendingProposition') / @get('totalSecurityProposition')))
    )

  thisLendersCurrentLvr: Ember.computed('existingLendingWithLender', 'existingSecuritiesWithLender', ->
    return @get('existingLendingWithLender') / @get('existingSecuritiesWithLender')
    )

  isApproved: Ember.computed('status',  ->
    status = @get('status')
    if ['Pre-approved', 'Approved', 'Unconditional', 'Awaiting pricing'].includes(status)
      return true
    )

  isAccepted: Ember.computed('status',  ->
    status = @get('status')
    if ['Accepted', 'Pending settlement', 'Settled'].includes(status)
      return true
    )

  lastModified: Ember.computed('updatedAt', ->
    updated = @get 'updatedAt'
    return moment(updated).fromNow()
  )

  bankUnsatisfiedConditions: Ember.computed.filterBy('conditions.content', 'bankSatisfied', false)

  name: Ember.computed('bank', 'status', ->
    return "#{@get('bank.name')} (#{@get('status')})"
    )





`export default Approval`
