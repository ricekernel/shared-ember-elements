`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`

LoanPlanEvent = DS.Model.extend(LoanCalculationsMixin,
  name: DS.attr 'string' #user can give each plan a name if they have multiples
  iconPath: DS.attr 'string' #icon and action is based on string. Set when the event is created
  years: DS.attr() #the year in the loan term this event occurs in
  endYear: DS.attr 'number'

  eventOrder: DS.attr 'number'
  eventValue: DS.attr 'number'
  eventValueType: DS.attr 'string' # eg value, 'percent','month',

  eventTypeName: DS.attr 'string' #store the name so we can match it to the config in the next instance

  recurrenceInterval: DS.attr 'number',
    defaultValue: 0

  eventType: DS.belongsTo 'loanPlanEventType',
    async:false
  loanPlan: DS.belongsTo 'loanPlan',
    async: false

#observers:
  recurrenceObs: Ember.observer('recurrenceInterval', ->
    Ember.run.once(this, 'manageRecurringEvents')
    )

#computeds:
  recurrenceYrs: Ember.computed('recurrenceInterval', ->
    return parseInt(@get('recurrenceInterval'))/12
    )
  year: Ember.computed.alias('years.firstObject')



#functions called per event type:
  borrowMore: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    planObject.balance+= eventValue
    planObject.borrowed+= eventValue
    return planObject

  increasePayments: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    eventValueType = @get 'eventValueType'
    if eventValueType == 'percent'
      planObject.pmt+= (planObject.pmt * eventValue)
    else
      planObject.pmt+= eventValue
    if planObject.rnd
      planObject.pmt = Math.ceil(planObject.pmt / planObject.rnd) * planObject.rnd
    return planObject

  interestOnly: (planObject) ->
    planObject.pmt = (planObject.balance * planObject.rate) / planObject.freq
    @ceaseRecurringPaymentEventsAtYear(@get('year'))
    return planObject

  lumpSumPayment: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    if planObject.balance > 0
      if eventValue < planObject.balance
        planObject.balance = planObject.balance - eventValue
        planObject.principalPaid = planObject.principalPaid + eventValue
      else
        planObject.principalPaid = planObject.principalPaid + planObject.balance
        planObject.balance = 0
    return planObject

  payOffLoan: (planObject) ->
    planObject.principalPaid+= planObject.balance
    planObject.balance = 0
    return planObject

  rateChange: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    if eventValue > 0
      planObject.rate = eventValue
    return planObject

  recalculatePayments: (planObject) ->
    eventValue = @get 'eventValue' || 0
    if eventValue > 0 and planObject.balance > 0
      year = @get('years.firstObject')
      remainingTerm = (parseInt(eventValue) - parseInt(year)) * 12
      if remainingTerm > 0
        planObject.pmt = @calculateLoanRepayment(planObject.balance, remainingTerm, planObject.rate, planObject.freq)
    @ceaseRecurringPaymentEventsAtYear(@get('year'))
    return planObject


  reducePayments: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    eventValueType = @get 'eventValueType'
    if eventValueType == 'percent'
      planObject.pmt = planObject.pmt - (planObject.pmt * eventValue)
    else
      planObject.pmt = planObject.pmt - eventValue
    if planObject.rnd
      planObject.pmt = Math.ceil(planObject.pmt / planObject.rnd) * planObject.rnd
    if planObject.pmt < 0
      planObject.pmt = 0
    return planObject

  roundUp: (planObject) ->
    eventValue = parseFloat(@get 'eventValue') || 0
    if eventValue > 0
      planObject.rnd = eventValue
      planObject.pmt = Math.ceil(planObject.pmt / planObject.rnd) * planObject.rnd
    return planObject


  mutatePlanObject: (planObject) ->
    eventType = @get('eventType.name')
    switch eventType
      when 'Borrow more'
        return @borrowMore(planObject)
      when 'Lump sum payment'
        return @lumpSumPayment(planObject)
      when 'Increase payments'
        return @increasePayments(planObject)
      when 'Interest only'
        return @interestOnly(planObject)
      when 'Pay off loan'
        return @payOffLoan(planObject)
      when 'Rate change'
        return @rateChange(planObject)
      when 'Recalculate payments'
        return @recalculatePayments(planObject)
      when 'Reduce payments'
        return @reducePayments(planObject)
      when 'Round up'
        return @roundUp(planObject)

      else
        return planObject


  manageRecurringEvents: ->
    recurrence = parseInt(@get('recurrenceYrs'))
    yr = @get('years.firstObject')
    endYr = @get('endYear') || 100
    @set('years', [yr])
    return false if recurrence == 0 or !recurrence
    while yr < endYr
      yr+= recurrence
      @get('years').pushObject(yr)

  ceaseRecurringPaymentEventsAtYear: (yr) ->
    @get('loanPlan.events').forEach (event) ->
      paymentEvents = ['Reduce payments', 'Increase payments']
      if paymentEvents.includes(event.get('eventTypeName'))
        if event.get('endYear') > yr or !event.get('endYear')
          event.set('endYear', yr)
          event.manageRecurringEvents()

)


`export default LoanPlanEvent`
