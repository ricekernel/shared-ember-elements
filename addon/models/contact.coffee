`import DS from 'ember-data'`

Contact = DS.Model.extend {
  contact: DS.attr 'string'
  inEdit: DS.attr 'boolean',
    default: false
  isPrimary: DS.attr 'boolean'


#relationships
  contactType: DS.belongsTo 'contactType', {async: false}
  client: DS.belongsTo 'client'

  #emailClients: DS.hasMany('clients', {inverse: null})
  #phoneClients: DS.hasMany('clients', {inverse: null})


#computeds:
  category: (->
    contactType = @get 'contactType.name'
    switch contactType
      when 'Email' then 'email'
      when 'LinkedIn' then 'social'
      when 'FaceBook' then 'social'
      when 'Twitter' then 'social'
      when 'Cell phone' then 'mobile'
      when 'Phone' then 'phone'
      when 'Home phone' then 'phone'
      when 'Work phone' then 'phone'
      else 'other'
    ).property 'contactType'

  iconPath: (->
    contactType = @get 'contactType.name'
    switch contactType
      when 'Email' then 'icons/envelope'
      when 'LinkedIn' then 'icons/linkedin'
      when 'FaceBook' then 'icons/facebook'
      when 'Twitter' then 'icons/twitter'
      when 'Cell phone' then 'icons/phone'
      when 'Phone' then 'icons/phone2'
      else 'icons/bell'
    ).property 'contactType'


  isEmail: (->
    contactType = @get "contactType"
    contactType?.get("name") == "Email"
    ).property "contactType"

  isPhone: (->
    contactType = @get "contactType"
    contactType?.get("name") == "Phone"
    ).property "contactType"


  mailto: (->
    contact = @get 'contact'
    if @get "isEmail"
      "mailto:#{contact}"
    else
      "tel:#{contact}"
    ).property "contactType"


  placeholder: Ember.computed('contactType', 'isPrimary', ->
    type = @get('contactType.name').toLowerCase()
    if @get 'isPrimary'
      return "Primary #{type}"
    else
      return type
    )






  #functions:
  contactTypeSetter: ()->
    #This code uses elements in the 'contact' to determine the contact type, rather than asking user
    contact = @get('contact')
    types = @store.peekAll 'contact_type'
    typeName = ''
    hasAmp = contact.indexOf('@')
    if hasAmp == -1
      fbook = contact.toLowerCase().indexOf('facebook')
      linkedIn = contact.toLowerCase().indexOf('linkedin')
      twitter = contact.toLowerCase().indexOf('twitter')
      if twitter != -1
        typeName='Twitter'
      if fbook != -1
        typeName='FaceBook'
      if linkedIn != -1
        typeName='LinkedIn'
      if fbook == -1 && linkedIn == -1
        typeName='Phone'
    else
      if hasAmp == 0
        typeName='Twitter'
      else
        typeName = 'Email'

    thisType = types.filterBy('name', typeName).get('firstObject')
    @set('contactType', thisType)




}



`export default Contact`
