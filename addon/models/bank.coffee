`import DS from 'ember-data'`

Bank = DS.Model.extend {
  name: DS.attr 'string'

  documents: DS.hasMany 'documents'


  logoPath: Ember.computed('name', ->
    name = @get('name')
    switch name
      when 'ANZ' then return '/shared-ember-elements/banks/ANZ.png'
      when 'ASB' then return '/shared-ember-elements/banks/ASB.png'
      when 'BNZ' then return '/shared-ember-elements/banks/BNZ.png'
      when 'Co-op bank' then return '/shared-ember-elements/banks/Coop.png'
      when 'Heartland' then return '/shared-ember-elements/banks/Heartland.png'
      when 'HSBC' then return '/shared-ember-elements/banks/HSBC.png'
      when 'Kiwibank' then return '/shared-ember-elements/banks/Kiwibank.png'
      when 'Resimac' then return '/shared-ember-elements/banks/Resimac.png'
      when 'SBS' then return '/shared-ember-elements/banks/SBS.png'
      when 'Sovereign' then return '/shared-ember-elements/banks/Sovereign.png'
      when 'TSB' then return '/shared-ember-elements/banks/TSB.png'
      when 'Westpac' then return '/shared-ember-elements/banks/Westpac.png'
      when 'Other' then return '/shared-ember-elements/banks/Other.png'
      else return false
    )
}

`export default Bank`
