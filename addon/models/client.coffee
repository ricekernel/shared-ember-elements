`import DS from 'ember-data'`
`import ModelNotifyServerActionsMixin from '../mixins/shared/model-notify-server-actions-mixin'`


DSstring = DS.attr "string"
DSnumber = DS.attr "number"
DSdate = DS.attr "date"

Client = DS.Model.extend(ModelNotifyServerActionsMixin,
  altModelName: 'client'

  dob: DS.attr()
  clientType: DS.attr 'enum', {caps: 'first', defaultValue: 'Person'}
  clientTypes: ['Person', 'Trust', 'Company']
  countryOfBirth: DS.attr('string', {defaultValue: 'New Zealand'})
  employer: DSstring
  firstName: DSstring
  gender: DSstring
  genders: ['', 'male', 'female', 'other']
  identification: DSstring
  isResident: DS.attr("boolean", {defaultValue: true})
  isCompanyTrust: DS.attr("boolean", {defaultValue: false}) #New : is a nonpersonal type client
  lastName: DSstring
  mainBankReferenceNumber: DSstring
  middleName: DSstring
  monthsAtCurrentEmployer: DSnumber
  monthsAtPreviousEmployer: DSnumber
  monthsAtCurrentAddress: DSnumber
  monthsAtPreviousAddress: DSnumber
  minorDependants: DSnumber
  nonMinorDependants: DSnumber
  occupation: DSstring
  preferredName: DSstring
  previousEmployer: DSstring
  secondaryIdentification: DSstring
  tagList: DS.attr()

#relationships:

  activities: DS.hasMany 'activities'
  assets: DS.hasMany "assets"
  bank: DS.belongsTo "bank", {async: false}
  #borrowerLoanApplications: DS.hasMany 'loanApplications', {inverse: 'borrowers'}
  #guarantorLoanApplications: DS.hasMany 'loanApplications', {inverse: 'guarantors'}
  leftClientRelationships: DS.hasMany 'clientRelationships', {inverse: 'leftClient'}
  rightClientRelationships: DS.hasMany 'clientRelationships', {inverse: 'rightClient'}
  contacts: DS.hasMany 'contacts', {inverse: 'client'}
  currentAddress: DS.belongsTo 'address', {inverse: null}
  documents: DS.hasMany 'documents'
  employmentStatus: DS.belongsTo "employmentStatus", {async: false}
  expenses: DS.hasMany "expenses"
  firm: DS.belongsTo 'firm'
  incomes: DS.hasMany 'incomes'
  liabilities: DS.hasMany 'liabilities'
  livingArrangement: DS.belongsTo "livingArrangement", {async: false}
  loanApplications: DS.hasMany 'loanApplications'
  maritalStatus: DS.belongsTo "maritalStatus", {async: false}
  owner: DS.belongsTo 'user'
  primaryPhone: DS.belongsTo 'contact'#, {inverse: null}#, async: false}
  primaryEmail: DS.belongsTo 'contact'#, {inverse: 'emailClients'}#, async: false}
  previousAddress: DS.belongsTo 'address', {inverse: null}

  salutation: DS.belongsTo "salutation", {async: false}
  #securities: DS.hasMany 'securities'
  tools: DS.hasMany 'tools'
  user: DS.belongsTo 'user'
  userActions: DS.hasMany 'userActions'


#computeds

  securities: Ember.computed.filterBy('assets', 'isProperty')

  name: Ember.computed.alias('fullName') #for use in Selects

  fullName: Ember.computed('firstName', 'lastName', 'preferredName', ->
    firstName = @get('firstName')
    lastName = @get('lastName')
    preferredName = @get('preferredName')
    if firstName && @get('preferredName') && lastName
      return "#{firstName} (#{preferredName}) #{lastName}"

    if firstName && lastName
      return "#{firstName} #{lastName}"

    if firstName
      return firstName
    else
      return 'Unnamed Client'
    )


  phone: Ember.computed('primaryPhone.contact',
    get: (key) ->
      if @get('primaryPhone.contact')
        return @get('primaryPhone.contact')
      else
        return null

    set: (key, value) ->
      if @get('primaryPhone.content') and @get('primaryPhone.content.isDeleted') == false
        @set('primaryPhone.contact', value)
      else
        emailType = @store.peekAll('contactType').findBy('name', 'Email')
        email = @store.createRecord('contact',
          contactType: emailType
        )
        email.set('contact', value)
        @set('primaryPhone', email)
      return value
  )


  email: Ember.computed('primaryEmail.contact',
    get: (key) ->
      if @get('primaryEmail.contact')
        return @get('primaryEmail.contact')
      else
        return null

    set: (key, value) ->
      if @get('primaryEmail.content') and @get('primaryEmail.content.isDeleted') == false
        @set('primaryEmail.contact', value)
      else
        emailType = @store.peekAll('contactType').findBy('name', 'Email')
        email = @store.createRecord('contact',
          contactType: emailType
        )
        email.set('contact', value)
        @set('primaryEmail', email)
      return value
  )

#observers:
  salutationObs: Ember.observer('salutation', ->
    if @get('salutation.gender') == 'male' or @get('salutation.gender') == 'female'
      @set('gender', @get('salutation.gender'))

    )

#functions:
  addContact: (contact, primary) ->
    newContact = @store.createRecord('contact',
      contact: contact
      )
    newContact.contactTypeSetter()

    if primary
      newContact.set('isPrimary', true)
      if newContact.get('contactType.name') == 'Email'
        @set('primaryEmail', newContact)
      else
        @set('primaryPhone', newContact)

    @get('contacts').pushObject(newContact)
    return newContact


  #function Overrides:

  copyClient: (client) ->
    self = @
    copyArray = ['isResident', 'maritalStatus', 'livingArrangement', 'minorDependants', 'nonMinorDependants', 'monthsAtCurrentAddress', 'monthsAtPreviousAddress']
    copyArray.forEach (attr) ->
      attrValue = client.get(attr)
      self.set(attr, attrValue)
    if client.get('currentAddress.content')
      client.get('currentAddress').then (add) ->
        currentAddress = add.copyAddress()
        currentAddress.save()
        self.set('currentAddress', currentAddress)
    if client.get('previousAddress.content')
      client.get('previousAddress').then (add) ->
        previousAddress = add.copyAddress()
        previousAddress.save()
        self.set('previousAddress', previousAddress)
    return client


  save: ->
    self = @
    @util_Namify()
    if @get('relatedSaved')
      @_super().then (response) ->
        self.set('relatedSaved', false)
        self.set('preventNotify', false)
    else
      @_super().then (response) ->
        self.set('preventNotify', true)
        self.saveRelatedModels().then (response2) ->
          self.set('relatedSaved', true)
          self.save()




  saveRelatedModels: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      firstSaves = []
      self.get('contacts.content').filterBy('hasDirtyAttributes').forEach (contact) ->
        contactDetails = contact.get('contact') || ''
        if contactDetails.length > 0
          firstSaves.pushObject(contact.save())
        else
          firstSaves.pushObject(contact.destroyRecord())
      if self.get('currentAddress.content.hasDirtyAttributes')
        currentAddress = self.get('currentAddress.content')
        firstSaves.pushObject(currentAddress.save())
      if self.get('previousAddress.content.hasDirtyAttributes')
        previousAddress = self.get('previousAddress.content')
        firstSaves.pushObject(previousAddress.save())
      Ember.RSVP.all(firstSaves).then ((response) ->
        resolve response
      ), (error) ->
        reject error
    )





  util_Namify: ->
    firstName = @get('firstName')
    middleName = @get('middleName')
    lastName = @get('lastName')

    if firstName
      firstName = firstName.charAt(0).toUpperCase() + firstName.slice(1)
      @set('firstName', firstName)

    if middleName
      middleName = middleName.charAt(0).toUpperCase() + middleName.slice(1)
      @set('middleName', middleName)

    if lastName
      lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1)
      @set('lastName', lastName)
)
`export default Client`
