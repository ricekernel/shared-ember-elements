`import DS from 'ember-data'`

ActionTrigger = DS.Model.extend
  name: DS.attr 'string'
  description: DS.attr 'string'

`export default ActionTrigger`
