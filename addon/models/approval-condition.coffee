`import DS from 'ember-data'`

ApprovalCondition = DS.Model.extend {
  name: DS.attr 'string' # what the bank or adviser calls this condition
  description: DS.attr 'string' # a description of what is required/acceptable
  conditionType: DS.attr 'enum', {caps: 'first'} # an enum for when condition must be satisfied ('pre-submission', 'pre-approval', 'pre-settlement', 'post-settlement')
  status: DS.attr 'enum', {caps: 'first'} # an enum for when condition must be satisfied ('pre-submission', 'pre-approval', 'pre-settlement', 'post-settlement')
  requiresDocument: DS.attr("boolean", {defaultValue: false}) #whether this condition requires a document to be provided (loan app will display in outstanding docs)


  #isSatisfied: DS.attr("boolean", {defaultValue: false}) #adviser thinks its satisfied
  #isBankSatisfied: DS.attr("boolean", {defaultValue: false}) #bank thinks its satisfied

  setBy: DS.attr 'enum', {caps: 'first'} #an enum for who set the condition: ['system generated', 'adviser', bank]

  documents: DS.hasMany 'documents'
  approval: DS.belongsTo 'approval'

  conditionTypes: ['', 'Pre submission', 'Pre approval', 'Pre settlement', 'Post settlement']
  setBys: ['', 'System generated', 'Bank', 'Adviser']
  statuses: ['', 'Outstanding', 'Bank satisfied', 'Adviser satisfied']

  isSatisfied: Ember.computed('status', ->
    status = @get('status')
    if ['Bank satisfied', 'Adviser satisfied'].includes(status)
      return true
    )
}

`export default ApprovalCondition`
