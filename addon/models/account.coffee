`import DS from 'ember-data'`

Account = DS.Model.extend
  #A firms 'account' - a model used for managing monthly charging
  billingStart: DS.attr()
  billingDay: DS.attr()
  lastInvoiceDate: DS.attr()

  invoices: DS.hasMany 'invoices'


`export default Account`
