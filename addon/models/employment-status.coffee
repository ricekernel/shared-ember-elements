`import DS from 'ember-data'`

EmploymentStatus = DS.Model.extend
  name: DS.attr 'string'

  hasNoEmployer: Ember.computed('name', ->
    name = @get('name')
    if ['Self employed', 'Student', 'Unemployed', 'Home maker', 'Retired'].includes(name)
      return true
    )

`export default EmploymentStatus`
