`import DS from 'ember-data'`
`import LoanCalculationsMixin from '../mixins/shared/loan-calculations-mixin'`


PricingComparator = DS.Model.extend(LoanCalculationsMixin,

  blob: DS.attr()
  defaultRate: DS.attr('number', {defaultValue: 0.065})
  defaultRateJustificationLink: "http://www.rbnz.govt.nz/statistics/key-graphs/key-graph-mortgage-rates"
  defaultPricingComparisonMonths: DS.attr('number', {defaultValue: 60})
  name: DS.attr('string')
  toolId: DS.attr('number')

  remainingMonths: DS.attr('number', {defaultValue: 360})
  sortAttribute: DS.attr('string', defaultValue: 'repayments')

  frequency: DS.belongsTo('frequency', {async: false})
  loanApplication: DS.belongsTo('loanApplication')
  loanStructure: DS.belongsTo('loanStructure', {async: false})
  pricingOffers: DS.hasMany('pricingOffers', {async: false})
  tool: DS.belongsTo('tool', {async: false})





  init: ->
    @_super()
    unless @get('loanStructure')
      newStructure = @store.createRecord('loanStructure')
      @set('loanStructure', newStructure)
    unless @get('frequency')
      monthly = @store.peekAll('frequency').findBy('name', 'Monthly')
      @set('frequency', monthly)


  comparisons: Ember.computed('sortAttribute', 'frequency', 'remainingMonths', 'pricingOffers.@each.isUpdated', 'loanStructure.loanStructureLiabilities.@each.owing', 'loanStructure.loanStructureLiabilities.@each.fixedRateMonths', 'loanStructure.loanStructureLiabilities.@each.interestOnly', 'loanStructure.loanStructureLiabilities.@each.liabilityType', ->
    self = @
    sortAttribute = @get('sortAttribute')
    comparisons = []
    @get('pricingOffers').forEach (offer) ->
      comparison = self.util_CreateComparisonObjectForOffer(offer)
      comparisons.pushObject(comparison)
    return comparisons.sortBy(sortAttribute)
    )


  util_CreateComparisonObjectForOffer: (offer) ->
    self = @
    frequency = @get('frequency.yearConversion')
    remainingMonths = @get('remainingMonths')
    totalRepayments = 0
    fiveYrIntCost = 0
    lifetimeIntCost = 0
    missingRates = false
    cashContribution = offer.get('cashContribution') || 0
    monthlyFees = offer.get('monthlyFees') || 0
    @get('loanStructure.loanStructureLiabilities').sortBy('liabilityType.sortOrder').forEach (lia) ->
      #go through each proposed loan, find a matching rate in the offer, then work out repayments, 5yrint and lifetime int
      rate = self.utilCalculateRateForLia(lia, offer)
      if rate > 0
        loanAmount = parseFloat( lia.get('owing') )
        liaRepayment = self.calculateLoanRepayment(loanAmount, remainingMonths, rate, frequency, lia.get('paymentIsInterestOnly'))


        liaObject = {
          startingAmount: parseFloat( lia.get('owing') - cashContribution)
          repayment: liaRepayment
          rate: rate
          frequency: frequency
          remainingMonths: remainingMonths
          fixedRateMonths: lia.get('fixedRateMonths')
        }
        cashContribution = 0

        interestNumbers = self.utilCalculateInterestNumbers(liaObject)
        fiveYrIntCost += interestNumbers[0]
        lifetimeIntCost += interestNumbers[1]
        totalRepayments += liaRepayment

      else
        missingRates = true
    #if any rates aren't there, return a false object so that the UI displays a missing rate message

    #if missingRates then return false
    if missingRates then totalRepayments = false

    comparison = {
      bank: offer.get('bank')
      repayments: totalRepayments
      fiveYrCost: fiveYrIntCost + ( monthlyFees * 60 )
      lifetimeCost: lifetimeIntCost + ( monthlyFees * remainingMonths )
      customerSat: self.util_ReturnCustomerSatForLender(offer.get('bank'))
    }
    return comparison



  utilCalculateInterestNumbers: (liaObj) ->
    self = @
    i = 0
    fiveYrInterest = 0
    lifetimeInterest = 0
    fixedRateMonths = liaObj.fixedRateMonths
    balance = liaObj.startingAmount
    monthlyRepayment = liaObj.repayment * liaObj.frequency / 12
    rate = liaObj.rate
    loop
      i++
      interest = balance * rate / 12
      lifetimeInterest += interest
      balance = balance + interest - monthlyRepayment

      if ( i >= fixedRateMonths && fixedRateMonths > 0 ) || ( i == 24 && fixedRateMonths == 0 )
        rate = self.get('defaultRate')
        monthlyRepayment = self.calculateLoanRepayment(balance, liaObj.remainingMonths - i, rate, 12, false)
      if i == 60 then fiveYrInterest = lifetimeInterest
      if i == liaObj.remainingMonths || balance <= 0 then break
    return [fiveYrInterest, lifetimeInterest]



  util_ReturnCustomerSatForLender: (bank) ->
    return "??"


  utilCalculateRateForLia: (lia, offer) ->
    liaTypeSummary = lia.get('typeSummary')
    matchingOffer = offer.get('pricingOfferRates').findBy('typeSummary', liaTypeSummary)
    if matchingOffer
      matchingRate = matchingOffer.get('interestRate')
      return matchingRate
    else
      return false


)

`export default PricingComparator`
