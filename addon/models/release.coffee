`import DS from 'ember-data'`

Release = DS.Model.extend
  title: DS.attr 'string'
  notes: DS.attr 'string'
  app: DS.attr 'string'
  blogLink: DS.attr 'string'
  date: DS.attr 'date'

  helpItems: DS.hasMany 'helpItems'

`export default Release`
