`import DS from 'ember-data'`

Lead = DS.Model.extend
  clientCity: DS.attr 'string'
  clientEmail: DS.attr 'string'
  clientFirstName: DS.attr 'string'
  clientLastName: DS.attr 'string'
  clientPhone: DS.attr 'string'
  createdAt: DS.attr 'date'
  description: DS.attr 'string'
  status: DS.attr()
  trademeListing: DS.attr()

  client: DS.belongsTo 'client'
  firm: DS.belongsTo 'firm'
  user: DS.belongsTo 'user'

  clientName: Ember.computed('clientFirstName', 'clientLastName', ->
    return "#{@get('clientFirstName')} #{@get('clientLastName')}"
    )

  createdMoment: Ember.computed('createdAt', ->
    return moment(@get('date')).format('DD MMM YY')
    )


`export default Lead`
