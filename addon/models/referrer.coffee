`import DS from 'ember-data'`

Referrer = DS.Model.extend {
  name: DS.attr 'string'
  token: DS.attr 'string'
  company: DS.attr 'string'
  firm: DS.belongsTo 'firm'
  user: DS.belongsTo 'user'
}

`export default Referrer`
