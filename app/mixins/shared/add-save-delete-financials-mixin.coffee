`import Ember from 'ember'`

AddSaveDeleteFinancialsMixin = Ember.Mixin.create(

  session: Ember.inject.service('session')

  #classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  actions:
    saveRecord:  ->
      self = @
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      @set('isSaving', true)
      model.save().then ->
        if model.get('loanPartyLiabilityLink') || model.get('loanPartySecurityLink')
          loanApplication.save().then ->
            self.set('isSaving', false)
            self.set('closeAttribute', false)
        else
          self.set('isSaving', false)
          self.set('closeAttribute', false)
        return false


    deleteFinancial: ->
      model = @get('model.content') || @get('model')
      identifier = model.get('identifier')
      inflector = new Ember.Inflector(Ember.Inflector.defaultRules)
      relationship = inflector.pluralize(identifier) #how sexy is this?!
      loanParty = @get('loanParty')
      link = model.get('loanPartyLiabilityLink') || model.get('loanPartySecurityLink')
      if link && @get('loanApplication')
        link.deleteRecord()
        la = @get('loanApplication')
        la.save()
      if loanParty
        loanParty.get(relationship).removeObject(model)
      model.destroyRecord()
      return false



    undoChanges: ->
      model = @get('model.content') || @get('model')
      model.rollbackAttributes()
      return false




  addNextRecord: ->
    @attrs.addNextRecord()
    return false


)



`export default AddSaveDeleteFinancialsMixin`
