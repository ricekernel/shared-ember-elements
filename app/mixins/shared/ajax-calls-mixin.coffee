`import Ember from 'ember'`
`import ENV from '../../config/environment'`
#A mixin to include on any input or button that has a help message.
AjaxCallsMixin = Ember.Mixin.create(

  session: Ember.inject.service('session')

  sendAjaxGetTo: (destination) ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      destinationUrl = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{destination}"
      xhttp = new XMLHttpRequest
      self.get('session').authorize('authorizer:oauth2', (header,token)->
        xhttp.open("GET", destinationUrl, true)
        xhttp.setRequestHeader('authorization', token)
        xhttp.onreadystatechange = ->
          if xhttp.readyState == 4
            if xhttp.status == 200 or xhttp.status == 201 or xhttp.status == 204
              if xhttp.responseText
                responseJSON = JSON.parse(xhttp.responseText)
                resolve responseJSON
              else
                resolve 'success'
            else
              if xhttp.status > 399
                reject xhttp.status
        xhttp.send()
      )
    )

  sendAjaxPatchTo: (jsonRecord, destination) ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      destinationUrl = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{destination}"
      xhttp = new XMLHttpRequest
      self.get('session').authorize('authorizer:oauth2', (header,token) ->
        xhttp.open("PATCH", destinationUrl, true)
        xhttp.setRequestHeader('authorization', token)
        xhttp.setRequestHeader("Content-type", "application/vnd.api+json")
        xhttp.data = jsonRecord

        xhttp.onreadystatechange = ->
          if xhttp.readyState == 4 and xhttp.status == 200
            responseJSON = JSON.parse(xhttp.responseText)
            resolve responseJSON
          else
            if xhttp.status > 399
              reject xhttp.status
        xhttp.send(JSON.stringify(jsonRecord))
      )
    )

  sendAjaxPostTo: (jsonRecord, destination) ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      destinationUrl = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{destination}"
      xhttp = new XMLHttpRequest

      self.get('session').authorize('authorizer:oauth2', (header,token) ->
        xhttp.open("POST", destinationUrl, true)
        xhttp.setRequestHeader('authorization', token)
        xhttp.setRequestHeader("Content-type", "application/vnd.api+json")
        xhttp.data = jsonRecord

        xhttp.onreadystatechange = ->
          if xhttp.readyState == 4 and xhttp.status == 201
            responseJSON = JSON.parse(xhttp.responseText)
            resolve responseJSON
          else
            if xhttp.status > 399
              reject xhttp.status
        xhttp.send(JSON.stringify(jsonRecord))
      )
    )

    #TODO: replace above 3 with below:

  sendAjaxType_To_WithData_: (type, destination, jsonData) ->
    self = @
    method = type.toUpperCase()
    return new Ember.RSVP.Promise((resolve, reject) ->
      destinationUrl = "#{ENV.APP.ADAPTER_HOST}/api/v1/#{destination}"
      xhttp = new XMLHttpRequest
      self.get('session').authorize('authorizer:oauth2', (header,token)->
        xhttp.open(method, destinationUrl, true)
        xhttp.setRequestHeader('authorization', token)
        if jsonData
          xhttp.setRequestHeader("Content-type", "application/vnd.api+json")
          xhttp.data = jsonData
        xhttp.onreadystatechange = ->
          if xhttp.readyState == 4 and xhttp.status > 199 and xhttp.status < 206
            if xhttp.responseText
              responseJSON = JSON.parse(xhttp.responseText)
            else
              responseJSON = ''
            resolve responseJSON
          else
            if xhttp.status > 399
              reject xhttp.status
        if jsonData
          xhttp.send(JSON.stringify(jsonData))
        else
          xhttp.send()
      )
    )

)


`export default AjaxCallsMixin`
