`import Ember from 'ember'`

TableOptionsMixin = Ember.Mixin.create(


  actions:
    deleteRecord: ->
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      unless model.get('id')
        model.deleteRecord()
        if loanApplication
          loanApplication.save()
        return false
      if model.get('id').length > 10
        model.deleteRecord()
        if loanApplication
          loanApplication.save()
      else
        model.destroyRecord().then ->
          if loanApplication
            loanApplication.save()
      return false


    undoChanges: ->
      model = @get('model.content') || @get('model')
      model.rollbackAttributes()
      return false

    viewDetails: ->
      @set('model.showDetails', true)
      return false

)

`export default TableOptionsMixin`
