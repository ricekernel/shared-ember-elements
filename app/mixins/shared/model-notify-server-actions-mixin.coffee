`import Ember from 'ember'`

ModelNotifyServerActionsMixin = Ember.Mixin.create(

  notify: Ember.inject.service('notify')
  ###
  displayName: Ember.computed('altModelName', ->
    if @get('altModelName')
      return @get('altModelName')
    else
      return @get('constructor.modelName')
    )

  becameError: ->
    notify = @get 'notify'
    notify.error 'Bummer dude - something went wrong.  The Socket team have been advised'

  didCreate: ->
    modelName = @get('displayName')
    notify = @get 'notify'
    notify.success "New #{modelName} saved!"


  didDelete: ->
    modelName = @get('displayName')
    capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
    notify = @get 'notify'
    notify.success "#{capitalisedModelName} deleted!"

  didUpdate: ->
    unless @get 'preventNotify'
      modelName = @get('displayName')
      capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
      notify = @get 'notify'
      notify.success "#{capitalisedModelName} updated!"
  ###
)


`export default ModelNotifyServerActionsMixin`
