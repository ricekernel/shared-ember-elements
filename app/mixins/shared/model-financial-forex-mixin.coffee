`import Ember from 'ember'`
`import DS from 'ember-data'`

ModelFinancialForexMixin = Ember.Mixin.create(


  #TODO: rename this as now it has the override save
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')
  forex: Ember.computed.alias('session.forex')

  valueIsNzd: DS.attr 'boolean', {defaultValue: true}
  rate: DS.attr 'number', {defaultValue: 1}
  currency: DS.belongsTo 'currency'


  ready: ->
    self = @
    unless @get 'valueIsNzd'
      self.set 'forex', true
    @_super(arguments...)

  #computeds
  nzdVal: (->
    unless @get('valueIsNzd')
      (Math.round(@get('numberOfInterest') * @get('rate')*100))/100
    else
      @get('numberOfInterest')
    ).property 'numberOfInterest', 'rate'


  #observers:
  currencyObserver: ( ->
    unless @get('isDeleted')
      Ember.run.once(this, 'conversion')
    ).observes 'currency'


  #functions
  conversion: ->
    unless @get('valueIsNzd')
      rate = @get('currency.nzdBasedRate')
      rate = (Math.round(rate*100))/100
      @set 'rate', rate

  ###
  didCreate: ->
    modelName = @get('constructor.modelName')
    notify = @get 'notify'
    notify.success "New #{modelName} saved!"

  didDelete: ->
    modelName = @get('constructor.modelName')
    capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
    notify = @get 'notify'
    notify.success "#{capitalisedModelName} deleted!"

  didUpdate: ->
    modelName = @get('constructor.modelName')
    capitalisedModelName = modelName.charAt(0).toUpperCase() + modelName.substring(1)
    notify = @get 'notify'
    notify.success "#{capitalisedModelName} updated!"
  ###

  overrideSave: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      if self.get('entity.isNew') #if the entity hasn't been persisted, don't persist its incomes
        resolve self
      else
        self.save().then (->
          resolve self
        ), (error) ->
          reject error #TODO: add error handling to my own promises
      )

)


`export default ModelFinancialForexMixin`
