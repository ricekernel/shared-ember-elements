`import Ember from 'ember'`

#A mixin for routes that require config data to use. Note: didn't put this on the router, as login route and possibly others don't require it, and aren't authenticated so cant get it
ConfigManagerMixin = Ember.Mixin.create(

  session: Ember.inject.service('session')

  beforeModel: ->
    self = @

    currentURL = window.location.href.toString()
    if currentURL.indexOf('login') == -1
      @set 'session.lastRoute', currentURL

    if @get('session.isAuthenticated')

      beforeModelServerCalls = [] #an array of the various calls we MIGHT make - dependant on many many things
      unless self.get('session.currentUser')
        beforeModelServerCalls.pushObject(self.getCurrentUser())

      if @store.peekAll('taxBand').get('length') == 0
        beforeModelServerCalls.pushObject(self.store.query('taxBand', {filter: {current: true}}))

      if localStorage.getItem('socketCRMconfiguration')
        config = JSON.parse(localStorage.getItem('socketCRMconfiguration'))
        self.store.pushPayload(config)
        localStorage.removeItem('socketCRMconfiguration')
        @_super(arguments...)


      else
        itemsToInclude = @configIncludeArrayForConvenience()
        beforeModelServerCalls.pushObject(self.store.query('configuration', {include: itemsToInclude}))

      Ember.RSVP.all(beforeModelServerCalls).then ->
        self._super(arguments...)
    else
      self._super(arguments...)

  afterModel: ->
    unless @get 'session.configCheckedThisSession'
      self = @
      config = @store.peekAll('configuration').get('firstObject')
      configDate = config.get('updatedAt')
      currentConfigID = @store.peekAll('configuration').get('firstObject.id')
      @store.query('configuration', {id: currentConfigID}).then (configs) ->
        newConfig = configs.get('firstObject')
        newConfigDate = moment(newConfig.get('updatedAt'))
        oldConfigDate = moment(configDate)

        if moment(newConfigDate).isAfter(oldConfigDate) or !configDate
          itemsToInclude = self.configIncludeArrayForConvenience()
          self.store.query('configuration', {include: itemsToInclude})
        self.set('session.configCheckedThisSession', true)
    @_super(arguments...)


  saveConfigDate: ->
    #grab the last updated of whats in there before the afterModel replaces it:
    config = @store.peekAll('configuration').get('firstObject')
    configDate = config.get('updatedAt')
    @set('configUpdated', configDate)

  getCurrentUser: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->

      self.store.find('user', 'current').then ((currentUser) ->
        currentUserID = currentUser.id
        currentUser = self.store.peekAll('user').findBy('id', currentUserID)
        currentUser.registerLogin()
        self.set('session.currentUser', currentUser)
        resolve currentUser
      ), (error) ->
        reject error

    )


  configIncludeArrayForConvenience: ->
    #An array for convenience of display, that we turn into a string to send in the store query. List all and comment what not required for copy + paste convenience
    configItemArray = [
      'action_statuses'
      "action_triggers"
      "action_types"
      "application_states"
      "approval_statuses"
      "asset_types"
      "banks"
      "contact_types"
      "document_purposes"
      "employment_statuses"
      "expense_types"
      "frequencies"
      "income_types"
      "liability_types"
      "liability_statuses"
      "living_arrangements"
      "marital_statuses"
      "referral_statuses"
      "relationship_types"
      "salutations"
      "security_ownership_types"
      "security_types"
      "security_use_types"
      "submission_methods"
      "template_fields"
      "template_types"
      ]

    return configItemArray.toString()

)

`export default ConfigManagerMixin`
