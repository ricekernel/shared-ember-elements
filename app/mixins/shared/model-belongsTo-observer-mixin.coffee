`import Ember from 'ember'`

ModelBelongsToObserverMixin = Ember.Mixin.create(

  hasDirtyRelations: false

  hasDirtyAttributesOrRelations: Ember.computed.or('hasDirtyRelations', 'hasDirtyAttributes')

  save: ->
    @set 'hasDirtyRelations', false
    @_super()


  makeModelDirty: (name) ->
    @set('hasDirtyRelations', true)

  ready: -> # add observers to each relationship, that set this models 'hasDirtyAttributes' if they change
    self = @
    @eachRelationship (name, descriptor) ->
      setTimeout ->
        if descriptor.kind == 'belongsTo'
          self.addObserver(name, ->
            Ember.run.once(self, 'makeModelDirty', name)
          )

        ###
        else if descriptor.kind == 'hasMany'
          self.addObserver(name, ->

            self.get(name).forEach (relatedModel) ->
              Ember.run.once(self, 'makeModelDirty', name)
          )
        ###

      , 2000 #delay to hopefully allow adapter to set values without triggering


)


`export default ModelBelongsToObserverMixin`
