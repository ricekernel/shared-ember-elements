`import Ember from 'ember'`

TableRowOperationsMixin = Ember.Mixin.create(

  classNameBindings: [':element-table-row', 'inEditMode', 'outOfFocus', 'tableExpanded::table-collapsed']

  showDisplayRow: (->
    if @get('model.isInEditMode') or @get('model.deleting')
      return false
    else
      if @get 'tableExpanded'
        return true
    ).property 'model.isInEditMode', 'model.deleting', 'tableExpanded'

  showEditRow: (->
    if @get 'inEditMode'
      return true
    ).property 'inEditMode'

  showOptions: (->
    return false unless @get('model.viewOptions') or @get('model.isInEditMode')
    if @get 'outOfFocus'
      return false
    else
      return true
      ).property 'model.viewOptions', 'model.isInEditMode', 'outOfFocus'


  inEditMode: (->
    if @get('model.isInEditMode')
      return true
    ).property 'model.isInEditMode'



  outOfFocus: (->
    if @get('tableInEditMode')
      unless @get('model.isInEditMode')
        return true
  ).property 'tableInEditMode', 'model.isInEditMode'

  putInEditMode: (->
    if @get('model.isInEditMode')
      @set 'tableInEditMode', true
    else
      @set 'tableInEditMode', false
      return false
    ).observes 'model.isInEditMode'


  mouseLeave: ->
    if @get('model.viewOptions')
      @set 'model.viewOptions', false


  actions:
    toggleViewOptions: (sender) ->
      @toggleProperty 'model.viewOptions'
      return false


)

`export default TableRowOperationsMixin`
