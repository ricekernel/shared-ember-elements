`import Ember from 'ember'`
#`import EmberUploader from 'ember-uploader'`
`import ENV from '../../config/environment'`

PdfUtilitiesMixin = Ember.Mixin.create(


  util_DocPageHeader: (doc, header) ->
    self = @
    self.util_SetTextColour(doc, 'accent')
    self.util_SetFillColour(doc, 'accent')
    self.util_SetDrawColour(doc, 'accent')
    self.util_CentredText(doc, header, 16, 15)


  util_CentredText: (doc, text, size, y) ->
    doc.setFontSize(size)
    text = text.toString()
    textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor
    textOffset = (doc.internal.pageSize.width - textWidth) / 2;
    doc.text(textOffset, y, text)


  util_CentredOnXText: (doc, text, size, x, y) ->
    doc.setFontSize(size)
    text = text.toString()
    txtWidth = doc.getTextWidth(text)
    xStart = x - (txtWidth/2)
    doc.text(xStart, y, text)

  util_RightAlignedText: (doc, text, size, x, y) ->
    doc.setFontSize(size)
    text = text.toString()
    txtWidth = doc.getTextWidth(text)
    xStart = x - txtWidth
    doc.text(xStart, y, text)


  util_AbbreviatedText: (source, maxLength) ->
    text = (source || '').toString()
    if maxLength
      textLimit = maxLength - 2
    if text.length > (textLimit)
      text = text.substring(0, textLimit) + '..'
    return text


  util_BoxWithText: (doc, text, boxColour, textColour, x, y, width, height, fontSize, textY, fill, borderColour) ->
    @util_SetFillColour(doc, boxColour)
    if fill
      doc.rect(x, y, width, height, 'DF')
    else
      doc.rect(x, y, width, height, 'S')
    @util_SetTextColour(doc, textColour)
    leftPoint = x + 1
    doc.text(leftPoint, textY, text)
    if borderColour
      @util_SetDrawColour(doc, borderColour)
      doc.rect(x, y, width, height, 'S')



  util_BoxWithCentredText: (doc, text, boxColour, textColour, x, y, width, height, fontSize, textY, fill, borderColour) ->
    @util_SetFillColour(doc, boxColour)
    if fill
      doc.rect(x, y, width, height, 'DF')
    else
      doc.rect(x, y, width, height, 'S')
    @util_SetTextColour(doc, textColour)
    centrePoint = x + (width / 2)
    @util_CentredOnXText(doc, text, fontSize, centrePoint, textY)
    if borderColour
      @util_SetDrawColour(doc, borderColour)
      doc.rect(x, y, width, height, 'S')


  util_BoxWithRightAlignedText: (doc, text, boxColour, textColour, x, y, width, height, fontSize, textY, fill, borderColour) ->
    @util_SetFillColour(doc, boxColour)
    if fill
      doc.rect(x, y, width, height, 'DF')
    else
      doc.rect(x, y, width, height, 'S')
    @util_SetTextColour(doc, textColour)
    rightPoint = x + width - 1
    @util_RightAlignedText(doc, text, fontSize, rightPoint, textY)
    if borderColour
      @util_SetDrawColour(doc, borderColour)
      doc.rect(x, y, width, height, 'S')


  util_SetTextColour: (doc, colourName) ->
    switch colourName
      when 'accent' then doc.setTextColor(65, 172, 204)
      when 'white' then doc.setTextColor(255, 255, 255)
      when 'black' then doc.setTextColor(57, 74, 88)
      when 'grey' then doc.setTextColor(128, 128, 128)
      when 'red' then doc.setTextColor(232, 45, 41)
      when 'bold' then doc.setTextColor(0, 0, 0)
      else doc.setTextColor(40, 44, 52)


  util_SetFillColour: (doc, colourName) ->
    switch colourName
      when 'accent' then doc.setFillColor(65, 172, 204)
      when 'white' then doc.setFillColor(255, 255, 255)
      when 'black' then doc.setFillColor(40, 44, 52)
      when 'grey' then doc.setFillColor(128, 128, 128)
      when 'light-grey' then doc.setFillColor(245, 245, 245)
      when 'text-over-grey' then doc.setFillColor(192, 192, 192)
      when 'anz-yellow' then doc.setFillColor(255, 254, 206)
      when 'westpac-bg' then doc.setFillColor(239, 236, 223)
      when 'red' then doc.setFillColor(232, 45, 41)
      when 'aqua' then doc.setFillColor(197, 255, 255)
      when 'green' then doc.setFillColor(115, 198, 95)
      when 'orange' then doc.setFillColor(255, 184, 2)
      else doc.setFillColor(255, 255, 255)

  util_SetDrawColour: (doc, colourName) ->
    switch colourName
      when 'accent' then doc.setDrawColor(65, 172, 204)
      when 'white' then doc.setDrawColor(255, 255, 255)
      when 'black' then doc.setDrawColor(40, 44, 52)
      when 'grey' then doc.setDrawColor(128, 128, 128)
      when 'anz-yellow' then doc.setDrawColor(255, 254, 206)
      else doc.setDrawColor(255, 255, 255)


  util_ValueWithBlockHeader: (doc, text, label, textColour, x, y, width) ->
    labelHeight = @get('labelHeight')
    labelFontSize = @get('labelFontSize')
    rowHeight = @get('rowHeight')
    fontSize = @get('fontSize')

    labelFontY = y + labelHeight - 1
    @util_BoxWithCentredText(doc, label, 'accent', 'white', x, y, width, labelHeight, labelFontSize, labelFontY, true)
    y += labelHeight
    fontY = y + labelHeight
    @util_BoxWithCentredText(doc, text, 'accent', textColour, x, y, width, rowHeight, fontSize, fontY, false)




  savePDFToAWS: (doc, fileName) ->
    self = @
    url = "#{ENV.APP.ADAPTER_HOST}/api/v1/documents/sign"
    #model = @get('model.content') || @get('model')
    #loanApplication = model.get('loanApplication')

    return new Ember.RSVP.Promise((resolve, reject) ->

      self.get('session').authorize('authorizer:oauth2', (header,token)->
        self.set('userToken', token)
      )
      token = self.get('userToken')

      uploader = EmberUploader.S3Uploader.extend({
        signingUrl: url
        signingAjaxSettings:
          headers:
            'authorization': token
            'Accept': 'application/vnd.api+json'

        didSign: (response) ->
          newDocId = response['x-amz-meta-id']
          self.set('newDocId', newDocId)
          #thisFile.id = newDocId
          @_super(response)
          return response

      }).create()

      uploader.on('didError', (jqXHR, textStatus, errorThrown) ->
          #self.attrs.uploadError(errorThrown)
          reject error
        )

      file = doc.output('blob')
      file.name = fileName# "#{loanApplication.get('title')} - Loan Application.pdf"

      newFile = uploader.upload(file)

      newFile.then ((response) ->

        newDocId = self.get('newDocId')

        docData = {
          data: {
            attributes: {
              name: file.name
              document_file_name: file.name
              document_content_type: file.type
            }
            id: newDocId
            type: 'documents'
          }
        }

        self.store.pushPayload(docData)
        documentInStore = self.store.peekRecord('document', newDocId)

        resolve documentInStore
      ), (error) ->
        reject error
      )


)



`export default PdfUtilitiesMixin`
