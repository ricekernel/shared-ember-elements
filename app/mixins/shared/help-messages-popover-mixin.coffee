`import Ember from 'ember'`

#A mixin to include on any input or button that has a help message.
HelpMessagesPopoverMixin = Ember.Mixin.create(


  session: Ember.inject.service('session')

  focusIn: ->
    if @get('helpMessage') and @get('session.helpMessagesOn')
      @get('tooltip').show()

  focusOut: ->
    if @get('tooltip')
      @get('tooltip').hide()
)


`export default HelpMessagesPopoverMixin`
