`import Ember from 'ember'`
`import ENV from "../config/environment"`

Loading = Ember.Controller.extend

  session: Ember.inject.service('session')
  ###
  LOADING_TIPS: [ #hardcoded so that they can display before any data is called
    "Socket's auto-actions are a powerful way to automate your task-list. Find them in the menu under 'Actions' to set up automatic reminders for birthdays, fixed rate rollovers and other triggers"
    "Sockets messaging tools allow you to use placeholders in your messages and templates that are replaced with the right information at the time you send it. Simpy type a double curly brace {{ in your message to view available placeholders"
    'Date selectors giving you grief?  You can type a date in the format dd-mm-yyyy or click on the month and year in the selector to change them quicker'
    'Need to upload a physical document (like ID)?  No worries!  Open Socket on your phone or tablet and use the camera'
    'Socket is designed to look pretty on a phone or tablet as well as a laptop, computer, or TV.  Use whatever device you are comfortable with'
    "If the google match for an address isn't quite right, you can overtype it. Socket will save your version"
    "Want quick access to your dashboard (or other page) from your iPhone? Open the page, tap the action button, then select 'Add to Home Screen'. This will put an app icon on your home screen"
    "Socket uses Gravatar for profile images based on your email address.  You can set your own at www.gravatar.com or via the user settings"
    "Wish to send a single message to multiple clients?  The Socket message service sends a separate message to each one - no more BCC'ing :)"
    "Did you just wake up in the middle of the night remembering an email you need to send tomorrow?  The Socket message service allows you to pick a time and date to deliver your message"
    "Sore fingers from writing so many emails?  Use auto-completed actions with templates (Actions page) to automate your most common communications"
    "When a loan application is settled in socket, socket will automagically create the new loans, remove any repaid loans and change the customer's address (for owner-occupied securities)"
    "By default, socket shows you data belonging to the customer records you 'own'. Access 'View Settings' from the gravatar menu to change this to see your whole firm or specific colleagues'"
    "Need to pass a client record over to a colleague.  Simply change the relationship owner in the 'Client Details' section"
    "Socket allows you to add tags to client records - and they can be anything you want.  Simply start typing a tag to see if it already exists.  If not, socket will create it.  Whats more - you can search for clients by tags, so use them generously"
  ]
  ###
  tips: ENV.APP.LOADING_TIPS

  init: ->
    @runThroughTips()
    @_super(arguments...)

  runThroughTips: ->
    tips = @get('tips')
    tipSelected = Math.floor(Math.random()*@get('tips.length'))
    @set 'currentTip', tips[tipSelected]



`export default Loading`
