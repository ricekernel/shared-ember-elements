`import DS from 'ember-data'`

# Converts server enum to string
EnumTransform = DS.Transform.extend(

  deserialize: (serialized, options) ->
    #string = serialized.replace( /([A-Z])/g, " $1" )
    unless serialized
      return '
      '
    words = serialized.split('_')

    if options.caps == 'all'
      i = 1
      sentence = words[0].charAt(0).toUpperCase() + words[0].slice(1)
      while i < words.length
        sentence = sentence + ' ' + words[i].charAt(0).toUpperCase() + words[i].slice(1)
        i++
      return sentence

    if options.caps == 'first'
      return serialized.charAt(0).toUpperCase() + serialized.slice(1).replace('_', ' ')

    else
      return serialized.replace('_', ' ')

    ###
    string = serialized.charAt(0).toUpperCase() + serialized.slice(1).replace('_', ' ')
    unless options.caps
      return string
    else
      words = string.split(' ')
      console.log words
      i = 1
      sentence = words[0]
      while i < words.length
        sentence = sentence + ' ' + words[i].charAt(0).toUpperCase() + words[i].slice(1)
        i++
      return sentence
      ###


  serialize: (deserialized, options) ->
    unless deserialized
      return ''
    return deserialized.toLowerCase().replace(' ', '_')

)

`export default EnumTransform`
