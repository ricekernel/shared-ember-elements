#`import Ember from 'ember'`
`import Document from 'shared-ember-elements/models/document'`
`import AjaxCallsMixin from '../mixins/shared/ajax-calls-mixin'`
`import ENV from "../config/environment";`

Document.reopen(AjaxCallsMixin,

#functions

  ready: ->
    @_super()
    unless @get('documentContentType')
      docName = @get('name')
      if docName.includes('pdf')
        @set 'documentContentType', 'pdf'
      if docName.includes('img') or docName.includes('jpg') or docName.includes('png') or docName.includes('gif') or docName.includes('tiff')
        @set 'documentContentType', 'image'



  viewDocument: ->
    self = @
    notify = @get('notify')
    signedURLEndPoint = "documents/#{@get('id')}/generate_location"
    return new Ember.RSVP.Promise((resolve, reject) ->
      self.sendAjaxGetTo(signedURLEndPoint).then ((response) ->
        presignedURL = response.presigned_url
        open = window.open(presignedURL)
        if open == null or typeof(open) == 'undefined'
          alert "Your browser is currently blocking popups. You'll need to turn off your pop-up blocker to view this document"
        resolve response
        ), (error) ->
          notify.error "We are unable to access the document at this time. It could be gremlins. If gremlins said #{error}"
          reject error
    )

)


`export default Document`
