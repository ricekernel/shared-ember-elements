#`import Ember from 'ember'`
`import Tool from 'shared-ember-elements/models/tool'`
`import ENV from "../config/environment";`

Tool.reopen

  launchTool: ->
    toolkitPath = ENV.APP.TOOLKIT_PATH
    thisToolsPath = "#{toolkitPath}/toolkit/#{@getToolRoute()}/#{@get('id')}"
    window.open(thisToolsPath)
    return false


  getToolRoute: ->
    toolType = @get('toolType')
    switch toolType
      when "Repayment Calculator" then return 'payment-calculator'
      when "Loan Planner" then return 'loan-planner'
      when "Offer Comparator" then return "pricing-comparator"
      when "Budget Builder" then return 'budget-builder'
      when "Lending Power" then return 'lending-power'
      when "Commission Calculator" then return 'commission-calculator'


`export default Tool`
