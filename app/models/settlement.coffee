#`import Ember from 'ember'`
`import Settlement from 'shared-ember-elements/models/settlement'`
`import AjaxCallsMixin from '../mixins/shared/ajax-calls-mixin'`

Settle = Settlement.extend(AjaxCallsMixin,

#functions

  addSettlementLoan: ->
    self = @
    revolvingCredit = @store.peekAll('liabilityType').findBy('name', 'Revolving credit')
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')

    return new Ember.RSVP.Promise((resolve, reject) ->

      self.get('loanApplication.borrowers').then (clients) ->

      #create the new loan for the remaining unspent settlement value
        newLoanValue = self.util_CalculateRemainingSettlementValue()
        newLoan = self.store.createRecord('liability',
          settlement: self
          frequency: monthlyFrequency
          status: 'pending'
          liabilityType: revolvingCredit
          remainingMonths: 360
          owing: newLoanValue
          clients: clients
          limitAfterDrawDown: newLoanValue
          )
        self.util_SetPaymentDetailsForLoan(newLoan)
        #self.util_SetPaymentDetailsForLoan(newLoan)

        #newLoan.get('loanApplications').pushObject(loanApplication)
        self.get('liabilities').pushObject(newLoan)

        newLoan.save().then (->
          resolve newLoan
        ), (error) ->
          reject error
      )


  addSecurity: ->
    self = @

    return new Ember.RSVP.Promise((resolve, reject) ->

      self.get('loanApplication.borrowers').then (clients) ->

        newSecurity = self.store.createRecord('security',
          settlement: this
          isProposed: true
          clients: clients
          )

        self.get('securities').pushObject(newSecurity)
        newSecurity.save().then (->
          resolve newSecurity
        ), (error) ->
          reject error
      )


  destroySettlement: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      destroyArray = []
      self.get('liabilities').forEach (lia) ->
        destroyArray.pushObject(lia.destroyRecord())
      Ember.RSVP.all(destroyArray).then (->
        self.destroyRecord()
        resolve 'success'
      ), (error) ->
        reject error
    )

  settle: ->
    self = @
    @set 'status', 'settled'
    @get('loanApplication').then (la) ->
      la.set('status', 'settled')
    return new Ember.RSVP.Promise((resolve, reject) ->
      settleRoute = "settlements/#{self.get('id')}/settle"
      self.sendAjaxGetTo(settleRoute).then (->
        resolve self
      ), (error) ->
        reject error
    )


  saveSettlement: ->
    self = @
    @set 'cantDelete', true
    return new Ember.RSVP.Promise((resolve, reject) ->
      saveArray = []

      self.save().then (->
        self.get('securities').forEach (security) ->
          if security.get('content')
            security = security.get('content')
          if security.get('hasDirtyAttributes')
            saveArray.pushObject(security.save())

        self.get('liabilities').forEach (loan) ->
          if loan.get('content')
            loan = loan.get('content')
          if loan.get('hasDirtyAttributes')
            saveArray.pushObject(loan.save())

        Ember.RSVP.all(saveArray).then (->
          self.get('loanApplication').then (la) ->
            la.save()
            self.set 'cantDelete', false
            resolve self
          ), (error) ->
            self.set 'cantDelete', false
            reject error
          ), (error) ->
            self.set 'cantDelete', false
            reject error
        )




  util_CalculateRemainingSettlementValue: ->
    #see what the total loans so far add up to
    values = @get('liabilities').mapBy('limitAfterDrawDown')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    #return the difference between total above and settlementValue
    settlementValue = parseFloat(@get('settlementValue'))
    return settlementValue - totalValue

  util_SetPaymentDetailsForLoan: (loan) ->
    @get('liabilities.content').forEach (lia) ->
      if lia.get('repaymentStartDate')
        pmtStart = lia.get('repaymentStartDate')
        loan.set('repaymentStartDate', pmtStart)
      if lia.get('frequency.yearConversion')
        pmtFreq = lia.get('frequency')
        loan.set('frequency', pmtFreq)



)


`export default Settle`
