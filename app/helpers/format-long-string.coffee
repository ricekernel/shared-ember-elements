`import Ember from 'ember'`

# This function receives the params `params, hash`
formatLongString = (params) ->
  value = params[0]
  maxLength = params[1]

  unless maxLength
    maxLength = 30

  if value
    result = value.substr(0, maxLength)
    if value.length > 30
      result += '...'
    return result

  else
    return null


FormatLongStringHelper = Ember.Helper.helper formatLongString

`export { formatLongString }`

`export default FormatLongStringHelper`
