`import Ember from 'ember'`

# This function receives the params `params, hash`
formatPercent = (params) ->
  value = params[0]
  hidePercentSign = params[1]
  
  if hidePercentSign
    return (Math.floor(parseFloat(value) * 10000))/100
  else
    return (Math.floor(parseFloat(value) * 10000))/100 + '%'

FormatPercentHelper = Ember.Helper.helper formatPercent

`export { formatPercent }`

`export default FormatPercentHelper`
