`import Ember from 'ember'`
`import accounting from 'accounting'`

# This function receives the params `params, hash`
formatCurrency = (params) ->
  value = params[0]
  showDecimal = params[1]

  if showDecimal
    return accounting.formatMoney(value, '$', 2)
  else
    return accounting.formatMoney(value, '$', 0)


FormatCurrencyHelper = Ember.Helper.helper formatCurrency

`export { formatCurrency }`

`export default FormatCurrencyHelper`
