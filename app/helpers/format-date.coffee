`import Ember from 'ember'`

# This function receives the params `params, hash`
formatDate = (params) ->
  date = params[0]
  format = params[1]
  if format
    return moment(date).format(format)
  else
    return moment(date).format('DD MMM YY')

FormatDateHelper = Ember.Helper.helper formatDate

`export { formatDate }`

`export default FormatDateHelper`
