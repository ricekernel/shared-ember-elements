`import Ember from 'ember'`
`import TableOptionsMixin from '../../../../mixins/shared/table-options-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableOptionsMixin,

  classNames: ['s24 flex-row-nowrap table-single-line']

  #onlyParty: Ember.computed.equal('model.loanApplication.content.loanParties.length', 1)

  onlyParty: Ember.computed.equal('partyCount', 1)


  actions:
    deleteRecord: ->
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      loanApplication.get('loanParties').removeObject(model)
      model.deleteRecord()
      loanApplication.save()

      return false




)

`export default BorrowersIndividualTableRowComponent`
