`import Ember from 'ember'`
`import TableRowOptionsMixin from 'loan-application/mixins/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from 'loan-application/mixins/flow/add-save-delete-financials-mixin'`
`import accounting from 'accounting'`

PropositionLiabilitySingleLineComponent = Ember.Component.extend(TableRowOptionsMixin,

  ###
  component for viewing a single security in a loan proposition

  Needs:
    model (security)
  ###

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  refinanceOptions: [
    {
      value: false
      colour: "rgba(224, 108, 117, 1.00)"
    },
    {
      value: true
      colour: "rgba(152, 195, 121, 1.00)"
    }
  ]

  bankNames: []

  bank: null
  bankObserver: Ember.observer('bank', ->
    model = @get('model.content') || @get('model')
    bankName = @get('bank')
    if bankName == 'No Mortgage'
      model.set('bank', null)
    else
      bank = @store.peekAll('bank').findBy('name', bankName)
      model.set('bank', bank)
  )




  actions:
    deleteLiability: ->
      model = @get('model.content') || @get('model')
      model.destroyRecord()
      return false

    editLiability: ->
      model = @get('model.content') || @get('model')
      model.set('showDetails', true)
      return false


  init: ->
    @_super()
    bankNames = @store.peekAll('bank').mapBy('name')
    bankNames.pushObject('No Mortgage')
    @set('bankNames', bankNames)
    if @get('model.bank.id')
      bank = @get('model.bank.name')
      @set('bank', bank)
    else
      @set('bank', 'No Mortgage')

)

`export default PropositionLiabilitySingleLineComponent`
