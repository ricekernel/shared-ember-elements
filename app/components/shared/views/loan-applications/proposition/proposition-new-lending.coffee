`import Ember from 'ember'`

PropositionNewLendingComponent = Ember.Component.extend

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  actions:
    save: ->
      self = @
      la = @get('model')
      @set('saving', true)
      la.save().then (->
        self.set('saving', false)
        self.set('closeAttribute', false)
      ), (error) ->
        self.saveError()


  saveError: ->
    notify = @get('notify')
    notify.error("There was a problem, please try again")
    @set('saving', false)











`export default PropositionNewLendingComponent`
