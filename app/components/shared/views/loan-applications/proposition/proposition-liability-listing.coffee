`import Ember from 'ember'`

PropositionLiabilitiesListingComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-align-centre']


  actions:
    addLiability: ->
      defaultType = @store.peekAll('liabilityType').findBy('name', 'Floating home loan')
      loanParty = @get('model.borrowerParty')
      newLiability = loanParty.addLiability()
      newLiability.setProperties(
        liabilityType: defaultType
        status: "current"
      )
      if @get('model.loanApplicationBlob.retainedLiabilities.length') > 0
        bank = @get('model.loanApplicationBlob.retainedLiabilities.lastObject.bank')
        newLiability.set('bank', bank)
      return false


    save: ->
      self = @
      la = @get('model')
      @set('saving', true)
      firstSaves = []
      la.get('loanApplicationBlob.retainedLiabilities').forEach (lia) ->
        firstSaves.pushObject(lia.save())
      Ember.RSVP.all(firstSaves).then ((response) ->
        la.save().then (->
          self.set('saving', false)
          self.set('closeAttribute', false)
        ), (error) ->
          self.saveError()
      ), (error) ->
        self.saveError()



  saveError: ->
    notify = @get('notify')
    notify.error("There was a problem, please try again")
    @set('saving', false)


)

`export default PropositionLiabilitiesListingComponent`
