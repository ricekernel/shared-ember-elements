`import Ember from 'ember'`

PropositionSecuritiesListingComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  actions:
    addSecurity: ->
      houseType = @store.peekAll('securityType').findBy('name', 'House')
      ownerOcc = @store.peekAll('securityUseType').findBy('name', 'Owner occupied')

      if @get('isProposed')
        la = @get('model')
        newSecurity = la.addProposedSecurity()
      else
        loanParty = @get('model.borrowerParty')
        newSecurity = loanParty.addSecurity()
        if @get('model.loanApplicationBlob.retainedSecurities.length') > 0
          bank = @get('model.loanApplicationBlob.retainedSecurities.lastObject.bank')
          newSecurity.set('bank', bank)
        newSecurity.setProperties(
          securityType: houseType
          securityUseType: ownerOcc
        )
      return false


    save: ->
      self = @
      la = @get('model')
      @set('saving', true)
      firstSaves = []
      if @get('isProposed')
        la.get('assets').forEach (ass) ->
          firstSaves.pushObject(ass.save())
      else
        la.get('loanApplicationBlob.retainedSecurities').forEach (sec) ->
          firstSaves.pushObject(sec.save())

      Ember.RSVP.all(firstSaves).then ((response) ->
        la.save().then (->
          self.set('saving', false)
          self.set('closeAttribute', false)
        ), (error) ->
          self.saveError()
      ), (error) ->
        self.saveError()



  saveError: ->
    notify = @get('notify')
    notify.error("There was a problem, please try again")
    @set('saving', false)


)

`export default PropositionSecuritiesListingComponent`
