`import Ember from 'ember'`

PropositionComponentComponent = Ember.Component.extend

  classNames: ['s24 flex-row-wrap flex-align-start']

  ###
  retainedLiabilities: Ember.computed('model.borrowerParty.liabilities.@each.liabilityType', 'model.borrowerParty.loanPartyLiabilityLinks.@each.liabilitySettlementAction', ->
    @get('model.borrowerParty.liabilities').filter (lia) ->
      if ['Kept (will not refinance)', 'Kept (will refinance if lender changes)'].includes( lia.get('loanPartyLiabilityLink.liabilitySettlementAction') ) && lia.get('liabilityType.isSecured')
        return true
    )

  totalRetainedLiabilities: Ember.computed('retainedLiabilities.@each.limitAfterDrawDown', ->
    return @get('retainedLiabilities').mapBy('limitAfterDrawDown').compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    )
  ###

  ###
  newLending: Ember.computed.alias('model.applicationValue')



  lendingProposition: Ember.computed('existingLending', 'existingLendingToBeRepaid', 'newLending', ->
    return @get('existingLending') + @get('newLending') - @get('existingLendingToBeRepaid')
    )
  ###

  #retainedLiabilitiesValues: Ember.computed.mapBy('retainedLiabilities', 'limitAfterDrawDown')
  #totalRetainedLiabilities: Ember.computed.sum('retainedLiabilitiesValues')

  ###
  init: ->
    @_super()
    Ember.run.later(this, 'initBaselineGraph', 1500)

  initBaselineGraph: ->
    labels = ['Securities', 'Lending']
    data = []

    propositionChartData = {
      labels: labels
      datasets: [
        {
          label: 'Existing'
          data: [10, 5]
          borderColor: ['rgba(52, 143, 170, 1.00)', 'rgba(67, 106, 201, 1.00)']
          backgroundColor: ['rgba(52, 143, 170, 1.00)', 'rgba(67, 106, 201, 1.00)']
        }, {
          label: 'Proposed'
          data: [3, 5]
          borderColor: ['rgba(52, 143, 170, 0.50)', 'rgba(67, 106, 201, 0.50)']
          backgroundColor: ['rgba(52, 143, 170, 0.50)', 'rgba(67, 106, 201, 0.50)']
        }
      ]
    }



    ctx = document.getElementById("propositionBarChart").getContext("2d")
    propositionChart = new Chart(ctx,
      type: 'horizontalBar'
      data: propositionChartData
      options:
        responsive: true
        maintainAspectRatio: true
        scales: {
          xAxes: [
            display: true
            stacked: true
            type: 'linear'
            beginAtZero: true
            ticks:
              beginAtZero: true
          ]
          yAxes: [
            categoryPercentage: 0.8

            display: false

            stacked: true

          ]
        }
        legend:
          display: false
        tooltips:
          mode: 'label'
          callbacks:
            title: (tooltipItem, data) ->
              return 'Total term (yrs)'


    )
    @set 'propositionChart', propositionChart


  updateYrsGraph: ->
    baselineYears = @get('baselineYears')
    planYears = @get('planYears')

    yrsChart = @get 'yrsChart'
    labels = []
    i = 0

    yrsChart.data.labels = labels
    yrsChart.data.datasets[0].data = [baselineYears]
    yrsChart.data.datasets[1].data = [planYears]
    yrsChart.update()

  ###







  actions:
    viewPopover: (popover) ->
      switch popover
        when ('existingLending') then @set('showExistingLiabilities', true)
        when ('newLending') then @set('showNewLending', true)
        when ('existingSecurities') then @set('showExistingSecurities', true)
        when ('proposedSecurities') then @set('showProposedSecurities', true)
      return false




  util_CalculateTotalFinancials: (list, valueAttribute) ->
    return 0 unless list
    values = list.mapBy(valueAttribute)
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0
    return totalValue










`export default PropositionComponentComponent`
