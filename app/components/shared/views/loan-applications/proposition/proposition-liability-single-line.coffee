`import Ember from 'ember'`
`import TableRowOptionsMixin from 'loan-application/mixins/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from 'loan-application/mixins/flow/add-save-delete-financials-mixin'`
`import accounting from 'accounting'`

PropositionLiabilitySingleLineComponent = Ember.Component.extend(TableRowOptionsMixin,

  ###
  component for viewing a single liability in a loan proposition

  Needs:
    model (liability)
  ###

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']


  loanType: Ember.computed('model.liabilityType',
    get: (key) ->
      type = @get('model.liabilityType.name')
      switch type
        when "Revolving credit" then return "Revolving"
        when "Floating home loan" then return "Floating"
        when "Fixed home loan" then return "Fixed"
        else return "Other"

    set: (key, value) ->
      switch value
        when "Revolving" then type = "Revolving credit"
        when "Floating" then type = "Floating home loan"
        when "Fixed" then type = "Fixed home loan"
        else type = "Other"
      liabilityType = @store.peekAll('liabilityType').findBy('name', type)
      @set('model.liabilityType', liabilityType)
      return value
    )

  loanTypes: ["Revolving", "Floating", "Fixed"]

  refinanceOptions: [
    {
      value: false
      colour: "rgba(224, 108, 117, 1.00)"
    },
    {
      value: true
      colour: "rgba(152, 195, 121, 1.00)"
    }
  ]

  limitObs: Ember.observer('model.owing', ->
    model = @get('model')
    link = model.get('loanPartyLiabilityLink')
    owing = @get('model.owing')
    @set('model.limit', owing)
    if model.changedAttributes().owing #This ensures the limitAfterDrawDown doesn't get changed in response to a server post
      @set('model.limitAfterDrawDown', owing)
  )


  actions:
    deleteLiability: ->
      model = @get('model.content') || @get('model')
      model.destroyRecord()
      return false

    editLiability: ->
      model = @get('model.content') || @get('model')
      model.set('showDetails', true)
      return false


)

`export default PropositionLiabilitySingleLineComponent`
