`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsApplicationOwnershipComponent = Ember.Component.extend(

  hasSettlements: Ember.computed.gt('model.settlements.content.length', 0)

  actions:
    addSettlement: ->
      self = @
      model = @get('model.content') || @get('model')
      @set('addingSettlement', true)
      model.addSettlement().then ((newSettlement) ->
        self.set('addingSettlement', false)
        self.set('showSettlements', true)
        return false
        ), (error) ->
          console.log error
          self.set('addingSettlement', false)
          return false

    viewSettlements: ->
      model = @get('model.content') || @get('model')
      @set('showSettlements', true)
      return false


)

`export default ViewsLoanApplicationsSettlementsApplicationOwnershipComponent`
