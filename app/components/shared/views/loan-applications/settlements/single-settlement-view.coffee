`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsSingleSettlementViewComponent = Ember.Component.extend(

  ###
  Popover component for viewing and adding settlements.

  Needs:
    model (loanApplication)

  ###

  classNames: ['presentation-object-inner s24 flex-column-nowrap']
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')


  commissionRates: []

  settlementBankObs: Ember.observer('settlement.bank', ->
    return false if @get('settlement.commissionRate.content')
    if @get('settlement.bank')
      bank = @get('settlement.bank')
      commissionRates = @get('commissionRates').filterBy('bank')
      @set('commissionRates', commissionRates)
    )

  totalCommission: Ember.computed('settlement.liabilities.content.@each.totalCommission', 'settlement.liabilities.@each.totalFlatFeeCommission', 'settlement.liabilities.@each.totalTrailCommission', 'settlement.liabilities.@each.totalUpFrontCommission',  ->
    if @get('settlement.liabilities.length') > 0
      commissions = @get('settlement.liabilities').mapBy('totalCommission')
      totalValue = commissions.compact().reduce ((a, b) ->
        (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
        ), 0
      return totalValue
    else
      return 0
    )

  ownerCommSplit: Ember.computed('settlement.loanApplication.content.ownerPercentage', 'totalCommission',  ->
    ownerShare =  @get('settlement.loanApplication.content.ownerPercentage') * @get('totalCommission')
    return ownerShare
    )

  secondaryOwnerCommSplit: Ember.computed('settlement.loanApplication.content.secondaryOwnerPercentage', 'totalCommission', ->
    secShare =  @get('settlement.loanApplication.content.secondaryOwnerPercentage') * @get('totalCommission')
    return secShare
    )

  firmCommSplit: Ember.computed('ownerCommSplit', 'secondaryOwnerCommSplit', 'totalCommission', ->
    return @get('totalCommission') - @get('ownerCommSplit') - @get('secondaryOwnerCommSplit')
    )


  commissionRatesDontMatchSettlementBank: Ember.computed('settlement.loanApplication.content.settlementBank', 'settlement.liabilities.content.@each.commissionRate.content.bank', ->
    settlementBank = @get('settlement.loanApplication.content.settlementBank')
    differ = false
    if @get('settlement.liabilities.length') > 0
      @get('settlement.liabilities').forEach (lia) ->
        bank = lia.get('commissionRate.content.bank')
        unless settlementBank == bank
          differ = true
    return differ
    )



  actions:
    addSecurity: ->
      self = @
      self.set 'addingSecurity', true
      settlement = @get('settlement')
      settlement.addSecurity().then (newSecurity) ->
        self.set 'addingSecurity', false
        self.send('showSecurity', newSecurity)

    addSettlementLoan: ->
      self = @
      self.set('creatingLoan', true)
      settlement = @get('settlement')
      settlement.addSettlementLoan().then (newLoan) ->
        self.set('creatingLoan', false)
        self.send('showSettlementLoan', newLoan)

    showAppOwnership: ->
      @set 'showAppOwnership', true


    removeSecurity: (security) ->
      @get('settlement.securities').removeObject(security)
      security.destroyRecord()
      return false

    removeSettlementLoan: (settlementLoan) ->
      @get('settlement.liabilities').removeObject(settlementLoan)
      settlementLoan.destroyRecord()
      return false

    saveSettlement: ->
      self = @
      @set 'isSaving', true
      notify = @get('notify')
      model = @get('settlement')

      model.saveSettlement().then (->
        #self.set 'isSaving', false
        notify.success 'Settlement saved!'
      ), (error) ->
        #self.set 'isSaving', false
        notify.error "hmmm - computer said #{error}"
      @set('closeAttribute', false)

    confirmSettlement: ->
      self = @
      notify = @get('notify')
      model = @get('settlement')
      if model.get('content')
        model = model.get('content')
      @set 'settling', true
      @set 'confirmSettlement', false
      model.settle().then (->
        notify.success 'settlement successful!'
        #self.set 'settling', false
      ), (error) ->
        notify.error "We couldn't process that settlement right now. Our server said: #{error}"
        #self.set 'settling', false

    showSecurity: (security) ->
      @set 'selectedSecurity', security
      @set 'showSecurity', true

    showSettlementLoan: (settlementLoan) ->
      @set 'selectedSettlementLoan', settlementLoan
      @set 'showSettlementLoan', true

    toggleConfirmSettlement: ->
      @toggleProperty 'confirmSettlement'
      return false



  init: ->
    today = new Date()
    today.setMonth(today.getMonth() - 1)
    @set 'lastMonth', today
    @_super()

  willDestroyElement: ->
    @settlement.saveSettlement()
    @_super()

)

`export default ViewsLoanApplicationsSettlementsSingleSettlementViewComponent`
