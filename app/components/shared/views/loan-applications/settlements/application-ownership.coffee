`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsApplicationOwnershipComponent = Ember.Component.extend(

  ###
  component for editing ownership of a loan application at settlement, for the purposes of defining commission share

  Needs:
    model (loanApplication)

  ###



  classNames: ['presentation-object-inner s24 flex-column-nowrap']
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  firmUsers: []
  gettingUserInfo : false
  showSecondOwner: false
  ownerPercentage: Ember.computed('model.secondaryPercentage', ->
    return 1 - @get('model.secondaryPercentage')
    )

  actions:
    addSecondOwner: ->
      @set 'showSecondOwner', true
      return false

    removeSecondOwner: ->
      @set('model.secondaryOwner', null)
      @set('model.secondaryPercentage', 0)
      @utilSaveModel()
      @set 'showSecondOwner', false
      return false

  init: ->
    self = @
    @set 'gettingUserInfo', true
    @_super()
    firmId = @get('session.currentFirm.id')
    @store.query('user', {filter: {current_firm: firmId}}).then (users) ->
      self.set 'firmUsers', users
      self.set 'gettingUserInfo', false
    if @get('model.secondaryOwner.content.name')
      @set 'showSecondOwner', true

  willDestroyElement: ->
    @utilSaveModel()

  utilSaveModel: ->
    model = @get('model')
    if model.get('content')
      model = model.get('content')
    model.save()



)

`export default ViewsLoanApplicationsSettlementsApplicationOwnershipComponent`
