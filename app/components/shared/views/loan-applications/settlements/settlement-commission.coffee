`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsSingleSettlementViewComponent = Ember.Component.extend(

  ###
  Popover component for viewing and adding settlements.

  Needs:
    model (commission)

  ###

  classNames: ['presentation-object-inner s24 flex-column-nowrap']
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')
  commissionIsPercent: true




  actions:
    

    save: ->
      self = @
      notify = @get 'notify'
      @set 'isSaving', true
      model = @get('model')
      if @get('model.content')
        model = @get('model.content')
      model.save().then (->
        notify.success 'Settlement loan saved!'
        self.set 'isSaving', false
        self.set 'backAttribute', false
      ), (error) ->
        notify.error "There was a problem: #{error}"
        self.set 'isSaving', false

    toggleCommissionIsPercent: ->
      @set 'model.commissionRate', null
      @set 'model.commissionValue', null
      @toggleProperty('commissionIsPercent')
      return false


)

`export default ViewsLoanApplicationsSettlementsSingleSettlementViewComponent`
