`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsSingleSettlementViewComponent = Ember.Component.extend(

  ###
  Popover component for viewing and adding settlements.

  Needs:
    model (loanApplication)

  ###

  classNames: ['presentation-object-inner s24 flex-column-nowrap']
  commRateChanged: false
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  loanTypes: []

  commissionRates: []
  commissionsRequireUpdating: Ember.computed.or('model.hasDirtyAttributes', 'commRateChanged')

  isRevolvingCredit: Ember.computed.equal('model.liabilityType.name', 'Revolving credit')

  ownerCommSplit: Ember.computed('model.settlement.loanApplication.content.ownerPercentage', 'model.totalCommission',  ->
    ownerShare =  @get('model.settlement.loanApplication.content.ownerPercentage') * @get('model.totalCommission')
    return ownerShare
    )

  secondaryOwnerCommSplit: Ember.computed('model.settlement.loanApplication.content.secondaryOwnerPercentage', 'model.totalCommission', ->
    secShare =  @get('model.settlement.loanApplication.content.secondaryOwnerPercentage') * @get('model.totalCommission')
    return secShare
    )

  firmCommSplit: Ember.computed('ownerCommSplit', 'secondaryOwnerCommSplit', 'model.totalCommission', ->
    return @get('model.totalCommission') - @get('ownerCommSplit') - @get('secondaryOwnerCommSplit')
    )

  commRateObserver: Ember.observer('model.commissionRate', ->
    @set 'commRateChanged', true
    )

  actions:
    save: ->
      self = @
      notify = @get 'notify'
      @set 'isSaving', true
      model = @get('model')
      if @get('model.content')
        model = @get('model.content')
      model.save().then (->
        notify.success 'Settlement loan saved!'
        self.set 'isSaving', false
        self.set 'commRateChanged', false
      ), (error) ->
        notify.error "There was a problem: #{error}"
        self.set 'isSaving', false
        self.set 'commRateChanged', false

    showCommission: (commission) ->
      @set 'selectedCommission', commission
      @set 'showCommission', true



  init: ->
    commissionRates = @store.peekAll('commissionRate')
    if @get('model.settlement.content.loanApplication.content.settlementBank')
      filterBank = @get('model.settlement.content.loanApplication.content.settlementBank')
      commissionRatesForBank = commissionRates.filterBy('bank', filterBank)
    if commissionRatesForBank
      if commissionRatesForBank.get('length') == 1
        commissionRate = commissionRatesForBank.get('firstObject')
        unless @get('model.commissionRate') == commissionRate
          @set('model.commissionRate', commissionRate)
          @set('commRateChanged', true)
      if commissionRatesForBank.get('length') > 0
        @set 'commissionRates', commissionRatesForBank
      else
        @set 'commissionRates', commissionRates
    else
      @set 'commissionRates', commissionRates

    @_super()
    securedLoanTypes = @store.peekAll('liabilityType').filterBy('isSecured')
    @set 'loanTypes', securedLoanTypes
    frequencies = @store.peekAll('frequency')
    paymentFrequencies = frequencies.filter (frequency) ->
      frequencyName = frequency.get('name')
      ['Weekly', 'Fortnightly', 'Monthly'].includes(frequencyName)

    @set 'paymentFrequencies', paymentFrequencies

  willDestroyElement: ->
    if @get('model.hasDirtyAttributes')
      @model.save()
    @_super()




)

`export default ViewsLoanApplicationsSettlementsSingleSettlementViewComponent`
