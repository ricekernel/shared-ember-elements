`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsApplicationSettlementsComponent = Ember.Component.extend(

  ###
  Popover component for viewing and adding settlements.

  Needs:
    model (loanApplication)

  ###


  classNames: ['presentation-object-inner s24 flex-column-nowrap']
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  settlementSort: ['settlementDate']
  sortedSettlements: Ember.computed.sort('model.settlements', 'settlementSort')


  actions:
    addSettlement: ->
      self = @
      @set 'addingSettlement', true
      model = @get('model.content') || @get('model')
      model.addSettlement().then ((newSettlement) ->
        self.set 'addingSettlement', false
        self.send('showSettlement', newSettlement)
        return false
        ), (error) ->
          self.set 'addingSettlement', false
          return false


    removeSettlement: (settlement) ->
      unless settlement.get('cantDelete')
        @get('model.settlements').removeObject(settlement)
        settlement.destroySettlement()
        @set 'selectedSettlement', null
      return false
      #settlement.destroyRecord()

    showSettlement: (settlement) ->
      @set 'selectedSettlement', settlement
      @set('showSettlement', true)
      return false


  didInsertElement: ->
    @_super()
    if @get('model.settlements.length') == 1
      settlement = @get('model.settlements.firstObject')
      Ember.run.later(this, ->
        @send('showSettlement', settlement)
      200)

    else
      if @get('model.settlements.length') == 0
        @send 'addSettlement'


)

`export default ViewsLoanApplicationsSettlementsApplicationSettlementsComponent`
