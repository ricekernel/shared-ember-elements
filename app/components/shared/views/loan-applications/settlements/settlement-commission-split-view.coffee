`import Ember from 'ember'`

ViewsLoanApplicationsSettlementsSingleSettlementViewComponent = Ember.Component.extend(

  #Table row for displaying a firm or user's share of a commission
  session: Ember.inject.service('session')

  firmUsers: []


  estimatedValue: Ember.computed('model.split', 'commission.estimatedValue', ->
    return parseFloat(@get('model.split')) * parseFloat(@get('commission.estimatedValue'))
    )



  init: ->
    self = @
    currentUser = @get('session.currentUser')
    @get('firmUsers').pushObject(currentUser)
    currentFirm = @get('session.currentFirm')
    ###
    users = currentFirm.get('users')
    users.forEach (user) ->
      unless self.get('firmUsers').includes(user)
        self.get('firmUsers').pushObject(user)
    ###
    @_super()



)

`export default ViewsLoanApplicationsSettlementsSingleSettlementViewComponent`
