`import Ember from 'ember'`
`import ENV from "../../../../config/environment"`

MenusUserMenuComponent = Ember.Component.extend(

  classNames: ['user-menu']

  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  model: Ember.computed('session.currentUser', ->
    if @get('session.currentUser')
      return @get('session.currentUser')
    else
      testUser = @store.createRecord('user',
        name: "Unknown User"
        email: "support@socket.co.nz"
      )
      return testUser
    )


  actions:
    logout: ->
      @set 'loggingOut', true
      revokeTokenURL = "#{ENV.APP.ADAPTER_HOST}/oauth/revoke"
      signOutURL = "#{ENV.APP.ADAPTER_HOST}/users/log_out"
      revokeHttp = new XMLHttpRequest

      self = @

      @get('session').authorize('authorizer:oauth2', (header,token)->
        revokeHttp.open("POST", revokeTokenURL, true)
        revokeHttp.setRequestHeader('authorization', token)
        revokeHttp.onreadystatechange = ->
          if revokeHttp.readyState == 4 and revokeHttp.status == 200
            self.get('session').invalidate().then ->
              window.location = signOutURL

          else
            if revokeHttp.status > 399
              alert 'There was a problem logging out, please try again'

        revokeHttp.send()
      )

      return false

    launchSettings: ->
      settingsPath = ENV.APP.SETTINGS_PATH
      window.open(settingsPath)
      return false

    toggleMenu: ->
      @toggleProperty('showUserMenu')
      return false

)

`export default MenusUserMenuComponent`
