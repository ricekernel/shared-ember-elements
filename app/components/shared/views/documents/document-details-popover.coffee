`import Ember from 'ember'`
`import DetailsAddSaveMixin from '../../../../mixins/shared/details-add-save-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(DetailsAddSaveMixin,

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']
  ###
  actions:
    deleteDocLink: ->
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      loanApplication.get('documents').removeObject(model)
      loanApplication.save()
      return false

    saveDocument: ->
      self = @
      model = @get('model.content') || @get('model')
      model.save().then ->
        self.set('closeAttribute', false)
      return false
      ###

)

`export default BorrowersIndividualTableRowComponent`
