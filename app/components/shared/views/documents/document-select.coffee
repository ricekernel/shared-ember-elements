`import Ember from 'ember'`

ViewsDocumentsDocumentSelectComponent = Ember.Component.extend(

  ###
  A table of documents with a selectbox next to each one.  Similar to client search
  - can pass it an 'owner' and will list all documents belonging to that owner, or pass it an array of documents
  Needs:
    - action (action): the parent view's action to send the document(s) selected into
  Options:
    - documentOwner (loan app, individual, user, firm): will display all documents belonging to that owner
    - documents (array of documents): will display them instead
    - multiSelect (boolean): if true, can select a number of documents, otherwise can only select one
  ###

  classNames: ['s24 flex-column-nowrap flex-align-centre flex-shrink-grow']
  selectedDocuments: []
  continueButtonTitle: 'continue'
  multiSelect: false #if passed as true, user can select multiple clients, rather than just one

  model: Ember.computed('documentOwner', 'documents', ->
    if @get 'documentOwner'
      owner = @get 'documentOwner'
      return owner.get('documents')
    else
      return @get 'documents'
    )


#computeds:
  notSelected: Ember.computed.setDiff('model', 'selectedDocuments')
  allSelected: Ember.computed.equal('notSelected.length', 0)

  multiSelectContinue: Ember.computed.and('multiSelect') #if multiSelect and at least one client selected, shows 'continue' button


  actions:
    addDocument: ->
      @set 'addDocument', true


    multiSelectContinue: ->
      #@set 'client', true #users of the tool are bound to the client. By setting to true, it changes their state
      selectedDocuments = @get('selectedDocuments')
      @sendAction('action', selectedDocuments)
      selectedDocuments.clear()

    selectDocument: (doc) ->
      if @get 'multiSelect'
        if @get('selectedDocuments').includes(doc)
          @get('selectedDocuments').removeObject(doc)
          #client.set('isSelected', false)
        else
          @get('selectedDocuments').pushObject(doc)
          #client.set('isSelected', true)
      else
        #@set 'client', client
        @sendAction('action', doc)

    toggleSelectAll: ->
      self = @
      if @get 'allSelected'
        @get('model').forEach (doc) ->
          self.get('selectedDocuments').removeObject(doc)
      else
        @get('model').forEach (doc) ->
          unless self.get('selectedDocuments').includes(doc)
            self.get('selectedDocuments').pushObject(doc)



)

`export default ViewsDocumentsDocumentSelectComponent`
