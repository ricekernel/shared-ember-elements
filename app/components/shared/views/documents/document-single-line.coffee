`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../mixins/shared/table-options-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableRowOptionsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  lastModifiedDisplay: Ember.computed('lastModified', ->
    return 'last modified ' + @model.get('lastModified')
    )

  actions:
    viewDocument: ->
      model = @get('model.content') || @get('model')
      model.viewDocument()
      return false

)

`export default BorrowersIndividualTableRowComponent`
