`import Ember from 'ember'`

ViewsDocumentsDocumentSelectByOwnerComponent = Ember.Component.extend(
  session: Ember.inject.service('session')

  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-align-centre']

  attachmentsArray: []


  documentOwners: Ember.computed('session.currentUser', 'owners.[]', ->
    if @get('owners.length') > 0
      documentOwners = @get('owners').toArray().copy()
    else
      documentOwners = []
    currentUser = @get('session.currentUser')
    documentOwners.pushObject(currentUser)
    currentFirm = currentUser.get('currentFirm')
    if currentFirm
      documentOwners.pushObject(currentFirm)
    return documentOwners.uniq().compact()
    )

  actions:
    selectDocumentOwner: (owner) ->
      @set 'documentOwner', owner
      @set 'documentSelection', true

    selectDocuments: (documents) ->
      @sendAction('action', documents)
)

`export default ViewsDocumentsDocumentSelectByOwnerComponent`
