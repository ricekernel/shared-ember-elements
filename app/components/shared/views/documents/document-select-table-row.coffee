`import Ember from 'ember'`

ViewsDocumentsDocumentSelectTableRowComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start hover-grey cursor-pointer']

  isSelected: Ember.computed('model', 'selectedDocuments.[]', ->
    return false unless @get('model') and @get('selectedDocuments')
    return true if @get('selectedDocuments').includes(@get('model'))
    )
)

`export default ViewsDocumentsDocumentSelectTableRowComponent`
