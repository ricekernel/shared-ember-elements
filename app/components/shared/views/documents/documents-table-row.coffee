`import Ember from 'ember'`
`import TableRowOperationsMixin from '../../../mixins/shared/table-row-operations-mixin'`

ViewsDocumentsDocumentsTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  documentPurposes: []

  lastModifiedDisplay: (->
    return 'last modified ' + @model.get('lastModified')
    ).property 'model.lastModified'

  shortenedDocumentName: (->
    return 'Unnamed document' unless @model.get('name')
    name = @model.get('name')
    extension = ''
    if name.length > 35
      extension = '...'
    return name.substring(0,31) + extension
    ).property 'model.name'

  init: ->
    @set('documentPurposes', @get('model.store').peekAll('documentPurpose').rejectBy('name', 'Application form'))
    @_super(arguments...)

  actions:
    viewDocument: (doc)->
      @get('model').viewDocument()


)

`export default ViewsDocumentsDocumentsTableRowComponent`
