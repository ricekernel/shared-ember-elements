`import Ember from 'ember'`

ColouredTagComponent = Ember.Component.extend(

  classNameBindings: [':shared-tag', 'canRemove:show-remove']

  actions:
    removeTag: ->
      tag = @get('tag')
      @sendAction('action', tag)
)

`export default ColouredTagComponent`
