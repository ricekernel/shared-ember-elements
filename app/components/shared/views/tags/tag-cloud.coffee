`import Ember from 'ember'`

ElementsTagCloudComponent = Ember.Component.extend(
  ###
  Needs:
    tags (array): array of tags as strings.  Can be empty
    store: reference to the store so we can query it

  Optional:
    isEditable (bool): if true, tags have x to remove
    canAddTags (bool): if true, bottom of cloud has an add tag input
    tagOwner (record): needed if we are going to save the tags
  ###
  newTag: ''
  possibleNewTagMatches: []
  noTags: Ember.computed.empty('tags')

  tagArray: Ember.computed('tags', ->
    return [] if @get('noTags')
    @get('tags')
  )

  showInstructions: Ember.computed.and('noTags', 'canAddTags')

  validNewTag: Ember.computed('newTag', ->
    if @get('newTag.length') > 2
      return true
    )

  serverTagAutocomplete: (->
    self = @
    if @get 'validNewTag'
      newTag = @get('newTag').toLowerCase()
      @get('store').query('tag', {filter: { name: newTag } }).then (matches) ->
        if self.get('validNewTag') #was bugging out when the results came after we had left field. So check a second time
          self.set 'possibleNewTagMatches', matches
    else
      @set 'possibleNewTagMatches', []
    ).observes 'newTag'

  showServerTagMatches: Ember.computed.notEmpty('possibleNewTagMatches')

  keyPress: (e) ->
    if e.keyCode == 13 or e.keyCode == 9
      @send('addNewTag')

  actions:
    removeTag: (tag) ->
      @get('tags').removeObject(tag)
      tagOwner = @get('tagOwner')
      tagOwner.save()

    addNewTag: ->
      newTag = @get('newTag')
      @get('tags').pushObject(newTag)
      @set('newTag', '')
      @set 'possibleNewTagMatches', []
      tagOwner = @get('tagOwner')
      tagOwner.save()

    addTagFromServerTags: (serverTag) ->
      @set 'newTag', serverTag
      @send 'addNewTag'
)

`export default ElementsTagCloudComponent`
