`import Ember from 'ember'`

PricingOfferEntryComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  loanTypes: []

  ###
  floatingRates: [
    {
      label: "Floating"
      interestRate: null
    },
    {
      label: "Revolving"
      interestRate: null
    }
  ]

  fixedRates: [
    {
      label: "6M"
      interestRate: null
    },
    {
      label: "1Y"
      interestRate: null
    },
    {
      label: "18M"
      interestRate: null
    },
    {
      label: "2Y"
      interestRate: null
    },
    {
      label: "3Y"
      interestRate: null
    },
    {
      label: "4Y"
      interestRate: null
    },
    {
      label: "5Y"
      interestRate: null
    },
    {
      label: "7Y"
      interestRate: null
    }
  ]
  ###

  init: ->
    @_super()
    loanTypes = @store.peekAll('liabilityType').filterBy('isSecured')
    @set('loanTypes', loanTypes)

)

`export default PricingOfferEntryComponent`
