`import Ember from 'ember'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  actions:
    save: ->
      model = @get('model')
      if @attrs.offerSaveFunction
        @attrs.offerSaveFunction(model)
      else
        model.save()
      return false

    delete: ->
      model = @get('model')
      model.destroyRecord()
      return false

)

`export default BorrowersIndividualTableRowComponent`
