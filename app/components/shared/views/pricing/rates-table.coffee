`import Ember from 'ember'`

RatesTableComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']


  termsInModel: Ember.computed('model.@each.isUpdated', ->
    terms = []
    allRates = []
    @get('model').forEach (pricingOffer) ->
      rates = pricingOffer.get('pricingOfferRates.content')
      rates.forEach (rate) ->
        if rate.get('interestRate')
          allRates.pushObject(rate)
    allRates.sortBy('fixedRateMonths').forEach (term) ->
      typeSummary = term.get('typeSummary')
      unless terms.includes(typeSummary)
        terms.pushObject(typeSummary)
    return terms
    )



  actions:
    addLender: ->
      model = @get('model')
      newPricingOffer = @store.createRecord('pricingOffer')
      newPricingOffer.createDefaultSetOfRates()
      model.pushObject(newPricingOffer)
      newPricingOffer.set('showDetails', true)


)

`export default RatesTableComponent`
