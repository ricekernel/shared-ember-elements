`import Ember from 'ember'`

PricingOfferSingleLineComponent = Ember.Component.extend(

  classNames: ['flex-row-nowrap']

  notify: Ember.inject.service('notify')

  productsToShow: []

  displayRates: Ember.computed('productsToShow.[]', 'model.pricingOfferRates.@each.interestRate', ->
    displayRates = []
    self = @
    @get('productsToShow').forEach (item) ->
      matchingRate = self.get('model.pricingOfferRates').findBy('typeSummary', item)
      if matchingRate
        displayRates.pushObject(matchingRate)
      else
        displayRates.pushObject({})
    return displayRates
    )

  ratesObserver: Ember.observer('model.pricingOfferRates.@each.interestRate', 'model.cashContribution', 'model.bank', 'model.monthlyFees', ->
    #toggling this property updates the rates table column headers
    @toggleProperty('model.isUpdated')
    )



  actions:
    edit: ->
      @set('model.showDetails', true)
      return false

    remove: ->
      model = @get('model')
      model.destroyRecord()
      return false

)

`export default PricingOfferSingleLineComponent`
