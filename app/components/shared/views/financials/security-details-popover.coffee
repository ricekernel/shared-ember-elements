`import Ember from 'ember'`
`import DetailsAddSaveMixin from '../../../../mixins/shared/details-add-save-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

AssetDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  bankNames: []

  bank: null
  bankObserver: Ember.observer('bank', ->
    model = @get('model.content') || @get('model')
    bankName = @get('bank')
    if bankName == 'No Mortgage'
      model.set('bank', null)
    else
      bank = @store.peekAll('bank').findBy('name', bankName)
      model.set('bank', bank)
    )

  actions:
    customUndo: ->
      model = @get('model.content') || @get('model')
      address = model.get('address.content')
      address.rollbackAttributes()
      model.rollbackAttributes()
      return false


  init: ->
    @_super()
    bankNames = @store.peekAll('bank').mapBy('name')
    bankNames.pushObject('No Mortgage')
    @set('bankNames', bankNames)
    if @get('model.bank.id')
      bank = @get('model.bank.name')
      @set('bank', bank)
    else
      @set('bank', 'No Mortgage')


)

`export default AssetDetailsPopoverComponent`
