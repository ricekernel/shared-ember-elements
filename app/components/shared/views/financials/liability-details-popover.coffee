`import Ember from 'ember'`
`import DetailsAddSaveMixin from '../../../../mixins/shared/details-add-save-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

`import LoanCalculationsMixin from '../../../../mixins/shared/loan-calculations-mixin'`

LiabilityDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin, LoanCalculationsMixin,

  ###
  attributes:
    hideRepayments: if true, the repayments section is hidden
    homeLoansOnly: if true, liabilityTypes are filtered to isSecured
    possibleOwners: an array of clients who might own this liability (such as related clients)
    showLiabilitySettlementAction: if true, liability settlement action displays
  ###

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  liabilityTypes: []
  possibleOwners: []

  isPartialRepay: Ember.computed.equal('model.liabilitySettlementAction', 'Partially Repaid')

  showRepayments: Ember.computed('model.liabilityType', ->
    if @get('hideRepayments')
      return false
    if @get('model.requiresTerm')
      return true
    if ['Hire purchase', 'Store card', 'Other loan'].includes(@get('model.liabilityType.name'))
      return true
    )

  repaymentCalculator: Ember.observer('model.remainingMonths', 'model.interestRate', 'model.limit', ->
    term = @get('model.remainingMonths')
    rate = @get('model.interestRate')
    limit = @get('model.limit')
    monthly = @store.peekAll('frequency').findBy('name', 'Monthly')
    if limit > 0 and rate > 0 and term > 0
      pmt = @calculateLoanRepayment(limit, term, rate, 12, false)
      @set('model.repayments', pmt)
      @set('model.frequency', monthly)
    )


  settlementActions: Ember.computed('model.liabilityType.isSecured', ->
    actions = ['Fully Repaid', 'Partially Repaid']
    if @get('model.liabilityType.isSecured')
      actions.unshiftObjects(['Kept (will not refinance)', 'Kept (will refinance if lender changes)'])
    else
      actions.unshiftObjects(['Kept'])
    return actions
    )


  settlementAction: Ember.computed('model.loanPartyLiabilityLink.liabilitySettlementAction', 'model.liabilityType',
    get: (key) ->
      value = @get('model.loanPartyLiabilityLink.liabilitySettlementAction')
      if @get('model.liabilityType.isSecured')
        return value
      else
        if ['Kept (will not refinance)', 'Kept (will refinance if lender changes)'].includes(value)
          return 'Kept'
        else
          return value

    set: (key, value) ->
      if value == "Kept"
        @set('model.loanPartyLiabilityLink.liabilitySettlementAction', 'Kept (will not refinance)')
      else
        @set('model.loanPartyLiabilityLink.liabilitySettlementAction', value)
      return value
    )


  init: ->
    @_super()
    if @get('homeLoansOnly')
      liabilityTypes = @store.peekAll('liabilityType').filterBy('isSecured')
    else
      liabilityTypes = @store.peekAll('liabilityType')
    @set('liabilityTypes', liabilityTypes)
    if @get('loanParty') && @get('possibleOwners.length') == 0
      possibleOwners = @get('loanParty.clients')
      @set('possibleOwners', possibleOwners)
)

`export default LiabilityDetailsPopoverComponent`
