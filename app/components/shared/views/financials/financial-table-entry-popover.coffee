`import Ember from 'ember'`

AssetDetailsPopoverComponent = Ember.Component.extend(

  #classNames: ['s02 flex-justify-centre']
  remainingExpenses: Ember.computed('model.@each.expenseType', ->
    expenseTypes = @store.peekAll('expenseType').sortBy('sortOrder')
    usedTypes = @get('model').mapBy('expenseType')
    remainingExpenses = []
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    expenseTypes.forEach (type) ->
      unless usedTypes.includes(type)
        remainingExpense = {
          expenseType: type
          value: null
          frequency: monthlyFrequency
        }
        remainingExpenses.pushObject(remainingExpense)
    return remainingExpenses

    )

  actions:
    viewTable: ->
      @set('showTable', true)
      return false

    saveExpenses: ->
      self = @
      party = @get('party')
      self.set('isSaving', true)
      loanApplication = party.get('loanApplication')
      clients = party.get('clients')
      remainingExpenses = @get('remainingExpenses')
      expenseSaves = []
      party.get('expenses').forEach (exp) ->
        expenseSaves.pushObject(exp.save()) #have to save all, as hasDirtyAttributes wouldn't catch type or freq change

      remainingExpenses.forEach (expense) ->
        if expense.value > 0
          newExpense = self.store.createRecord('expense',
            expenseType: expense.expenseType
            frequency: expense.frequency
            value: expense.value
            clients: clients
          )
          expenseSaves.pushObject(newExpense.save())
          party.get('expenses').pushObject(newExpense)
      Ember.RSVP.all(expenseSaves).then ( (response) ->
        self.set('isSaving', false)
        self.set('showTable', false)

        return false
      ), (error) ->
        self.set('isSaving', false)
        self.set('showTable', false)
        return false


)

`export default AssetDetailsPopoverComponent`
