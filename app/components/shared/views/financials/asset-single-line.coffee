`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../mixins/shared/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin, TableRowOptionsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

)

`export default BorrowersIndividualTableRowComponent`
