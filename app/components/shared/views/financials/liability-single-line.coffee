`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../mixins/shared/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableRowOptionsMixin, AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']
  ###
  actions:
    deleteRecord: ->
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      link = model.get('loanPartyLiabilityLink')
      loanParty = link.get('loanParty')
      loanParty.get('loanPartyLiabilityLinks').removeObject(link)
      model.destroyRecord()
      loanApplication.save()
      return false
      ###



)

`export default BorrowersIndividualTableRowComponent`
