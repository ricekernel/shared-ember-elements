`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../mixins/shared/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableRowOptionsMixin, AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  isSelected: Ember.computed('selectedArray.[]', ->
    if @get('selectedArray')
      model = @get('model.content') || @get('model')
      selectedArray = @get('selectedArray.content') || @get('selectedArray')
      if selectedArray.includes(model)
        return true
    )


  actions:
    customUndo: ->
      model = @get('model.content') || @get('model')
      address = model.get('address.content') || model.get('address')
      if address
        address.rollbackAttributes()
      model.rollbackAttributes()
      return false

)

`export default BorrowersIndividualTableRowComponent`
