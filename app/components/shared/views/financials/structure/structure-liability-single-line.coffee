`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../../mixins/shared/table-options-mixin'`

StructureLiabilitySingleLineComponent = Ember.Component.extend(TableRowOptionsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  loanTypes: ["Revolving credit", "Floating", "Fixed 6M", "Fixed 1Y", "Fixed 18M", "Fixed 2Y", "Fixed 3Y", "Fixed 4Y", "Fixed 5Y", "Other"]

  loanType: Ember.computed('model.liabilityType', 'model.fixedRateMonths',
    get: (key) ->
      type = @get('model.liabilityType.name')
      fixedRateMonths = @get('model.fixedRateMonths')
      switch type
        when 'Floating home loan' then loanType = 'Floating'
        when 'Fixed home loan'
          if fixedRateMonths % 12 == 0
            loanType = "Fixed #{fixedRateMonths/12}Y"
          else
            loanType = "Fixed #{fixedRateMonths}M"

        else
          loanType = type

      unless @get('loanTypes').includes(loanType)
        @get('loanTypes').pushObject(loanType)
      return loanType


    set: (key, value) ->
      switch value
        when "Revolving credit"
          @util_SetTypeAndMonths("Revolving credit", 0)
        when "Floating"
          @util_SetTypeAndMonths("Floating home loan", 0)
        when "Other"
          @set('model.showDetails', true)
        else
          type = 'Fixed home loan'
          if value.includes('M')
            months = parseInt(value.substring(value.length - 3, value.length - 1))
            @util_SetTypeAndMonths(type, months)
          else
            months = parseInt(value.substring(value.length - 3, value.length - 1)) * 12
            @util_SetTypeAndMonths(type, months)
      return value
  )


  util_SetTypeAndMonths: (typeName, months) ->
    type = @store.peekAll('liabilityType').findBy('name', typeName)
    @set("model.liabilityType", type)
    @set("model.fixedRateMonths", months)


  actions:
    editDetails: ->
      @set('model.showDetails', true)
      return false

    deleteLiability: ->
      model = @get('model')
      @attrs.deleteLiability(model)
      return false

    saveLiability: ->
      model = @get('model')
      @attrs.saveLiability(model)
      return false



)

`export default StructureLiabilitySingleLineComponent`
