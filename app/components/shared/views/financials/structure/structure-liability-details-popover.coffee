`import Ember from 'ember'`

StructureLiabilityDetailsPopoverComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  homeLoanTypes: []

  isRevolvingCredit: Ember.computed.equal('model.liabilityType.name', 'Revolving credit')

  loadPaymentDetails: true
  showInterestRate: true

  actions:
    save: ->
      @attrs.saveLiability()
      return false

    delete: ->
      @attrs.deleteLiability()
      return false

    setRepayment: (pmt) ->
      @set('model.repayments', pmt)
      return false


  init: ->
    @_super()
    homeLoanTypes = @store.peekAll('liabilityType').filterBy('isSecured', true)
    @set('homeLoanTypes', homeLoanTypes)


)

`export default StructureLiabilityDetailsPopoverComponent`
