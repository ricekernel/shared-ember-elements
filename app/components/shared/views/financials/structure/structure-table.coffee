`import Ember from 'ember'`

StructureTableComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  notify: Ember.inject.service('notify')
  update: false
  targetTotal: 0


  sortedLiabilities: Ember.computed('update', ->
    return @get('model.loanStructureLiabilities').sortBy('sortOrder')
    )

  actions:
    addStructureLoan: ->
      model = @get('model')
      targetTotal = @get('targetTotal') || 0
      newLoan = model.addStructureLoan(targetTotal)
      @toggleProperty('update')
      return false

    save: ->
      if @attrs.saveStructure
        @attrs.saveStructure()
        @toggleProperty('update')
        return false

    saveLiability: (lia) ->
      if @attrs.saveStructure
        @attrs.saveStructure()
      lia.set('showDetails', false)
      @toggleProperty('update')
      return false

    deleteLiability: (lia) ->
      model = @get('model')
      model.get('loanStructureLiabilities').removeObject(lia)
      lia.deleteRecord()
      @attrs.saveStructure()
      return false




  ###
  difference: Ember.computed('model.loanApplication.applicationValue', 'model.totalLending', ->
    diff =  @get('model.totalLending') - @get('model.loanApplication.applicationValue')
    if diff < 0
      diff = diff * -1
    return diff
    )


  sortedLiabilities: Ember.computed('update', 'model.liabilities.content.[]', ->
    return @get('model.liabilities.content').sortBy('sortOrder')
    )


  actions:
    addStructureLoan: ->
      model = @get('model.content') || @get('model')
      newLoan = model.addLoan()
      newLoan.set('limit', @get('difference'))
      @toggleProperty('update')
      return false

    saveStructure: ->
      self = @
      notify = @get('notify')
      self.set('saving', true)
      structure = @get('model.content') || @get('model')
      saves = []
      structure.get('liabilities.content').forEach (lia) ->
        saves.pushObject(lia.save())  #should check for hasDirtyAttributes, but changing the type doesn't impact this
      Ember.RSVP.all(saves).then (->
        self.toggleProperty('update')
        structure.save()
        notify.success("Loan Structure saved!")
        self.set('saving', false)
        ), (error) ->
          notify.error("There was a problem, please try again")
          self.set('saving', false)
      ###


)

`export default StructureTableComponent`
