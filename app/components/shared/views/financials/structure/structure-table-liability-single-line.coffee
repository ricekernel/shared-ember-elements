`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../../mixins/shared/table-options-mixin'`

StructureTableLiabilitySingleLineComponent = Ember.Component.extend(TableRowOptionsMixin,

  classNameBindings: [':s24', ':flex-row-nowrap', ':flex-align-centre', ':table-single-line', 'notSelected:reduced-opacity']

  selected: Ember.computed('selectedArray.[]',
    get: (key) ->
      model = @get('model')
      unless @get('selectedArray') then return false
      if @get('selectedArray').includes(model)
        return true
    set: (key, value) ->
      model = @get('model')
      if @get('selectedArray').includes(model)
        @get('selectedArray').removeObject(model)
        return false
      else
        @get('selectedArray').pushObject(model)
        return true
    )

  notSelected: Ember.computed.not('selected')

  pricingOfferRate: Ember.computed('pricingOffer.pricingOfferRates.@each.liabilityType', 'pricingOffer.pricingOfferRates.@each.fixedRateMonths', ->
    return false unless @get('pricingOffer')
    model = @get('model')
    ###
    pricingOffer = @get('pricingOffer.id')
    return @get('model.pricingOfferRates.content').findBy('pricingOffer.content.id', pricingOffer)
    ###
    pricingOffer = @get('pricingOffer')
    return model.matchPricingOfferRateFromPricingOffer(pricingOffer)
    #return pricingOffer.get('pricingOfferRates.content').filterBy('liabilityType', 'model.liabilityType').findBy('fixedRateMonths', 'model.fixedRateMonths')
    )
)

`export default StructureTableLiabilitySingleLineComponent`
