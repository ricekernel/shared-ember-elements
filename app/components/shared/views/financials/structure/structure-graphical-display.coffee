`import Ember from 'ember'`

StructureGraphicalDisplayComponent = Ember.Component.extend(

  classNames: ['structure-graphical-display-container']

  notify: Ember.inject.service('notify')


  structure: [
    {
      summary: 'Revolving'
      value: '$50,000'
      width: '10%'
      class: 's0'
    },
    {
      summary: 'Floating'
      value: '$100,000'
      width: '10%'
      class: 's1'
    },
    {
      summary: 'Fixed 6M'
      value: '$50,000'
      width: '10%'
      class: 's2'
    }
    ,
    {
      summary: 'Fixed 1Y'
      value: '$50,000'
      width: '10%'
      class: 's3'
    }
    ,
    {
      summary: 'Fixed 18M'
      value: '$50,000'
      width: '10%'
      class: 's4'
    }
    ,
    {
      summary: 'Fixed 2Y'
      value: '$50,000'
      width: '10%'
      class: 's5'
    }
    ,
    {
      summary: 'Fixed 3Y'
      value: '$50,000'
      width: '10%'
      class: 's6'
    }
    ,
    {
      summary: 'Fixed 4Y'
      value: '$50,000'
      width: '10%'
      class: 's7'
    }
    ,
    {
      summary: 'Fixed 5Y'
      value: '$25,000'
      width: '10%'
      class: 's8'
    }
    ,
    {
      summary: 'Fixed 7Y'
      value: '$25,000'
      width: '10%'
      class: 's9'
    }
  ]


  actions:
    addStructureLoan: ->
      model = @get('model')
      targetTotal = @get('targetTotal') || 0
      newLoan = model.addStructureLoan(targetTotal)
      @toggleProperty('update')
      return false

    save: ->
      if @attrs.saveStructure
        @attrs.saveStructure()
        @toggleProperty('update')
        return false

    saveLiability: (lia) ->
      if @attrs.saveStructure
        @attrs.saveStructure()
      lia.set('showDetails', false)
      @toggleProperty('update')
      return false

    deleteLiability: (lia) ->
      model = @get('model')
      model.get('loanStructureLiabilities').removeObject(lia)
      lia.deleteRecord()
      @attrs.saveStructure()
      return false





)

`export default StructureGraphicalDisplayComponent`
