`import Ember from 'ember'`

StructureTableLiabilitySingleLineComponent = Ember.Component.extend(

  classNames: ['structure-component-row']

  click: (e) ->
    model = @get('model.lia')
    model.set('showDetails', true)
    return false


  actions:
    saveLiability: ->
      model = @get('model.lia')
      model.set('showDetails', false)
      if @get('onUpdate')
        @attrs.onUpdate()
      return false

    deleteLiability: ->
      model = @get('model.lia')
      structure = model.get('loanStructure')
      structure.get('loanStructureLiabilities').removeObject(model)
      model.deleteRecord()
      if @get('onUpdate')
        @attrs.onUpdate()
      model.set('showDetails', false)
      return false

    showDetails: ->
      model = @get('model.lia')
      model.set('showDetails', true)
      return false
)

`export default StructureTableLiabilitySingleLineComponent`
