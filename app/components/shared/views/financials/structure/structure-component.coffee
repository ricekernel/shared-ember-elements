`import Ember from 'ember'`

`import accounting from 'accounting'`

StructureTableComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-wrap']

  notify: Ember.inject.service('notify')

  onUpdate: false

  structureLoans: Ember.computed('model.loanStructureLiabilities.@each.showDetails', ->
    self = @
    structureLoans = []
    @get('model.loanStructureLiabilities').sortBy('liabilityType.sortOrder').sortBy('fixedRateMonths').forEach (lia) ->
      liaObject = {
        lia: lia
        colour: self.rgbaForTypeSummary( lia.get('typeSummary2') )
      }
      structureLoans.pushObject(liaObject)
    return structureLoans
    )


  structureLoansObs: Ember.observer('structureLoans', ->
    if @get('structureDonut')
      @updateDonut()
    else
      Ember.run.later(this, ->
        @initDonut()
        2200
        )
    )




  donutOptions:
    responsive: true
    maintainAspectRatio: true
    legend:
      display: false
    hover:
      mode: 'label'
    tooltips:
      mode: 'label'
      callbacks:
        title: (tooltipItem, data) ->
          return 'Proposed Loan'
        label: (tooltipItem, data) ->
          label = "#{data.labels[tooltipItem.index]}: #{accounting.formatMoney(data.datasets[0].data[tooltipItem.index], '$', 2)}"
          return label


  actions:
    addLoan: ->
      model = @get('model')
      newLoan = model.addStructureLoan()
      newLoan.set('showDetails', true)
      return false



  init: ->
    @_super()
    unless @get('model')
      newStructure = @store.createRecord('loanStructure')
      @set('model', newStructure)
    if @get('model.loanStructureLiabilities.length') > 0
      Ember.run.later(this, ->
        @initDonut()
        2200
        )






  initDonut: ->
    donutOptions = @get 'donutOptions'
    structureDonutData = {
      labels: ["No Proposed Loans"]
      datasets: [
        {
          data: [0]
          backgroundColor: ['rgba(73, 203, 167, 1.00)']
        }
      ]
    }
    ctx = document.getElementById("structureDonut").getContext("2d")

    structureDonut = new Chart(ctx,
      type: 'doughnut'
      data: structureDonutData
      options: donutOptions
    )
    @set 'structureDonut', structureDonut
    Ember.run.later(this, ->
      @updateDonut()
      1200
      )



  updateDonut: ->
    structureDonut = @get 'structureDonut'
    structureDonut = @getDonutData(structureDonut)
    structureDonut.update()


  getDonutData: (structureDonut) ->
    self = @
    structureDonut.data.datasets[0].data = []
    structureDonut.data.datasets[0].backgroundColor = []
    structureDonut.data.labels = []
    @get('structureLoans').forEach (lia) ->
      liaRecord = lia.lia
      liaType = liaRecord.get('typeSummary2')
      liaColour = lia.colour
      structureDonut.data.labels.pushObject(liaType)
      structureDonut.data.datasets[0].data.pushObject( liaRecord.get('owing') )
      structureDonut.data.datasets[0].backgroundColor.pushObject(liaColour)
    return structureDonut


  rgbaForTypeSummary: (type) ->
    switch type
      when "Revolving Credit" then return "rgba(73, 203, 167, 1.00)"
      when "Floating" then return "rgba(67, 106, 201, 1.00)"
      when "Fixed: 6M" then return "rgba(128, 198, 219, 1.00)"
      when "Fixed: 1Y" then return "rgba(109, 190, 214, 1.00)"
      when "Fixed: 18M" then return "rgba(89, 181, 207, 1.00)"
      when "Fixed: 2Y" then return "rgba(71, 172, 202, 1.00)"
      when "Fixed: 3Y" then return "rgba(59, 160, 190, 1.00)"
      when "Fixed: 4Y" then return "rgba(52, 143, 170, 1.00)"
      else return "rgba(46, 126, 151, 1.00)"

)

`export default StructureTableComponent`
