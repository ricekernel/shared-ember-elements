`import Ember from 'ember'`
`import DetailsAddSaveMixin from '../../../../mixins/shared/details-add-save-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

AssetDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  ###
  securityWillBeSold: Ember.computed('model.loanPartySecurityLink.securitySettlementAction', 'model.loanPartySecurityLink.willRefinance',
    get: (key) ->
      if @get('model.loanPartySecurityLink.securitySettlementAction') == "Sold"
        return true
      else
        return false

    set: (key, value) ->
      if value == true
        @set('model.loanPartySecurityLink.securitySettlementAction', "Sold")
      else
        if @get('model.loanPartySecurityLink.willRefinance')
          @set('model.loanPartySecurityLink.securitySettlementAction', "Kept (will refinance if lender changes)")
        else
          @set('model.loanPartySecurityLink.securitySettlementAction', "Kept (will not refinance)")
      return value
    )
  ###


  ###
  settlementActions: Ember.computed('bank', ->
    actions = ['It will be sold']
    if @get('bank') == 'No Mortgage'
      actions.pushObjects(['It will be kept without mortgage', 'It will be provided as security to the new lender'])
    else
      actions.pushObjects(['It will be provided as security to the new lender', 'It will remain as security with the existing lender'])
    return actions
    )

  settlementAction: Ember.computed('model.loanPartySecurityLink.securitySettlementAction', 'bank',
    get: (key) ->
      actions = @get('actionList')
      value = @get('model.loanPartySecurityLink.securitySettlementAction')
      if @get('bank') == 'No Mortgage'
        action = actions.findBy('value', value)
        if action
          return action.withoutMortgage
        else
          return null

      else
        action = actions.findBy('value', value)
        if action
          return actions.findBy('value', value).withMortgage
        else
          return null

    set: (key, value) ->
      actions = @get('actionList')
      if @get('bank') == 'No Mortgage'
        action = actions.findBy('withoutMortgage', value).value
      else
        action = actions.findBy('withMortgage', value).value
      @set('model.loanPartySecurityLink.securitySettlementAction', action)
      return value
    )
  ###

  bankNames: []

  bank: null
  bankObserver: Ember.observer('bank', ->
    model = @get('model.content') || @get('model')
    bankName = @get('bank')
    if bankName == 'No Mortgage'
      model.set('bank', null)
    else
      bank = @store.peekAll('bank').findBy('name', bankName)
      model.set('bank', bank)
  )

  actionList: [
    {
    value: 'Sold'
    withMortgage: 'It will be sold'
    withoutMortgage: 'It will be sold'
    },
    {
    value: 'Kept (will refinance if lender changes)'
    withMortgage: 'It will be provided as security to the new lender'
    withoutMortgage: 'It will be provided as security to the new lender'
    },
    {
    value: 'Kept (will not refinance)'
    withMortgage: 'It will remain as security with the existing lender'
    withoutMortgage: 'It will be kept without mortgage'
    }
  ]

  assetTypeObserver: Ember.observer('model.assetType', -> #if its a property is creates the necessary linkage for the loan app model
    loanParty = @get('loanParty')
    asset = @get('model')
    unless loanParty && asset then return false
    if @get('assetType.name') == 'Property'
      unless asset.get('loanPartySecurityLink')
        newLink = @store.createRecord('loanPartySecurityLink',
          loanParty: loanParty
          security: asset
        )
        loanParty.get('loanPartySecurityLinks').pushObject(newLink)
    )


  init: ->
    @_super()
    bankNames = @store.peekAll('bank').mapBy('name')
    bankNames.pushObject('No Mortgage')
    @set('bankNames', bankNames)
    if @get('model.bank.id')
      bank = @get('model.bank.name')
      @set('bank', bank)
    else
      @set('bank', 'No Mortgage')




)

`export default AssetDetailsPopoverComponent`
