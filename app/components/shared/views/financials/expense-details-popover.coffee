`import Ember from 'ember'`
`import DetailsAddSaveMixin from '../../../../mixins/shared/details-add-save-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../mixins/shared/add-save-delete-financials-mixin'`

AssetDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']

)

`export default AssetDetailsPopoverComponent`
