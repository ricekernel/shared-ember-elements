`import Ember from 'ember'`

ViewsMessagingNewMessageAddRecipientsComponent = Ember.Component.extend(
  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-align-centre']


  actions:
    clientSearch: ->
      @set 'clientSearch', true

    addEmails: ->
      emailList = @get('emailList')
      if emailList.length > 0
        emailArray = emailList.split(';')
        if emailArray.length > 0

          emailObjectArray = []
          emailArray.forEach (email) ->
            if email.charAt(0) == ' ' #in case they've separated by '; '
              email = email.slice(1)
            emailObject = {
              fullName: email
              type: 'email'
            }
            emailObjectArray.pushObject(emailObject)
          @send('selectRecipients', emailObjectArray)
          @set 'emailList', ''


    selectRecipients: (recipients) ->
      @sendAction('action', recipients)
)

`export default ViewsMessagingNewMessageAddRecipientsComponent`
