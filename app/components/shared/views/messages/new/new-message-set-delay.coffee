`import Ember from 'ember'`

ViewsMessagingNewMessageSetDelayComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']

  ampm: ['am', 'pm']
  sendLater: false
  sendLaterAmPm: 'pm'

#computeds:
  sendLaterTime: Ember.computed('sendLaterTimeRaw',
    get: (key) ->
      return @get('sendLaterTimeRaw')

    set: (key, value) ->
      value = '' + value
      rawTimeBytes = value.split('')
      if rawTimeBytes.length > 2
        if rawTimeBytes.indexOf(':') > -1
          rawTimeBytes.splice(rawTimeBytes.indexOf(':'), 1)
        rawTimeBytes.splice(rawTimeBytes.length-2, 0, ':')
        @set 'sendLaterTimeRaw', rawTimeBytes.reduce (a, b) ->
          a + b
        return rawTimeBytes.reduce (a, b) ->
          a + b
      else
        return value
    )


  actions:
    sendLater: (tf) ->
      @set 'sendLater', tf
      return false

    setDelay:->
      sendLaterTime = @get('sendLaterTime')
      sendLaterDate = moment(@get('sendLaterDate')).format('DDMMYY')
      sendLaterAmPm = @get('sendLaterAmPm')
      if sendLaterTime
        sendTime = @util_CleanTimeFor24hUse(sendLaterTime)
        sendAtString = "#{sendTime}#{sendLaterAmPm}#{sendLaterDate}"
        sendAtDate = moment(sendAtString, "hhmmADDMMYY")
        @set 'message.sendAt', sendAtDate
      @set 'backAttribute', false





  init: ->
    @set 'today', new Date()
    @_super()

  util_CleanTimeFor24hUse: (time) ->
    hours = parseInt(time.split(':')[0])
    #adjust if entered it eg 15:30
    if hours > 12
      hours = hours - 12
    newTime = "#{hours}:#{time.split(':')[1]}"
    return newTime

)

`export default ViewsMessagingNewMessageSetDelayComponent`
