`import Ember from 'ember'`

ViewsMessagingNewMessageComponent = Ember.Component.extend(

  ###
  Popover component for sending messages.

  Options:
    client (client): will add that client to recipients List
    clients (array): array of clients to be sent to
  ###

  classNames: ['presentation-object-inner s24']
  canEditRecipients: true
  clients: []
  loanApplication: null
  bodyRows: 12
  clientMessage: true

  notify: Ember.inject.service('notify')

  messageIsValid: Ember.computed('message.recipients.[]', 'message.body', 'message.to',->
    if @get('message.recipients.length') > 0 or @get('message.to.length') > 6
      if @get('message.body.length') > 2
        return true
    )

  documentOwners: Ember.computed('message.recipients.[]', 'loanApplication', ->
    documentOwners = @get('message.recipients').copy()
    if @get('loanApplication')
      documentOwners.pushObject(@get('loanApplication'))
    return documentOwners
    )

  templateFieldContext: Ember.computed('clientMessage', 'loanApplication', ->
    modelArray = []
    modelArray.pushObject('user')
    if @get('clientMessage')
      modelArray.pushObject('client')
    if @get('loanApplication')
      modelArray.pushObject('loan_application')
    return modelArray
    )


#observers
  clientsObs: Ember.observer('clients.[]', ->
    self = @
    messageRecipients = self.get('message.recipients')
    @get('clients').forEach (client) ->
      unless messageRecipients.includes(client)
        self.get('message.recipients').pushObject(client)

    )


  actions:
    addRecipients: ->
      @set 'recipients', true

    navigate: (section) ->
      @setProperties(
        recipients: false
        templates: false
        addAttachments: false
        delay: false
        preview: false
      )
      if section
        @set(section, true)

    setClientMessage: (tf) ->
      @set 'clientMessage', tf
      if tf == true
        @set 'bodyRows', 12
      else
        @set 'bodyRows', 8
      return false

    setDelay: (delay) ->
      @set 'msgDelay', delay
      @send('navigate')

    removeAttachment: (doc) ->
      @get('message.attachments').removeObject(doc)

    removeRecipient: (client) ->
      @get('message.recipients').removeObject(client)

    selectAttachments: (documents) ->
      self = @
      documents.forEach (doc) ->
        unless self.get('message.attachments').includes(doc)
          self.get('message.attachments').pushObject(doc)
      @send('navigate')

    selectRecipients: (clients) ->
      self = @
      clients.forEach (client) ->
        unless self.get('message.recipients').includes(client)
          self.get('message.recipients').pushObject(client)
      @send('navigate')


    previewMessage: ->
      notify = @get 'notify'
      self = @
      message = @get('message')

      messageText = message.body.replace('{{{', '{{') #this happened a couple of times in testing so put a catch in for

      if @get('composedMessage')
        newMessage =  @get('composedMessage')
      else
        newMessage = self.store.createRecord('message')
      newMessage.setProperties(
        message: message.body
        subject: message.subject
        output: 'email'
        sendAt: message.sendAt
        status: 0 #won't send (we change after preview)
      )

      templateData = {}
      @get('message.recipients').forEach (recipient) ->
        templateData[recipient.get('id')] = {}

        if self.get 'loanApplication'
          templateData[recipient.get('id')] = {
            loan_application_id: self.get('loanApplication.id')
          }

      ### old code:
      if @get 'loanApplication'
        templateData = {}
        @get('message.recipients').forEach (recipient) ->
          templateData[recipient.get('id')] = {
            loan_application_id: self.get('loanApplication.id')
          }
        ###
      newMessage.set('templateData', templateData)


      if @get('clientMessage')
        msgRecipients = message.recipients
        if msgRecipients.length > 1
          clientList = msgRecipients.mapBy('id')
          newMessage.set 'clientList', clientList
        else
          client = msgRecipients.get('firstObject')
          newMessage.set 'client', client

      else
        newMessage.set 'to', message.to
        newMessage.set 'cc', message.cc
        newMessage.set 'bcc', message.bcc

      if message.attachments.length > 0
        attachmentList = []
        message.attachments.forEach (attachment) ->
          attachmentList.pushObject(attachment.get('id'))
        newMessage.set('attachmentList', attachmentList)

      ###
      if @get 'message.Delay'
        sendLaterDate = @get('sendLaterDate')
        sendLaterHourMinute = @get('sendLaterTime').split(':')
        sendLaterMinute = parseInt(sendLaterHourMinute[1])
        sendLaterHour = parseInt(sendLaterHourMinute[0])
        if @get('sendLaterAmPm') == 'pm' && sendLaterHour != 12
          sendLaterHour = sendLaterHour += 12
        if sendLaterHour == 24
          sendLaterHour = 0

        sendLaterDate = moment(new Date(sendLaterDate)) #using new Date() based on this: https://github.com/moment/moment/issues/1407
        sendLaterDate.set('hour', sendLaterHour)
        sendLaterDate.set('minute', sendLaterMinute)
        newMessage.set('sendAt', sendLaterDate)
      ###

      newMessage.set('isSavingPreview', true)
      @set 'composedMessage', newMessage
      @send('navigate', 'preview')

      newMessage.save().then (->
        newMessage.set('isSavingPreview', false)
      ), (error) ->
        newMessage.set('isSavingPreview', false)
        notify.error "We're experiencing problems at the moment. The socket team have been notified.  The error is: #{error}"



  init: ->
    @_super()
    message = {
      recipients: [] #array of clients
      to: ''  #string list of email addresses
      cc: ''  #string list of email addresses
      bcc: ''  #string list of email addresses
      subject: ''
      body: ''
      attachments: []
      sendAt: ''
    }
    @set 'message', message
    if @get('client')
      recipient = @get 'client'
      @send('selectRecipients', [recipient])

    if @get('clients')
      recipients = @get 'clients'
      @send('selectRecipients', recipients)

    if @get('attachments')
      attachments = @get('attachments')
      if attachments.get('content')
        attachments = attachments.get('content')
      @send('selectAttachments', attachments)





)

`export default ViewsMessagingNewMessageComponent`
