`import Ember from 'ember'`

ViewsMessagingNewMessagePreviewComponent = Ember.Component.extend(
  notify: Ember.inject.service('notify')
  classNames: ['s24 presentation-object-inner flex-column-nowrap flex-align-centre']

  actions:
    send: ->
      self = @
      notify = @get 'notify'
      message = @get('message')
      message.set('status', 1)
      @set 'sendingMessage', true
      message.save().then (->
        notify.success 'Message sent!'
        self.set 'sendingMessage', false
        self.set 'closeAttribute', false
      ), (error) ->
        notify.error 'There was a problem sending your message. Please try again'
        self.set 'sendingMessage', false


)

`export default ViewsMessagingNewMessagePreviewComponent`
