`import Ember from 'ember'`

ViewsMessagingNewMessageSelectTemplateComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-align-centre']

  actions:
    selectTemplate: (template) ->
      if template
        subject = template.get('subject')
        message = template.get('message')
        @set 'message.subject', subject
        @set 'message.body', message
      @set('backAttribute', false)
)

`export default ViewsMessagingNewMessageSelectTemplateComponent`
