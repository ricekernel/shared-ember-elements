`import Ember from 'ember'`

ViewsEntitiesEntityFinancialsCardComponent = Ember.Component.extend(

  classNames: ['grey-bordered-white-box-flat pad5']

  ###
  defaultFrequency: Ember.computed('model.store.frequency.[]', ->
    @get('model.store').peekAll('frequency').get('firstObject')
    )
  ###
  liabilityTypes: Ember.computed('store.liabilityType.[]', ->
    @get('store').peekAll('liabilityType')
    )

  assetTypes: Ember.computed('store.assetType.[]', ->
    @get('store').peekAll('assetType')
    )

  securityTypes: Ember.computed('store.securityType.[]', ->
    @get('store').peekAll('securityType')
    )

  incomeTypes: Ember.computed('store.incomeType.[]', ->
    @get('store').peekAll('incomeType')
    )

  expenseTypes: Ember.computed('store.expenseType.[]', ->
    @get('store').peekAll('expenseType')
    )

  ###
  init: ->
    @_super()
    if @get('model.content')
      @set('entity', @get('model.content'))
    else
      @set('entity', @get('model'))
    ###

  actions:
    addAsset: (type) ->
      newAsset = @get('model').addFinancial('asset', type)
      newAsset.set 'isInEditMode', true
      newAsset.set 'viewOptions', true

    addExpense: (type) ->
      newExpense = @get('model').addFinancial('expense', type)
      newExpense.set 'isInEditMode', true
      newExpense.set 'viewOptions', true

    addIncome: (type) ->
      newIncome = @get('model').addFinancial('income', type)
      newIncome.set 'isInEditMode', true
      newIncome.set 'viewOptions', true

    addLiability: (type) ->
      newLiability = @get('model').addFinancial('liability', type)
      newLiability.set 'status', 'current'
      newLiability.set 'isInEditMode', true
      newLiability.set 'viewOptions', true

    addSecurity: (type) ->
      newSecurity = @get('model').addFinancial('security', type)
      newSecurity.set('isProposed', false)
      newSecurity.set 'isInEditMode', true
      newSecurity.set 'viewOptions', true


)

`export default ViewsEntitiesEntityFinancialsCardComponent`
