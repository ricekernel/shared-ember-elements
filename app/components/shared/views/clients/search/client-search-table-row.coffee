`import Ember from 'ember'`

ViewsClientsClientSearchTableRowComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start hover-grey cursor-pointer']

  session: Ember.inject.service('session')


  showPhone: true
  showEmail: true

  isSelected: Ember.computed('model', 'clientArray.[]', ->
    return false unless @get('model') and @get('clientArray')
    return true if @get('clientArray').includes(@get('model'))
    )

  userViewIsSelf: Ember.computed.equal('session.currentUser.currentViewDescription', "Only My Data")
  showOwner: Ember.computed.not('userViewIsSelf')
  #showOwner: Ember.computed.or('session.currentUser.userViewIsFirm', 'session.currentUser.userViewIsMulti')


)

`export default ViewsClientsClientSearchTableRowComponent`
