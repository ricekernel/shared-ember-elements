`import Ember from 'ember'`

ViewsClientsClientSearchComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-justify-start flex-shrink-grow']
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')

  filteredClients: []
  filtering: false
  hasEmailFilter: false #if true, will only return clients that have a primary email
  clientArray: [] #used when multiSelecting clients
  multiSelect: false #if passed as true, user can select multiple clients, rather than just one
  newTag: ''
  possibleNewTagMatches: []
  q: null
  searchByName: true
  searchByTag: false
  searchTags: []
  tagSearchResults: []
  continueButtonTitle: 'continue'

  #these passed to search result table row:
  showEmail: true
  showPhone: true



#computeds:
  notSelected: Ember.computed.setDiff('sortedFilteredClients', 'clientArray')
  allSelected: Ember.computed.equal('notSelected.length', 0)

  multipleSearchResults: Ember.computed('sortedFilteredClients', ->
    if @get('sortedFilteredClients.length') > 1
      return true
  )

  multiSelectContinue: Ember.computed.and('multiSelect', 'clientArray') #if multiSelect and at least one client selected, shows 'continue' button


  showNoResults: Ember.computed('searchTags.[]', 'q', 'filtering', ->
    if @get('filtering') == false and (@get('searchTags.length') > 0 or @get('q.length') > 1)
      return true
    )



  showServerTagMatches: Ember.computed.notEmpty('possibleNewTagMatches')

  hideInstructions: Ember.computed('sortedFilteredClients', 'searchTags', 'showNoResults', ->
    return true if @get('showNoResults') or @get('sortedFilteredClients.length')
    )

  sortedFilteredClients: Ember.computed('searchByName', 'filteredClients.@each.lastName', 'tagSearchResults.@each.lastName', 'clientArray.[]', ->
    if @get('searchByName')
      resultsArray = @get 'filteredClients'
    else
      resultsArray = @get 'tagSearchResults'

    if @get('hasEmailFilter')
      resultsArray = resultsArray.filterBy('primaryEmail.content.contact')

    self = @
    filteredWithSelected = resultsArray.map((item) ->
      if self.get('clientArray').includes(item)
        item.set 'isSelected', true
      else
        item.set 'isSelected', false
      return item
    )
    filteredWithSelected.sortBy('lastName').filterBy('id')


    return filteredWithSelected
  )

  validNewTag: Ember.computed('newTag', ->
    if @get('newTag.length') > 2
      return true
    )


#observers:
  searchByNameFilterer: (->
    self = @
    searchString = @get 'q'
    if searchString.length > 1
      @set 'filtering', true
      Ember.run.debounce(this, 'searchByNameFilter', 900)
    else
      @set 'filtering', false
    ).observes 'q'

  serverTagAutocomplete: (->
    self = @
    if @get 'validNewTag'
      @set 'filtering', true
      Ember.run.debounce(this, 'searchForTag', 900)
    else
      @set 'possibleNewTagMatches', []
      @set 'filtering', false
    ).observes 'newTag'



  actions:
    addClient: ->
      @set 'addClient', true

    addTagToSearchArray: (tag) ->
      @get('searchTags').pushObject(tag)
      @set('newTag', '')
      @send('searchServerByTags')

    multiSelectContinue: ->
      #@set 'client', true #users of the tool are bound to the client. By setting to true, it changes their state
      clientArray = @get('clientArray')
      @sendAction('action', clientArray)
      @reset()

    removeTagFromSearchArray: (tag) ->
      @get('searchTags').removeObject(tag)
      @send('searchServerByTags')

    searchServerByTags: ->
      self = @
      currentUserId = @get('session.currentUser.id')
      searchTags = @get('searchTags')
      tagsString = searchTags.toString()
      @store.query('client', { filter: { '[tag_list]' : tagsString} }).then (matches) ->
        self.set('tagSearchResults', matches)


    selectClient: (client) ->
      if @get 'multiSelect'
        if @get('clientArray').includes(client)
          @get('clientArray').removeObject(client)
        else
          @get('clientArray').pushObject(client)
      else
        @sendAction('action', client)
        @reset()


    selectNewClient: (client) ->
      @set 'addClient', false
      @send('selectClient', client)
      @send('viewSelected')

    toggleSearchType: ->
      @toggleProperty 'searchByName'
      @toggleProperty 'searchByTag'
      return false

    toggleSelectAll: ->
      self = @
      if @get 'allSelected'
        @get('sortedFilteredClients').forEach (client) ->
          self.get('clientArray').removeObject(client)
      else
        @get('sortedFilteredClients').forEach (client) ->
          unless self.get('clientArray').includes(client)
            self.get('clientArray').pushObject(client)

    viewSelected: ->
      clientArray = @get('clientArray')
      @set 'filteredClients', clientArray


  init: ->
    @get('clientArray').clear()
    @set('filteredClients', @store.peekAll('client'))
    @_super()

  didInsertElement: ->
    @_super()
    Ember.run.later(this, ->
      $('[name="clientSearchInput"]').focus()
    , 400)

  #name search in response to search string change:
  searchByNameFilter: ->
    self = @
    searchString = @get 'q'
    if searchString.length > 1
      includesArray = ['primary_email', 'primary_phone'].toString()
      @store.query('client', {filter: {name: searchString}, include: includesArray }).then (filteredClients) ->
        #if self.get('hasEmailFilter')
          #filteredClients = filteredClients.filterBy('primaryEmail.content.contact')
        self.set 'filteredClients', filteredClients
        self.set 'filtering', false

  searchForTag: ->
    self = @
    newTag = @get('newTag')
    if @get 'validNewTag'
      @store.query('tag', {filter: { name: newTag } }).then (matches) ->
        #if self.get('filtering') == true #this prevents issues where an earlier call returns after a later one, changing the filteredclients
        self.set 'possibleNewTagMatches', matches
        self.set 'filtering', false



  reset: ->
    @setProperties(
      newTag: ''
      filteredClients: []
      filtering: false
      #sortedFilteredClients: []
      clientArray: [] #used when multiSelecting clients
      possibleNewTagMatches: []
      q: ''
      searchByName: true
      searchByTag: false
      searchTags: []
      tagSearchResults: []
    )
    return false

)

`export default ViewsClientsClientSearchComponent`
