`import Ember from 'ember'`

ViewsClientsClientSearchComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-justify-start flex-align-centre flex-shrink-grow']
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')


  actions:
    addClient: ->
      @set 'createClient', true

    searchClient: ->
      @set 'findClient', true
      return false

    addNewClient: (client)->
      clients = [client]
      @sendAction('action', clients)
      @set 'createClient', false
      @set 'backAttribute', false

    selectClients: (clients)->
      @sendAction('action', clients)
      @set 'findClient', false
      @set 'backAttribute', false

    
)

`export default ViewsClientsClientSearchComponent`
