`import Ember from 'ember'`

ViewsContactsContactsQuickAddContactComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24 flex-column-nowrap flex-justify-start flex-align-centre flex-shrink-grow']
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')
  checkForMatches: false


  showSaveButton: Ember.computed.alias('firstName')

  actions:
    clearForm: ->
      owner = @get('session.currentUser')

      @setProperties(
        firstName: ''
        preferredName: ''
        lastName: ''
        phone: ''
        email: ''
        owner: owner
        matchingClients: null
        alertMatches: false
      )
      return false


    saveNewContact: (overrideMatchCheck)->
      self = @
      checkForMatches = @get('checkForMatches')
      contactSaveArray = []
      @set('isSaving', true)
      unless @get('lastName')
        @set 'lastName', ' '

      if checkForMatches && !overrideMatchCheck
        self.util_SearchForClient().then ((clientMatches) ->
          if clientMatches.content.length > 0
            self.util_AlertUserToMatches(clientMatches)
          else
            self.util_SaveClientAndContacts()
          ), (error) ->
            self.saveError()
      else
        @set 'alertMatches', false
        @set 'matchingClients', null
        self.util_SaveClientAndContacts()



    selectExistingClient: (client) ->
      @returnClient(client)
      return false




  namify: (name) ->
    return null unless name
    return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase()

  returnClient: (client) ->
    if @get 'onAddClient'
      @attrs.onAddClient(client)

  saveError: ->
    notify = @get 'notify'
    notify.error 'Error, please try again'
    @set('isSaving', false)




  util_SearchForClient: ->
    self = @
    return new Ember.RSVP.Promise((resolve, reject) ->
      searchString = "#{self.get('firstName')} #{self.get('lastName')}"
      includesArray = ['primary_email', 'primary_phone'].toString()
      self.store.query('client', {filter: {name: searchString}, include: includesArray }).then ((filteredClients) ->
        self.set('matchingClients', filteredClients)
        resolve filteredClients
        ), (error) ->
          reject error
    )


  util_AlertUserToMatches: (clientMatches) ->
    @set('isSaving', false)
    @set 'alertMatches', true


  util_SaveClientAndContacts: ->
    self = @
    @set('isSaving', true)
    newClient = @store.createRecord('client',
      firstName: @namify(@get('firstName'))
      preferredName: @namify(@get('preferredName'))
      lastName: @namify(@get('lastName'))
      owner: @get('owner')
    )
    contactSaveArray = []
    if @get 'email'
      email = @get('email')
      newEmail = newClient.addContact(email, true)

    if @get('phone')
      phone = @get('phone')
      newPhone = newClient.addContact(phone, true)

    newClient.save().then ((client) ->

      self.set('isSaving', false)

      self.returnClient(newClient)
      self.send 'clearForm'
    ), (error) ->
      self.saveError()


  #function overrides:
  init: ->
    @_super()
    @send('clearForm')
)

`export default ViewsContactsContactsQuickAddContactComponent`
