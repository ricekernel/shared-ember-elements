`import Ember from 'ember'`

ViewsContactsContactsQuickAddContactComponent = Ember.Component.extend(

  classNames: ['s24 full-height flex-column-nowrap flex-align-centre flex-justify-space-between']
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')


  actions:
    selectClient: (client) ->
      @attrs.selectExistingClient(client)
      return false

    saveNewContact:  ->
      @attrs.saveNewContact(false)
      return false
)

`export default ViewsContactsContactsQuickAddContactComponent`
