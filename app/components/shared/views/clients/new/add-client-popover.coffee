`import Ember from 'ember'`

ViewsClientsAddClientPopoverComponent = Ember.Component.extend(

  session: Ember.inject.service('session')
  relatedClientArray: []

  actions:
    addClient: (client) ->
      @set('showClientAdded', true)
      @set('client', client)
      return false

    addRelatedClient: ->
      @set 'addRelated', true
      return false

    close: ->
      @set 'closeAttribute', false
      return false

    navigateToAdded: ->
      @set 'addRelated', false

    viewClient: ->
      clientId = @get('client')
      destinationRoute = "client.details"
      @get('router').transitionTo(destinationRoute, clientId)
      @set 'closeAttribute', false
      return false

)

`export default ViewsClientsAddClientPopoverComponent`
