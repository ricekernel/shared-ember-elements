`import Ember from 'ember'`
`import TableOptionsMixin from '../../../../mixins/shared/table-options-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableOptionsMixin,

  classNames: ['s24 flex-row-nowrap table-single-line']
  session: Ember.inject.service('session')

  showContacts: true
  showCollaborate: true

  closeAttributeObs: Ember.observer('model.showDetails', ->
    client = @get('model.content') || @get('model')
    if @get('loanParty')
      loanApplication = @get('loanParty.loanApplication')
      if client.get('showDetails') == false && client.get('hasDirtyAttributes')
        client.save().then ->
          loanApplication.save()
    )


  actions:
    callClient: ->
      @get('model.primaryPhone').then (phone) ->
        phoneNumber = phone.get('contact')
        window.open("tel:+#{phoneNumber}")
        return false

    deleteRecord: ->
      model = @get('model.content') || @get('model')
      loanParty = @get('loanParty')
      loanParty.removeClient(model)
      loanApplication = loanParty.get('loanApplication')
      loanApplication.save()


    invite: ->
      @set('showInvite', true)
      return false


)

`export default BorrowersIndividualTableRowComponent`
