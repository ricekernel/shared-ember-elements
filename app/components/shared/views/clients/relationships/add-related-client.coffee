`import Ember from 'ember'`

ViewsClientsRelationshipsAddRelatedClientComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24']
  relationship: null
  notify: Ember.inject.service('notify')


  showSaveButton: Ember.computed.and('thisClientType', 'linkedClientType')

  actions:
    createRelationship: ->
      self = @
      notify = @get 'notify'
      @set 'isSaving', true
      relationship = @get 'relationship'
      leftClient = @get('model')
      leftClientType = @get('relationship.left')
      rightClient = @get('linkedClient')
      rightClientType = @get('relationship.right')
      newRelationship = @store.createRecord('clientRelationship',
        leftClient: leftClient
        leftRelationshipType: leftClientType
        rightClient: rightClient
        rightRelationshipType: rightClientType
        relationshipType: relationship
      )
      #@get('model.clientRelationships').pushObject(newRelationship)
      newRelationship.save().then (->
        self.get('model.leftClientRelationships').pushObject(newRelationship)
        self.set 'isSaving', false
        #self.set 'closeAttribute', false
        notify.success 'Relationship created!'
        self.sendAction('action')

      ), (error) ->
        notify.error "Aww - no luck: #{error}"

    linkClient: (client) ->
      @set 'linkedClient', client
      @set 'selectRelationshipType', true


)

`export default ViewsClientsRelationshipsAddRelatedClientComponent`
