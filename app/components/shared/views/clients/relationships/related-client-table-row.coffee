`import Ember from 'ember'`
`import TableRowOperationsMixin from '../../../../../mixins/shared/table-row-operations-mixin'`

ViewsClientsRelationshipsRelatedClientTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  ###
  Pass the client context, and the component will work out the side of the relationship to show
  ###

  clientSide: 'left'
  relatedClientSide: 'right'
  tableExpanded: true


  thisClient: Ember.computed('model.leftClient', 'model.rightClient', 'clientSide',
    get: (key) ->
      clientAttribute = "model.#{@get('clientSide')}Client"
      @get(clientAttribute)

    set: (key, value) ->
      clientAttribute = "model.#{@get('clientSide')}Client"
      @set(clientAttribute, value)
      return value
    )


  relatedClient: Ember.computed('model.leftClient', 'model.rightClient', 'clientSide',
    get: (key) ->
      relatedClientAttribute = "model.#{@get('relatedClientSide')}Client"
      @get(relatedClientAttribute)

    set: (key, value) ->
      relatedClientAttribute = "model.#{@get('relatedClientSide')}Client"
      @set(relatedClientAttribute, value)
      return value
    )

  relatedClientType: Ember.computed('model.relationshipType', 'clientSide',
    get: (key) ->
      relatedClientTypeAttribute = "model.relationshipType.#{@get('relatedClientSide')}"
      @get(relatedClientTypeAttribute)

    set: (key, value) ->
      relatedClientTypeAttribute = "model.relationshipType.#{@get('relatedClientSide')}"
      @set(relatedClientTypeAttribute, value)
      return value
    )

  actions:
    viewClient: ->
      model = @get('relatedClient')
      router = @get('router')
      router.transitionTo('client.details', model)
      return false


  calculateRelatedClientSideOfRelationship: ->
    client = @get('client.content') || @get('client')
    rightClient =  @get('model.rightClient.content') || @get('model.rightClient')
    if rightClient == client
      @setProperties(
        clientSide: 'right'
        relatedClientSide: 'left'
      )

  init: ->
    @calculateRelatedClientSideOfRelationship()
    @_super()

  relationshipTypes: [
    'spouse/partner'
    'business partner'
    'trust'
    'trustee'
    'guarantor'
    'guarantee'
    'parent'
    'child'
  ]


)

`export default ViewsClientsRelationshipsRelatedClientTableRowComponent`
