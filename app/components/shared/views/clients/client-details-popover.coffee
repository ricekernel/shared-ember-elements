`import Ember from 'ember'`
`import {countries, countriesWithoutZip, countriesWithState} from 'ember-countries';`

ClientDetailsPopoverComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']
  session: Ember.inject.service('session')

  genders: Ember.computed.alias('model.genders')

  isPerson: Ember.computed('model.clientType', ->
    unless @get('model.clientType')
      return true
    unless @get('model.clientType') == 'Company' || @get('model.clientType') == 'Trust'
      return true
    )


  contacts: Ember.computed.mapBy('model.contacts', 'contactType.name')


  hasPhoneEmail: Ember.computed('contacts.[]', ->
    phonePresent = @get('contacts').contains('Phone')
    emailPresent = @get('contacts').contains('Email')

    if !phonePresent or !emailPresent
      return false
    else
      return true
    )


  individualsToCopyFrom: Ember.computed('loanApplication.clients.[]', ->
    clientsList = []
    model = @get('model')
    if @get('loanApplication.clients.length') > 0
      @get('loanApplication.clients').forEach (client) ->
        unless model == client
          clientsList.pushObject(client)
    return clientsList
    )

  notify: Ember.inject.service('notify')

  previousAddressRequired: Ember.computed('model.monthsAtCurrentAddress', ->
    if @model.get('monthsAtCurrentAddress') < 36
      return true
    )

  previousEmployerRequired: Ember.computed('model.monthsAtCurrentEmployer', ->
    if @model.get('monthsAtCurrentEmployer') < 36
      return true
    )

  actions:
    copyIndividual: (individual) ->
      notify = @get('notify')
      self = @
      copyArray = ['isResident', 'maritalStatus', 'livingArrangement', 'minorDependants', 'nonMinorDependants', 'monthsAtCurrentAddress', 'monthsAtPreviousAddress']
      copyArray.forEach (attr) ->
        attrValue = individual.get(attr)
        self.model.set(attr, attrValue)
      if individual.get('currentAddress')
        @set('model.currentAddress', individual.get('currentAddress'))
      if individual.get('previousAddress')
        @set('model.previousAddress', individual.get('previousAddress'))
      notify.success 'Copied!'


    delete: ->
      model = @get('model.content') || @get('model')
      loanParty = @get('loanParty')
      loanParty.removeClient(model)
      loanApplication = loanParty.get('loanApplication')
      loanApplication.save()


    save: ->
      self = @
      client = @get('model.content') || @get('model')
      loanApplication = @get('loanParty.loanApplication')
      @set('isSaving', true)
      client.save().then ->
        loanApplication.save()
        self.set('isSaving', false)
        client.set('showDetails', false)
      return false

    undo: ->
      client = @get('model.content') || @get('model')
      client.rollbackAttributes()
      return false

  init: ->
    countriesList = countries.mapBy('country')
    countriesList.pushObject(null)
    @set 'countriesList', countriesList
    model = @get('model')
    contactTypes = @store.peekAll('contactType')
    unless model.get('email')
      emailType = contactTypes.findBy('name', 'Email')
      email = @store.createRecord('contact'
        contactType: emailType
      )
      model.set('primaryEmail', email)
    unless model.get('phone')
      phoneType = contactTypes.findBy('name', 'Phone')
      phone = @store.createRecord('contact'
        contactType: phoneType
      )
      model.set('primaryPhone', phone)
    @_super()

)

`export default ClientDetailsPopoverComponent`
