`import Ember from 'ember'`

ViewsEntitiesEntityFinancialsCardComponent = Ember.Component.extend(

  classNames: ['grey-bordered-white-box-flat pad5']

  loans: Ember.computed('model.liabilities.[]', ->
    loans = @model.get('liabilities').filterBy('liabilityType.isSecured').filterBy('isProposed', false)
    loans.sortBy('liabilityType.name')
    return loans
    )

  defaultFrequency: Ember.computed('model.store.frequency.[]', ->
    @get('model.store').peekAll('frequency').get('firstObject')
    )

  liabilityTypes: Ember.computed('model.store.liabilityType.[]', ->
    @get('model.store').peekAll('liabilityType')
    )

  assetTypes: Ember.computed('model.store.assetType.[]', ->
    @get('model.store').peekAll('assetType')
    )

  securityTypes: Ember.computed('model.store.securityType.[]', ->
    @get('model.store').peekAll('securityType')
    )

  incomeTypes: Ember.computed('model.store.incomeType.[]', ->
    @get('model.store').peekAll('incomeType')
    )

  expenseTypes: Ember.computed('model.store.expenseType.[]', ->
    @get('model.store').peekAll('expenseType')
    )


  actions:
    addAsset: (type) ->
      newAsset = @get('model.store').createRecord('asset',
        assetType: type
        entity: @get('model')
        )

      #@get('model.assets').pushObject(newAsset)
      newAsset.set 'isInEditMode', true
      newAsset.save().then ()->
        #saving it so it has an ID to delete later
        newAsset.set 'viewOptions', true


    addLiability: (type) ->
      newLiability = @get('model.store').createRecord('liability',
        liabilityType: type
        entity: @get('model')
        )

      @get('model.liabilities').pushObject(newLiability)
      newLiability.set 'isInEditMode', true
      newLiability.save().then ()->
        #saving it so it has an ID to delete later
        newLiability.set 'viewOptions', true


    addIncome: (type) ->
      newIncome = @get('model.store').createRecord('income',
        incomeType: type
        entity: @get('model')
        frequency: @get('defaultFrequency')
        )

      @get('model.incomes').pushObject(newIncome)
      newIncome.set 'isInEditMode', true
      newIncome.save().then ()->
        #saving it so it has an ID to delete later
        newIncome.set 'viewOptions', true

    addExpense: (type) ->
      newExpense = @get('model.store').createRecord('expense',
        expenseType: type
        entity: @get('model')
        frequency: @get('defaultFrequency')
        )


      @get('model.expenses').pushObject(newExpense)
      newExpense.set 'isInEditMode', true
      newExpense.save().then ()->
        #saving it so it has an ID to delete later
        newExpense.set 'viewOptions', true

    addSecurity: (type) ->
      newSecurity = @get('model.store').createRecord('security',
        securityType: type
        entity: @get('model')
        isProposed: false
        )

      @get('model.securities').pushObject(newSecurity)
      newSecurity.set 'isInEditMode', true
      newSecurity.save().then ()->
        #saving it so it has an ID to delete later
        newSecurity.set 'viewOptions', true


)

`export default ViewsEntitiesEntityFinancialsCardComponent`
