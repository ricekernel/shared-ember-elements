`import Ember from 'ember'`

ViewsUserTemplatesUserTemplateSelectComponent = Ember.Component.extend(

  classNames: ['s23 shared-client-search-results-container flex-shrink-grow relative']

  actions:
    selectTemplate: (template) ->
      @sendAction('action', template)

  init: ->
    @_super()
    @set('model', @store.findAll('template'))
)

`export default ViewsUserTemplatesUserTemplateSelectComponent`
