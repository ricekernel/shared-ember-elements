`import Ember from 'ember'`
`import EmberUploader from 'ember-uploader'`
`import ENV from '../../../../config/environment'`



s3UploadComponent = EmberUploader.FileField.extend(
  url: "#{ENV.APP.ADAPTER_HOST}/api/v1/documents/sign"
  multiple: true
  uploadError: 'uploadError'

  session: Ember.inject.service('session')
  userToken:''

  newDocId: null

  uploadCount: 0


  filesDidChange: (files) ->

    uploadUrl = @get('url')
    self = this

    if !Ember.isEmpty(files)


      @attrs.uploadStarted()

      $.each files, (key, value) ->

        uploadCount = self.get('uploadCount')
        self.set('uploadCount', uploadCount++)

        thisFile = files[key]

        self.get('session').authorize('authorizer:oauth2', (header,token)->
          self.set('userToken', token)
          )
        token = self.get('userToken')



        uploader = EmberUploader.S3Uploader.extend({
          signingUrl: uploadUrl
          signingAjaxSettings:
            headers:
              'authorization': token
              'Accept': 'application/vnd.api+json'

          didSign: (response) ->
            newDocId = response['x-amz-meta-id']
            thisFile.id = newDocId

            @_super(response)
            return response

        }).create()


        uploader.on('progress', (e) ->
          self.attrs.uploadProgress(e.percent)
        )

        uploader.on('didUpload', (response) ->
          self.attrs.uploadComplete(response)
        )

        uploader.on('didError', (jqXHR, textStatus, errorThrown) ->
          self.attrs.uploadError(errorThrown)
        )


        newFile = uploader.upload(files[key])

        newFile.then (response) ->
          self.createNewDocument(thisFile, response, thisFile.id)





  checkUploadComplete: ->




  createNewDocument: (file, awsResponse, newDocId) ->
    unless @get 'uploadCancelled'
      self = @
      uploadedUrl = $(awsResponse).find('Location')[0].textContent
      #url = decodeURIComponent(uploadedUrl)
      #key = $(awsResponse).find('Key')[0].textContent
      #etag = $(awsResponse).find('ETag')[0].textContent

      docData = {
        data: {
          attributes: {
            name: file.name
            document_file_name: file.name
            document_content_type: file.type
          }
          id: newDocId
          type: 'documents'
        }
      }
      self.store.pushPayload(docData)

      documentInStore = self.store.peekRecord('document', newDocId)
      self.attrs.newDocumentReady(documentInStore)

      uploadComplete = self.get('uploadCount')
      if uploadComplete == 0
        self.attrs.uploadComplete()
      else
        uploadComplete--
        self.set('uploadComplete', uploadComplete)



)
`export default s3UploadComponent`
