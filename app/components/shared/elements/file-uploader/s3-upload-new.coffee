`import Ember from 'ember'`
`import EmberUploader from 'ember-uploader'`
`import ENV from '../../../../config/environment'`



s3UploadComponent = EmberUploader.FileField.extend(
  url: "#{ENV.APP.ADAPTER_HOST}/api/v1/documents/sign"
  multiple: true

  session: Ember.inject.service('session')
  userToken:''

  newDocId: null

  uploadCount: 0


  filesDidChange: (files) ->

    uploadUrl = @get('url')
    self = this

    if !Ember.isEmpty(files)

      $.each files, (key, value) ->
        thisFile = files[key]

        self.get('session').authorize('authorizer:oauth2', (header,token)->
          self.set('userToken', token)
          )
        token = self.get('userToken')



        uploader = EmberUploader.S3Uploader.extend({
          signingUrl: uploadUrl
          signingAjaxSettings:
            headers:
              'authorization': token
              'Accept': 'application/vnd.api+json'

          didSign: (response) ->
            newDocId = response['x-amz-meta-id']
            this.id = newDocId
            thisFile.id = newDocId
            self.createNewDocument(thisFile, response, newDocId)
            @_super(response)
            return response

        }).create()


        uploader.on('progress', (e) ->
          thisDoc = self.store.peekAll('document').findBy('id', uploader.id)
          thisDoc.set('uploadProgress', e.percent)
        )

        uploader.on('didUpload', (response) ->
          thisDoc = self.store.peekAll('document').findBy('id', uploader.id)
          thisDoc.set('uploadProgress', 100)
          self.attrs.uploadComplete(thisDoc)
        )


        uploader.on('didError', (jqXHR, textStatus, errorThrown) ->
          self.attrs.uploadError()
        )


        newFile = uploader.upload(files[key])



  createNewDocument: (file, awsResponse, newDocId) ->
    self = @
    docData = {
      data: {
        attributes: {
          name: file.name
          document_file_name: file.name
          document_content_type: file.type
        }
        id: newDocId
        type: 'documents'
      }
    }
    self.store.pushPayload(docData)

    documentInStore = self.store.peekRecord('document', newDocId)
    documentInStore.set('uploadProgress', 0)
    self.attrs.newDocumentCreated(documentInStore)



)
`export default s3UploadComponent`
