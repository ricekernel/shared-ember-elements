`import Ember from 'ember'`
`import EmberUploader from 'ember-uploader'`
`import ENV from '../../../../config/environment'`



s3UploadComponent = EmberUploader.FileField.extend(
  url: "#{ENV.APP.ADAPTER_HOST}/api/v1/documents/sign"
  multiple: true
  uploadError: 'uploadError'

  session: Ember.inject.service('session')
  userToken:''

  newDocId: null


  filesDidChange: (files) ->
    uploadUrl = @get('url')
    self = this
    @set('fileArray',[])
    @attrs.uploadStarted()

    @get('session').authorize('authorizer:oauth2', (header,token)->
      self.set('userToken', token)
      )
    token = self.get('userToken')

    uploader = EmberUploader.S3Uploader.extend({
      signingUrl: uploadUrl
      signingAjaxSettings:
        headers:
          'authorization': token
          'Accept': 'application/vnd.api+json'

      didSign: (response) ->
        newDocId = response['x-amz-meta-id']
        self.set('newDocId', newDocId)

        @_super(response)
        return response

    }).create()



    uploader.on('progress', (e) ->
      self.attrs.uploadProgress(e.percent)
    )

    uploader.on('didUpload', (response) ->
      self.attrs.uploadComplete(response)
    )

    uploader.on('didError', (jqXHR, textStatus, errorThrown) ->
      self.attrs.uploadError(errorThrown)
    )

    if !Ember.isEmpty(files)


      $.each files, (key, value) ->


        thisFile = files[key]
        newFile = uploader.upload(files[key])

        newFile.then (response) ->
          self.createNewDocument(thisFile, response, self.get('newDocId'))




  createNewDocument: (file, awsResponse, newDocId) ->
    unless @get 'uploadCancelled'
      self = @
      uploadedUrl = $(awsResponse).find('Location')[0].textContent
      #url = decodeURIComponent(uploadedUrl)
      #key = $(awsResponse).find('Key')[0].textContent
      #etag = $(awsResponse).find('ETag')[0].textContent

      docData = {
        data: {
          attributes: {
            name: file.name
            document_file_name: file.name
            document_content_type: file.type
          }
          id: newDocId
          type: 'documents'
        }
      }
      self.store.pushPayload(docData)

      documentInStore = self.store.peekRecord('document', newDocId)

      self.attrs.newDocumentReady(documentInStore)


)
`export default s3UploadComponent`
