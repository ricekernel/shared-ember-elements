`import Ember from 'ember'`

ElementsFileUploadComponent = Ember.Component.extend(

  ###
  options:
    newDocumentReady (action) - action to call once the doc is ready
  ###

  uploadId: Ember.computed(->
    uuid = 'xxxxxxxxyyy'.replace /[xy]/g, (c) ->
      r = Math.random()*16|0
      v = if c == 'x' then r else (r&0x3|0x8)
      v.toString(16)
    return 'S3Upload' + uuid
    )

  uploadPercent: null
  notify: Ember.inject.service('notify')

  provideNotifications: true

  actions:
    newDocumentCreated: (doc) ->
      if @attrs.newDocumentCreated
        @attrs.newDocumentCreated(doc)
      return false

    uploadComplete: (doc) ->
      if @attrs.uploadComplete
        @attrs.uploadComplete(doc)
      return false

    uploadError: ->
      if @attrs.uploadError
        @attrs.uploadError()
      return false


)

`export default ElementsFileUploadComponent`
