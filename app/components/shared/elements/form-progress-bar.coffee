`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementsFormProgressBarComponent = Ember.Component.extend(
  ###
  needs:
    progress (decimal)
  ###
  classNames: ['section-progress-indicator-container']
  progress: 0
  alert: false



  progressWidth: Ember.computed('progress', ->
    percentComplete = @get('progress')
    return new Ember.String.htmlSafe('width:' + Math.floor(percentComplete) + '%')
    )

  complete: Ember.computed('progress', ->
    if @get('progress') > 99
      return true
    )

)

`export default ElementsFormProgressBarComponent`
