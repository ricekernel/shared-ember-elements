`import Ember from 'ember'`

LoanTypeSelectComponent = Ember.Component.extend(

  classNames:['flex-row-nowrap']

  loanTypes: []

  class: 'svg-small'


  init: ->
    @_super()
    liabilityTypes = @store.peekAll('liabilityType')

    ['Floating home loan', 'Revolving credit'].forEach (type) ->
      thisType = liabilityTypes.findBy('name', type)
      typeFirstWord = type.split(' ')[0].toLowerCase()
      iconPath = "icons/home-loan-#{typeFirstWord}"
      typeObject = {
        name: type
        svgPath: iconPath
      }
      @get('loanTypes').pushObject(typeObject)


)

`export default LoanTypeSelectComponent`
