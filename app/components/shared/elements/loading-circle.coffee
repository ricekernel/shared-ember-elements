`import Ember from 'ember'`

LoadingCircleComponent = Ember.Component.extend(

  loading: true

  svgSize: 'svg-small'

  completeMessage: 'done!'

  svgClass: Ember.computed('svgSize', ->
    svgClass = @get('svgSize')
    return "loading #{svgClass}"
    )

)

`export default LoadingCircleComponent`
