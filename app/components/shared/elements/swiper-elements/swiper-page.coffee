`import Ember from 'ember'`

SwiperPageComponent = Ember.Component.extend(

  classNameBindings: [':swiper-page', 'selected', 'past']

  selected: Ember.computed('index', 'selectedIndex', ->
    if @get('index') == @get('selectedIndex')
      return true
    )

  past: Ember.computed('index', 'selectedIndex', ->
    if @get('index') < @get('selectedIndex')
      return true
    )

  #session: Ember.inject.service('session')


)

`export default SwiperPageComponent`
