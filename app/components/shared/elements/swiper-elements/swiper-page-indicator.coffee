`import Ember from 'ember'`

SwiperPageIndicatorComponent = Ember.Component.extend(

  classNames: ['swiper-footer']
  #session: Ember.inject.service('session')

  selectedIndex: 0

  actions:
    selectIndex: (index) ->
      @set('selectedIndex', index)
      return false

)

`export default SwiperPageIndicatorComponent`
