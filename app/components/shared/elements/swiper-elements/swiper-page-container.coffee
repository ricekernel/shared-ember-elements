`import Ember from 'ember'`

SwiperPageContainerComponent = Ember.Component.extend(

  classNames: ['swiper-page-container']
  #session: Ember.inject.service('session')

  selectedIndex: 0

  lastIndex: Ember.computed('model.[]', ->
    return @get('model.length') - 1
    )

  actions:
    navigate: (direction) ->
      selectedIndex = @get('selectedIndex')
      selectedIndex += direction
      @set('selectedIndex', selectedIndex)
      return false

)

`export default SwiperPageContainerComponent`
