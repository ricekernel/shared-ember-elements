`import Ember from 'ember'`

LoanTypeSelectComponent = Ember.Component.extend(

  classNames:['loan-type-select']

  loanTypes: []

  class: 'svg-small'

  thisType: Ember.computed('value', 'loanTypes.[]', ->
    value = @get('value')
    @get('loanTypes').findBy('value', value)
    )

  actions:
    selectType: (type) ->
      @set 'value', type.value
      @set 'showSelect', false
      return false

    toggleSelect: ->
      @toggleProperty('showSelect')
      return false


  init: ->
    @_super()
    liabilityTypes = @store.peekAll('liabilityType')
    self = @
    if @get('loanTypes.length') > 0
      return false
    ['Fixed home loan', 'Floating home loan', 'Revolving credit'].forEach (type) ->
      thisType = liabilityTypes.findBy('name', type)
      typeFirstWord = type.split(' ')[0].toLowerCase()
      iconPath = "icons/home-loan-#{typeFirstWord}"
      typeObject = {
        name: type
        svgPath: iconPath
        value: thisType
      }
      self.get('loanTypes').pushObject(typeObject)




)

`export default LoanTypeSelectComponent`
