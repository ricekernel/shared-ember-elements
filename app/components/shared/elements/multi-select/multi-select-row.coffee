`import Ember from 'ember'`


MultiSelectRowComponent = Ember.Component.extend(


  classNames: ['s24 flex-row-nowrap flex-align-centre hover-highlight cursor-pointer pad5']

  isSelected: Ember.computed('selectedArray.[]', 'model', ->
    model = @get('model.content') || @get('model')
    if @get('selectedArray').includes(model)
      return true
    )

  click: ->
    model = @get('model.content') || @get('model')
    selectedArray = @get('selectedArray')
    if selectedArray.includes(model)
      selectedArray.removeObject(model)
    else
      selectedArray.pushObject(model)
    return false

)

`export default MultiSelectRowComponent`
