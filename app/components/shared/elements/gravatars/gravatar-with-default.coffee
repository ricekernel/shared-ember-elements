`import Ember from 'ember'`

GravatarWithDefaultComponent = Ember.Component.extend(

  classNameBindings: [':relative', ':contact-avatar-container', 'size', 'menu', 'ghosted']

  displayName: Ember.computed('model.name', ->
    name = @get('model.name')
    firstName = name.split(' ')[0]
    return firstName
    )

  title: Ember.computed('model.name', ->
    if @get('model.name')
      return @get('model.name')
    else
      return "Unknown name"
    )

  isClient: false
  size: null
  menu: Ember.computed('inMenu', ->
    if @get 'inMenu'
      return 'in-menu'
    )
  ghosted: false

  userEmail: Ember.computed('model.email', ->
    if @get('isClient')
      if @get('model.primaryEmail.contact')
        return @get('model.primaryEmail.contact').toLowerCase()
    else
      unless @get('model.email')
        return ""
      return @get('model.email').toLowerCase()
    )

  defaultColour: Ember.computed('model.name', ->
    name = @get('model.name')
    unless name
      return new Ember.String.htmlSafe('background-color: #FGFGFG')

    if name.length == 0
      return new Ember.String.htmlSafe('background-color: #FGFGFG')

    return new Ember.String.htmlSafe('background-color: #2196f3')

    )


  initials: Ember.computed('model.name', ->

    name = @get('model.name')
    return '' unless name
    if name.includes(' ')
      nameArray = name.split(' ')
      nameArrayLast = nameArray.length - 1
      initials = nameArray[0].charAt(0) + nameArray[nameArrayLast].charAt(0)
    else
      initials = name.charAt(0)
    return initials
    )


)

`export default GravatarWithDefaultComponent`
