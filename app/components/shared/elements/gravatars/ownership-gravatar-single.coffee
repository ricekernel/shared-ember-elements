`import Ember from 'ember'`

OwnershipGravatarSingleComponent = Ember.Component.extend(

  classNames: ['selectable']
  notify:  Ember.inject.service('notify')

  notInArray: Ember.computed('model', 'selectedArray.[]', ->
    model = @get('model.content') || @get('model')
    unless @get('selectedArray').includes(model)
      return true
    )

  click: ->
    model = @get('model.content') || @get('model')
    sa = @get('selectedArray')
    if sa.includes(model)
      if sa.get('length') == 1
        notify = @get('notify')
        notify.error('You must have at least one owner!')
      else
        sa.removeObject(model)
    else
      sa.pushObject(model)

)

`export default OwnershipGravatarSingleComponent`
