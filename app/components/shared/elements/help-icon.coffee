`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`


HelpIconComponent = Ember.Component.extend(

  tagName: 'span'
  classNames: 'help-icon-container'

  click: ->
    @send('showHelpMessage')
    return false

)

`export default HelpIconComponent`
