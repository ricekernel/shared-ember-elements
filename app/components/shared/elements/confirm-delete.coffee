`import Ember from 'ember'`


ConfirmDeleteComponent = Ember.Component.extend(

  classNames: ['grey-out-bg-with-centred-content']

  actions:
    confirmDeletion: ->
      @attrs.confirmDelete()
      @set('closeAttribute', false)
      return false

    cancel: ->
      @set('closeAttribute', false)
      return false

)

`export default ConfirmDeleteComponent`
