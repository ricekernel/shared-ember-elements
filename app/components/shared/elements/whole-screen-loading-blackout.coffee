`import Ember from 'ember'`

WholeScreenLoadingBlackoutComponent = Ember.Component.extend(

  loading: true

  classNames: ['whole-screen-loading-blackout-container']

)

`export default WholeScreenLoadingBlackoutComponent`
