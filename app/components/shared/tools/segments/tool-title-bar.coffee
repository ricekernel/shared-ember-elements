`import Ember from 'ember'`

ToolsToolSegmentsToolTitleBarComponent = Ember.Component.extend(

  classNames: ['tools-header-title']

  actions:
    backAction: ->
      @set 'backAttribute', false

    close: ->
      @set 'closeAttribute', false
)

`export default ToolsToolSegmentsToolTitleBarComponent`
