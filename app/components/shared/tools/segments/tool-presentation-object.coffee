`import Ember from 'ember'`

ToolsToolSegmentsToolPresentationObjectComponent = Ember.Component.extend(

  #A container for the inside object of a presentation container. Content is shown just before
  # and removed just after it is shown

  classNameBindings: [':s24', ':presentation-object', ':white-bg', 'offRight:off-right']

  offRight: Ember.computed.not('show')

  showContent: false

  showObs: Ember.observer('show', ->
    if @get('show')
      @set 'showContent', true
    else
      Ember.run.later(this, 'hideContent' ,600)

    )

  hideContent: ->
    unless @get('show')
      @set 'showContent', false

)

`export default ToolsToolSegmentsToolPresentationObjectComponent`
