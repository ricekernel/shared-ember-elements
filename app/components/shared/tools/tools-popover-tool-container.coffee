`import Ember from 'ember'`

ToolsToolsPopoverToolContainerComponent = Ember.Component.extend(

  ###
  options:
  confirmClose: if true, clicking the fadeout will ask to confirm close of the popover

  ###

  classNames: ['tools-popover-outer-container']
  confirmClose: true
  cantClose: false

  click: (e) ->
    target = e.target
    unless @get('cantClose')
      if $(target).hasClass('tools-popover-outer-container')
        if @get('confirmClose')
          @set 'askToClose', true
        else
          @set 'closeAttribute', false

  actions:
    backAction: ->
      @sendAction 'backAction'

    cancelClose: ->
      @set 'askToClose', false

    close: ->
      @set 'closeAttribute', false

)

`export default ToolsToolsPopoverToolContainerComponent`
