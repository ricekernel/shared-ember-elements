`import Ember from 'ember'`
`import LoanCalculationsMixin from '../../../../mixins/shared/loan-calculations-mixin'`

ToolsToolsPopoverToolContainerComponent = Ember.Component.extend(LoanCalculationsMixin,

  ###
  needs:
    setRepayment (action)

  options:
    can pass in: loanAmount, rate, term, frequency, interestType

  ###

  classNames: ['tools-repayment-calculator-button-container']
  #confirmClose: true

  interestType: 'P&I'
  interestTypes: ['P&I', 'Int Only']

  repayment: Ember.computed('loanAmount', 'rate', 'term', 'frequency', 'interestType', ->
    loanAmount = parseFloat(@get('loanAmount'))
    rate = parseFloat(@get('rate'))
    term = parseFloat(@get('term'))
    paymentsPerYear = @get('frequency.yearConversion')

    if @get('interestType') == 'Int Only' then isInterestOnly = true else isInterestOnly = false

    return @calculateLoanRepayment(loanAmount, term, rate, paymentsPerYear, isInterestOnly)

    )

  actions:
    toggleCalculator: ->
      @toggleProperty('showCalculator')
      return false

    use: ->
      repayment = @get('repayment')
      @attrs.setRepayment(repayment)
      @set('showCalculator', false)
      return false



)

`export default ToolsToolsPopoverToolContainerComponent`
