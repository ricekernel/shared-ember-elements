`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

InputsElementColourIndicatorSelectorComponent = Ember.Component.extend(
  classNames: ['s24 flex-row-nowrap flex-align-centre flex-justify-centre cursor-pointer']

  displayColour: Ember.computed('value', 'optionsArray', 'colour', ->
    if @get('colour') then return Ember.String.htmlSafe("background-color: #{@get('colour')}")
    if @get('optionsArray')
      optionsArray = @get('optionsArray')
      value = @get('value')
      index = optionsArray.mapBy('value').indexOf(value)
      if index > -1 && optionsArray.objectAt( index )
        colour = optionsArray.objectAt( index ).colour
        return Ember.String.htmlSafe("background-color: " + colour)
      else return Ember.String.htmlSafe("background-color: #FFFFFF")
    else return Ember.String.htmlSafe("background-color: #FFFFFF")
    )


  click: (e) ->
    optionsArray = @get('optionsArray')
    if optionsArray
      value = @get('value')
      currentIndex = optionsArray.mapBy('value').indexOf(value)
      newIndex = currentIndex + 1
      if newIndex >= optionsArray.length
        newIndex = 0
      newValue = optionsArray.objectAt( newIndex ).value
      @set('value', newValue)
    return false



  actions:
    toggleValue: ->
      @toggleProperty('value')
      return false


  init: ->
    @_super()
    optionsArray = @get('optionsArray')
    value = @get('value')
    if optionsArray
      unless optionsArray.mapBy('value').indexOf(value) >= 0
        value = optionsArray.objectAt(0)
        @set('value', value)

  )


`export default InputsElementColourIndicatorSelectorComponent`
