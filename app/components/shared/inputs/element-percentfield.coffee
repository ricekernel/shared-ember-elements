`import Ember from 'ember'`
`import accounting from 'accounting'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementPercentfieldComponent = Ember.Component.extend(

  ###
  needs:
    value
  options:
    perAnnum [BOOL]: if 'true' adds 'p.a.' after % sign
  ###
  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  percentValue: Ember.computed('value',
    get: (key) ->
      return Math.round(@get('value')*10000)/100||null

    set: (key, value) ->
      val = Math.round(value* 100) / 10000
      @set('value', val)
      return value

    )


)

`export default ElementPercentfieldComponent`
