`import Ember from 'ember'`
`import accounting from 'accounting'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementCurrencyfieldComponent = Ember.Component.extend(


  ###
  An input component for currencies, that is a number input that formats to currency by placing a label over the input

  Needs:
    value(number)

  Options:
    placeholder (string)
    helpMessage (string): the popover help message
    showDecimals (bool): defaults to true. If set to false, numbers rounded to the nearest dollar.
  ###

  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  showDecimals: true
  hasFocus: false
  nullValue: Ember.computed.not('value')
  hideLabel: Ember.computed.or('hasFocus', 'nullValue')

  hasValue: Ember.computed('value', ->
    value = @get('value')
    return true if value?
    )

  inputClass: Ember.computed('hideLabel', ->
    if @get 'hideLabel'
      return 'element-input'
    else
      return 'element-input element-currency-field-hidden-element'
      )

  ###
  init: ->
    @_super()
    if @get('value') == 0
      @set 'value', '0'
      ###


  currencyFormattedValue: Ember.computed('value', ->
    value = @get 'value'
    if @get 'showDecimals'
      return accounting.formatMoney(value, '$', 2)
    else
      return accounting.formatMoney(value, '$', 0)
    )


  click: (e) ->
    @set 'hasFocus', true
    e.stopImmediatePropagation()

  focusIn: ->
    @set 'hasFocus', true

  focusOut: ->
    @_super()
    if @get('value') > 999999999.99
      @set('value', 999999999.99)
    @set 'hasFocus', false

)

`export default ElementCurrencyfieldComponent`
