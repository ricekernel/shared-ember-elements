`import Ember from 'ember'`

InputsElementIncrementerComponent = Ember.Component.extend(

  classNames: ['input-slider-container']
  #iconLocation: 0

  svgPath: 'icons/add-button'

  barId: Ember.computed('elementId', ->
    return "#{@get('elementId')}Bar"
  )


  valueObs: Ember.observer('value', ->
    @util_MoveIconForValue()
    )

  willDestroyElement: ->
    @_super()
    $(window).off('resize.handleResize')

  didRender: ->
    @_super()
    $(window).on('resize.handleResize', Ember.run.bind(this, @util_ResizeWindow))
    unless @get('rendered')
      #delaying this stops the didRender deprecation firing:
      Ember.run.later(this, 'util_MoveIconForValue', 400)
      @set 'rendered', true


  actions:
    dragGhost: (xPlaneLocation)->
      unless xPlaneLocation <= 0
        @util_CalculateValueForLocation(xPlaneLocation)


    clickMove: ->
      barID = @get('barId')
      value = parseFloat(@get('value'))
      basis = parseFloat(@get('basis'))
      eventX = event.clientX - $('#' + barID).offset().left

      iconLocation = @get 'iconLocation'
      if eventX < iconLocation
        @set 'value', value - basis
      if eventX > iconLocation + 25
        @set 'value', value + basis



  util_CalculateValueForLocation: (xPlaneLocation) ->
    barID = @get('barId')
    minValue = @get('min')
    maxValue = @get('max')
    basis = @get('basis')
    minPoint = $('#' + barID).offset().left

    if xPlaneLocation < minPoint
      @set('value', minValue)
      return false
    if xPlaneLocation > (minPoint + $('#' + barID).width() )
      @set('value', maxValue)
      return false
    else
      valPerPx = @util_CalculateValPerPxOnBar()
      xPlaneLocationPastMin = xPlaneLocation - minPoint
      value = (minValue + (valPerPx * xPlaneLocationPastMin))
      basedValue = (Math.round(value/basis)) * basis
      @set('value', basedValue)
      return false



  util_CalculateValPerPxOnBar: ->
    #run this each time in case it gets resized
    barID = @get('barId')
    minValue = @get('min')
    maxValue = @get('max')
    range = maxValue - minValue
    valPerPx = range / $('#' + barID).width()
    return valPerPx


  util_MoveIconForValue: ->
    barID = @get('barId')
    value = @get('value')
    minValue = @get('min')
    maxValue = @get('max')
    minPoint = $('#' + barID).offset().left
    valPerPx = @util_CalculateValPerPxOnBar()
    iconLocation = ((value - minValue) / valPerPx)
    if iconLocation < 0
      iconLocation = 0
    if iconLocation > $('#' + barID).width()
      iconLocation = $('#' + barID).width()
    @set('iconLocation', iconLocation)

  util_ResizeWindow: ->
    Ember.run.debounce(this, 'util_MoveIconForValue', 400)

)

`export default InputsElementIncrementerComponent`
