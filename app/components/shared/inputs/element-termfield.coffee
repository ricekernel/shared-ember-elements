`import Ember from 'ember'`
`import accounting from 'accounting'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementTermfieldComponent = Ember.Component.extend(
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  yrValue: Ember.computed('value',
    get: (key) ->
      Math.floor(@get('value') / 12) || null


    set: (key, value) ->
      mths = @get('mthValue')
      @set('value', accounting.unformat(value)*12 + mths)
      return value
    )

  mthValue: Ember.computed('value',
    get: (key) ->
      @get('value') %12 || null# - @get('yrValue') * 12 || null

    set: (key, value) ->
      yrs = @get('yrValue') * 12
      newVal = accounting.unformat(value) + yrs
      @set('value', newVal) #accounting.unformat(value) + yrs)

      return value %12

    )


  ### Code from before Ember 2.2.0 update:

  yrValue: ((key, value, previousValue) ->
    if arguments.length > 1
      mths = @get('mthValue')
      @set('value', accounting.unformat(value)*12 + mths)
    Math.floor(@get('value') / 12) || null
  ).property('value', 'mthValue')

  mthValue: ((key, value, previousValue) ->
    if arguments.length > 1
      #yrs = @get('yrValue') * 12
      newVal = @get('value') + (value - previousValue)
      @set('value', newVal) #accounting.unformat(value) + yrs)
    @get('value') %12 || null# - @get('yrValue') * 12 || null
  ).property('value')

  ###

)

`export default ElementTermfieldComponent`
