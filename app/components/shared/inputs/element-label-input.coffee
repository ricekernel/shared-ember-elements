`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementLabelInputComponent = Ember.Component.extend(


  ###
  An input component for currencies, that is a number input that formats to currency by placing a label over the input

  Needs:
    value(number)

  Options:
    placeholder (string)
    helpMessage (string): the popover help message
    showDecimals (bool): defaults to true. If set to false, numbers rounded to the nearest dollar.
  ###

  tagName: 'div'

  click: (e) ->
    @set 'isInEditMode', true
    e.stopImmediatePropagation()

  focusIn: ->
    @set 'isInEditMode', true

  focusOut: ->
    @_super()
    @set 'isInEditMode', false

)

`export default ElementLabelInputComponent`
