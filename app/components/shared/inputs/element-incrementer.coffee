`import Ember from 'ember'`

InputsElementIncrementerComponent = Ember.Component.extend(

  classNames: ['input-container']

  svgPath: 'icons/add-button'
  #basis: [1, 10, 100]
  basis1: 1
  basis2: 10
  basis3: 100

  value: 1
  xMovement: 0


  actions:
    decreaseValue: (basis) ->
      value = @get 'value'
      #basis = @get 'basis'
      newValue = value - basis
      @set 'value', newValue

    increaseValue: (basis) ->
      value = @get 'value'
      #basis = @get 'basis'
      newValue = value + basis
      @set 'value', newValue

  init: ->
    @_super()
    if @get('isTerm')
      @setProperties(
        basis1: 1
        basis2: 12
        basis3: 60
      )
    else
      basis = @get('basis')
      @setProperties(
        basis1: basis
        basis2: basis * 10
        basis3: basis * 100
      )


  mouseDown: (event) ->
    console.log event
  ###
  mouseDown: (event) ->
    @set 'mouseIsDown', true
    distanceFromIcon = @getEventPositionRelativeToIcon event
    @set 'xMovement', distanceFromIcon
    #mouseDownInterval = setInterval(@adjustValue(), 150)
    #@set 'mouseDownInterval', mouseDownInterval


  mouseMove: (event) ->
    if @get('mouseIsDown')
      distanceFromIcon = @getEventPositionRelativeToIcon event
      @set 'xMovement', distanceFromIcon

  mouseUp: (event) ->
    @set 'xMovement', 0
    @set 'mouseIsDown', false
    #mouseDownInterval = @get 'mouseDownInterval'
    #clearInterval(mouseDownInterval)
  ###

  getEventPositionRelativeToIcon: (event) ->
    containerId = @get('elementId')
    containerLeft = $('#' + containerId).offset().left
    containerWidth = $('#' + containerId).width()
    iconCentre = containerLeft + (containerWidth/2)
    distance = event.originalEvent.clientX - iconCentre

    if distance > (containerWidth/2)
      distance = (containerWidth/2)
    if distance < (0 - (containerWidth/2))
      distance = 0 - (containerWidth/2)
    return distance

  adjustValue: ->
    console.log 'adjust'

)

`export default InputsElementIncrementerComponent`
