`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementSelectComponent = Ember.Component.extend(
  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']


  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )
  optionsList: []

  tabIndex: Ember.computed('tabIndex', ->
    if @get('noTab') then return "-1" else return null
    )


  init: ->
    self = @
    listName = @get('listName')

    if @get('optionsList.length') == 0 && listName
      listFromStore = @store.peekAll(listName)
      if listFromStore.get('firstObject.sortOrder')
        sortedListFromStore = listFromStore.sortBy('sortOrder')
        @set 'optionsList', sortedListFromStore
      else
        @set 'optionsList', listFromStore
    Ember.run.once(this, 'modifyList')
    @_super()



  listObs: Ember.observer('value', 'optionsList.[]', 'optionsList', ->
    Ember.run.once(this, 'modifyList')
    )


  modifyList: ->
    valueID = @get('value.id')
    if @get('optionsList.length') > 0
      list = @get('optionsList').map((item) -> #create a new object to represent the item, so we can give it a unique property for this select (isSelected)
        optionItem = {
          id: item.get('id')
          name: item.get('name')
          isSelected: item.get('id') == valueID
        }

        return optionItem
      )
    else
      list = []
    @set('list', list)
  ###
  list: Ember.computed('value', 'optionsList.[]', ->
    valueID = @get('value.id')
    if @get('optionsList.length') > 0
      list = @get('optionsList').map((item) -> #create a new object to represent the item, so we can give it a unique property for this select (isSelected)
        optionItem = {
          id: item.get('id')
          name: item.get('name')
          isSelected: item.get('id') == valueID
        }

        return optionItem
      )
    else
      list = []
    return list
  )
  ###


  change: (e)-> #get the id of the item selected, find the original item that represents, and set the value of the select to it (which will run the value computed again)
    indexSelected = e.target.selectedIndex
    itemSelected = @get('list')[indexSelected]
    idSelected = itemSelected.id
    @set('value', @get('optionsList').findBy('id', idSelected))


)


`export default ElementSelectComponent`
