`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`


ElementTextareaComponent = Ember.Component.extend(
  ###
  requires:
    value

  optional:
    useTemplateFields(bool): will allow curly-brace entry of template-fields
    templateFieldContext (array): an array of text names of the baseModels for template-fields supported
  ###

  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  useTemplateFields: false

  templateFieldContextObs: Ember.observer('templateFieldContext', ->
    if @get('useTemplateFields')
      @setUsableTemplateFields()
    )

  didInsertElement: ->
    @_super()
    if @get('useTemplateFields')
      @setUsableTemplateFields()


  setUsableTemplateFields: ->
    termsArray = @store.peekAll('templateField').sortBy('name')

    if @get('templateFieldContext')
      templateFieldContext = @get('templateFieldContext')
      filteredTermsArray = termsArray.filter (term) ->
        return true if templateFieldContext.includes(term.get('baseModel'))
      termsArray = filteredTermsArray
    terms = termsArray.mapBy('name')

    @$().children('.element-textarea').textcomplete('destroy')
    @$().children('.element-textarea').textcomplete [
      {
        match: /\B([{]{1,2})(\w*)$/
        search: (term, callback)->
          callback $.map terms, (word)->
            if word.indexOf(term) == 0 then word else null
        replace: (word)->
          "{{#{word}}} "
      }
    ]

)

`export default ElementTextareaComponent`
