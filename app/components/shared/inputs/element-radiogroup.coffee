`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementRadiogroupComponent = Ember.Component.extend(

  tagName: 'div'
  classNames: ['input-container']


  actions:
    selectValue: (value) ->
      unless @get('isStrings')
        @get('listItems').forEach (item) ->
          item.set 'isValue', false
        value.set 'isValue', true
      @set 'value', value



)

`export default ElementRadiogroupComponent`
