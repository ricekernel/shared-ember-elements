`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementSegmentbuttonComponent = Ember.Component.extend(
  classNames: ['input-container']
  tagName: 'div'

  init: ->
    self = @
    objectArray = []
    @get('listItems').forEach (item) ->
      selected = false
      if self.get('value') == item
        selected = true
      itemObject = {
        selected: selected
        name: item
      }
      objectArray.pushObject(itemObject)
    @set 'optionsList', objectArray
    @_super()




  actions:
    selectSegment: (segment) ->
      @set 'value', segment.name

      @get('optionsList').forEach (item) ->
        Ember.set(item, 'selected', false)
      Ember.set(segment, 'selected', true)


)

`export default ElementSegmentbuttonComponent`
