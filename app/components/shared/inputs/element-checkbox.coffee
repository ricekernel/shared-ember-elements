`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementCheckboxComponent = Ember.Component.extend(
  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )
  actions:
    toggleCheck: ->
      this.$('.checkCard').toggleClass 'flipped'
      @toggleProperty 'isChecked'
)
`export default ElementCheckboxComponent`
