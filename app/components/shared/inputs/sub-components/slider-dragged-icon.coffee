`import Ember from 'ember'`

InputsSubComponentsSliderDraggedIconComponent = Ember.Component.extend(
  classNames: ['element-slider-icon', 'ease-in-out']
  attributeBindings: ['style']

  iconLocation: 0

  style: Ember.computed('iconLocation', ->
    left = @get('iconLocation') + 'px'
    return Ember.String.htmlSafe("left:#{left}")
  )

  didRender: ->
    @_super()
    unless @get('rendered')
      Ember.run.later(this, 'util_RemoveEaser', 1200)
      @set 'rendered', true



  util_RemoveEaser: ->
    thisElementId = @get('elementId')
    $('#' + thisElementId).removeClass('ease-in-out')



)

`export default InputsSubComponentsSliderDraggedIconComponent`
