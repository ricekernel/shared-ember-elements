`import Ember from 'ember'`

InputsSubComponentsIncrementerDragObjectComponent = Ember.Component.extend(

  classNames: ['element-increment-icon drag-ghost']
  attributeBindings: [ 'draggable' ]
  draggable: 'true'

  xOrigin: 0


  dragStart: (event) ->
    containerID = @get 'dragContainer.elementId'
    @set 'maxDragLength', $('#' + containerID).width()/2
    @set 'xOrigin', event.originalEvent.clientX


  drag: (event) ->
    xPlaneLocation = event.originalEvent.clientX
    origin = @get 'xOrigin'
    maxDragLength = @get 'maxDragLength'
    xMovement = xPlaneLocation - origin
    @set 'draggedProportion', xMovement / maxDragLength

    unless Math.abs(xMovement) > maxDragLength
      @set 'xMovement', xMovement

    Ember.run.throttle(this, 'adjustValue', 150)




  dragEnd: (event) ->
    @set 'xMovement', 0


  adjustValue:  ->
    value = @get 'value'
    basis = @get 'basis'
    isTerm = @get 'isTerm'
    test = isTerm? 12 : 10

    draggedProportion = @get 'draggedProportion'
    if Math.abs(draggedProportion) > 0 then multiplier = 1
    if Math.abs(draggedProportion) > 0.33 then multiplier = isTerm ? 12 : 10
    if Math.abs(draggedProportion) > 0.66 then multiplier = isTerm ? 120 : 100

    adjustment = basis * multiplier
    if draggedProportion < 0
      adjustment = adjustment * -1

    @set 'value', value + adjustment

)

`export default InputsSubComponentsIncrementerDragObjectComponent`
