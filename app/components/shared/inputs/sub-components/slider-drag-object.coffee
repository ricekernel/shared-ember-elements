`import Ember from 'ember'`

InputsSubComponentsSliderDragObjectComponent = Ember.Component.extend(

  classNames: ['element-slider-icon drag-ghost']
  attributeBindings: [ 'draggable', 'style' ]
  draggable: 'true'

  style: Ember.computed('left', ->
    left = @get('left') + 'px'
    return Ember.String.htmlSafe("left:#{left}")
  )



  touchMove: (e) ->
    @dragOrTouchMove(e)

  drag: (e) ->
    @dragOrTouchMove(e)

  dragOrTouchMove: (event) ->
    if event.originalEvent.touches
      xPlaneLocation = event.originalEvent.touches[0].clientX
    else
      xPlaneLocation = event.originalEvent.clientX
    @attrs.dragged(xPlaneLocation)
    return false

)

`export default InputsSubComponentsSliderDragObjectComponent`
