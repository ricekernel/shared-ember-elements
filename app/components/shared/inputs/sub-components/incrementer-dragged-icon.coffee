`import Ember from 'ember'`

InputsSubComponentsIncrementerDraggedIconComponent = Ember.Component.extend(

  classNames: ['element-increment-icon']
  attributeBindings: ['style']

  style: Ember.computed('xMovement', ->
    adjustment = @get('xMovement') + 'px'
    return Ember.String.htmlSafe("transform:translateX(#{adjustment})")
  )
)

`export default InputsSubComponentsIncrementerDraggedIconComponent`
