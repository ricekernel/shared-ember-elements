`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`


InputsElementNumberPlusMinusComponent = Ember.Component.extend(

  ###
  A component that displays an integer with a plus and minus button to add or subtract

  Needs:
    value (integer): the value to display, add, subtract

  Optional:
    helpMessage (string): the message to display when help messages are on
  ###

  tagName: 'div'
  classNames: ['input-container element-number-plus-minus-component-class']

  init: ->
    unless @get 'value'
      @set 'value', 0
    @_super(arguments...)

  actions:
    add: ->
      @set 'value', @get('value') + 1
      return false

    subtract: ->
      unless @get('value') == 0
        @set 'value', @get('value') - 1
      return false

)

`export default InputsElementNumberPlusMinusComponent`
