`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

AssembliesAddressInputWithLookupComponent = Ember.Component.extend(
  ###
  needs:
    address (if address is null, component will create one)
    placeholder (eg 'current address')
  ###
  classNames: ['address-entry-container']
  notify: Ember.inject.service('notify')

  actions:
    setAddress: (address) ->
      if address.get('content')
        address = address.get('content')
      @set 'isDirty', false
      #replace address each time
      newAddress = @store.createRecord('address',
        street: address.get('street')
        suburb: address.get('suburb')
        city: address.get('city')
        postCode: address.get('postCode')
        country: address.get('country')
        )
      @set 'address', newAddress
      newAddress.save()
      @set 'showAddressList', false




    setGoogleAddress: (address) ->
      @set 'isDirty', false
      self = @
      request = placeId: address.placeID

      callback = (place, status) ->
        if status == google.maps.places.PlacesServiceStatus.OK
          newAddress = self.store.createRecord('address')

          googleTermMatches = {
            subpremise: 'streetNumberPrefix'
            street_number: 'streetNumber'
            route: 'streetName'
            sublocality: 'suburb'
            locality: 'city'
            country: 'country'
            postal_code: 'postCode'
          }
          addressComponents = place.address_components
          addressComponents.forEach (component) ->
            Object.keys(googleTermMatches).forEach (key) ->
              if component.types.includes(key)
                addressAttribute = googleTermMatches[key]
                newAddress.set(addressAttribute, component.long_name)
          numberPrefix = newAddress.get('streetNumberPrefix') + '/'
          number = newAddress.get('streetNumber') + ' '
          street = newAddress.get('streetName')
          streetAddress = ''
          [numberPrefix, number, street].forEach (element) ->
            unless element == 'undefined ' or element == 'undefined/' or !element
              if element.length > 1
                streetAddress += element

          newAddress.set 'street', streetAddress
          self.set 'address', newAddress
          self.set 'showAddressList', false
          newAddress.save()
        else
          self.get('notify').error('We are having trouble saving addresses at present. Please continue with your application while we turn dials and push buttons at HQ')


      node = $('#googleAddressContainerID').get(0)
      service = new (google.maps.places.PlacesService)(node)
      service.getDetails {placeId: address.placeID}, callback




  addressString: Ember.computed('address.street', 'address.suburb', 'address.city', 'address.country',
    get: (key) ->
      address = @get 'address'
      if address.get 'content'
        address = address.get('content')
      addressArray = []
      ['street', 'suburb', 'city', 'country'].forEach (addressSegment) ->
        if address.get(addressSegment)
          addressArray.pushObject("#{address.get(addressSegment)}")

      if addressArray.length > 0
        return addressArray.join(', ')
      else
        return null

    set: (key, value) ->
      @set 'isDirty', true
      Ember.run.debounce(this, 'getAddressSuggestions', 400)
      return value
    )


  focusOut: ->
    @_super()
    @set 'showAddressList', false
    Ember.run.later(this, 'saveAddressChanges', 400)


  getAddressSuggestions: ->
    addressString = @get('addressString').toLowerCase()
    return false if addressString == 'tbc' or addressString == 'tba'

    loadedAddresses = @store.peekAll('address').filter (address) ->
      if address.get('street')
        address.get('street').substring(0, addressString.length) == addressString
    if loadedAddresses.length > 0
      @set 'loadedAddresses', loadedAddresses
    else
      @set 'loadedAddresses', null

    if addressString.length > 2
      @googlePlacesSuggestions()

    if loadedAddresses.length > 0
      @set 'showAddressList', true


  googlePlacesSuggestions: ->
    self = @
    notify = @get 'notify'
    searchString = @get 'addressString'
    displaySuggestions = (predictions, status) ->
      if status != google.maps.places.PlacesServiceStatus.OK
        unless status == 'ZERO_RESULTS'
          self.get('notify').error "We're having some issues with our address lookup. Please record the address, separated by commas."
        return

      googleAddresses = []
      predictions.forEach (prediction) ->
        thisPrediction = {
          placeID: prediction.place_id
          description: prediction.description
        }
        googleAddresses.pushObject(thisPrediction)
      if googleAddresses.length > 0
        self.set 'googleAddresses', googleAddresses
        self.set 'showAddressList', true
      else
        self.set 'googleAddresses', null

    service = new google.maps.places.AutocompleteService()

    location = new google.maps.LatLng(-42, 174)

    request = {
      input: searchString
      location: location
      radius: 1000
    }
    service.getPlacePredictions(request, displaySuggestions)
    return

  saveAddressChanges: ->
    @set 'showAddressList', false
    if @get 'isDirty'
      addressString = @get('addressString').split(', ')
      nzArray = [' nz', 'nz', ' new zealand', 'new zealand']
      if @get 'address.street'
        address = @get('address')
      else
        address = @store.createRecord('address')
        @set 'address', address
      #address = @get('address')
      #if address.get('content')
        #address = address.get('content')
      address.set 'street', addressString[0]
      if addressString[1]
        address.set 'suburb', addressString[1]
      if addressString.length > 3
        address.set 'city', addressString[2]
        address.set 'country', addressString[addressString.length-1]
      else
        if addressString[2]
          if nzArray.includes(addressString[2].toLowerCase())
            address.set 'country', 'New Zealand'
          else
            address.set 'city', addressString[2]
      if address.get('content')
        address = address.get('content')
      address.save()


)

`export default AssembliesAddressInputWithLookupComponent`
