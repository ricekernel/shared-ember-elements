`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

InputsElementTickboxComponent = Ember.Component.extend(
  classNameBindings: [':element-tick-box-container', 'noPlaceholder:no-placeholder']

  

  actions:
    toggleValue: ->
      @toggleProperty('value')
      return false

  )


`export default InputsElementTickboxComponent`
