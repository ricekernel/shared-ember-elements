`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

InputsElementStringSelectComponent = Ember.Component.extend(
  #tagName: 'div'
  ###
  A dropdown for listing and selecting from strings
  Needs:
    selectOptions (array): a list of options
    selectedValue (string): the value the selected item is/will be

  Options:
    placeholder (string)
  ###
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )
  #classNames: ['element-input element-string-select-container']

  value: Ember.computed.alias('selectedValue')

  )

`export default InputsElementStringSelectComponent`
