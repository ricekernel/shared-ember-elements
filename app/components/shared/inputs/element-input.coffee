`import Ember from 'ember'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`


ElementInputComponent = Ember.Component.extend(
  tagName: 'div'
  classNameBindings: [':input-container', 'noPlaceholder:no-placeholder']

  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  hasValue: Ember.computed('value', ->
    if @get('value') || @get('value') == 0
      return true
    )

  inputClasses: Ember.computed('centre', 'hideBorder', ->
    className = 'element-input'
    if @get('centre')
      className += ' text-centre'
    if @get('rightAlign')
      className += ' text-end'
    unless @get('hideBorder')
      className += ' has-border'
    if @get('noPlaceholder')
      className += ' no-placeholder'
    return className
    )




  keyPress: (e) ->
    if e.charCode == 13 && @attrs.onEnterKey
      @attrs.onEnterKey()
    return unless @get('inputType') == 'password'
    s = String.fromCharCode e.which
    if s.toUpperCase() == s and s.toLowerCase() != s and !e.shiftKey
      @sendAction 'capsLock'

)

`export default ElementInputComponent`
