`import Ember from 'ember'`

ViewsFinancialsFinancialTableComponent = Ember.Component.extend(

  classNames: ['s24', 'pad5', 'flex-justify-centre', 'text-centre', 'font-three-quarter']


  actions:
    addItem: (type) ->
      item = @get('type')
      if type
        @sendAction('action', item, type)
      else
        @sendAction('action', item)

    toggleTable: ->
      @toggleProperty('showTable')
      return false


  init: ->
    if @get('type')
      modelType = @get('type')
      #inflector = new Ember.Inflector(Ember.Inflector.defaultRules)
      #financials = inflector.pluralize(financial)

      modelTypes = modelType + 'Type'
      #@set 'title', financials.charAt(0).toUpperCase() + financials.slice(1)
      @set 'optionTypes', @store.peekAll(modelTypes)
    @_super()


)

`export default ViewsFinancialsFinancialTableComponent`
