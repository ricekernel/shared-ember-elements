`import Ember from 'ember'`

ViewsFinancialsFinancialTableComponent = Ember.Component.extend(

  ###
  A table container with a header, add 'type' button and possible title.  Set the rows in the yield

  Attrs:

    colourClass: for the Header BG
    title (string): the title for the header
    model (array): to show the number if desired
    type: eg 'asset'.  Will display assetTypes in the add button
    action: The action to call when adding an item

  Options:
    canToggle: let table expand/contract
    showRecordCount: display a number before the title
    showTable: whether table starts expanded contracted
    totalValue: if provided, displayed as a currency to top right of table
  ###
  classNames: ['s24']

  showTable: true
  showRecordCount: true
  canToggle: true

  recordCount: Ember.computed('model.length', 'showRecordCount', ->
    if @get('showRecordCount')
      return @get('model.length')
    else
      return null
    )



  actions:
    addItem: (type) ->
      item = @get('type')
      if type
        @sendAction('action', item, type)
      else
        @sendAction('action', item)

    toggleTable: ->
      @toggleProperty('showTable')
      return false


  init: ->
    if @get('type')
      modelType = @get('type')
      #inflector = new Ember.Inflector(Ember.Inflector.defaultRules)
      #financials = inflector.pluralize(financial)

      modelTypes = modelType + 'Type'
      #@set 'title', financials.charAt(0).toUpperCase() + financials.slice(1)
      @set 'optionTypes', @store.peekAll(modelTypes)
    @_super()


)

`export default ViewsFinancialsFinancialTableComponent`
