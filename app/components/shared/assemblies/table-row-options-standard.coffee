`import Ember from 'ember'`

ButtonsElementToggleButtonComponent = Ember.Component.extend(
  session: Ember.inject.service('session')

  classNames: ['relative']

  hasUndoButton: false


  actions:
    deleteRecord: ->
      if @get('mustConfirmDelete')
        @set('showConfirmDelete', true)
      else
        @attrs.deleteRecord()
      return false

    confirmDelete: ->
      @attrs.deleteRecord()
      return false

    undoChanges: ->
      @attrs.undoChanges()
      return false

    viewDetails: ->
      @attrs.viewDetails()
      return false


)

`export default ButtonsElementToggleButtonComponent`
