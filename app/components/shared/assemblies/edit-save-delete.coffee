`import Ember from 'ember'`

EditSaveDeleteComponent = Ember.Component.extend(
  #pass model and modelName : using these to find item in store so that we can get around dramas where some models are a promise and others are not
  classNames: ['edit-save-delete-container']
  inEditMode: false
  cantDelete: false
  notify: Ember.inject.service('notify')



  init: ->
    @_super(arguments...)
    if @get('model.content')
      @set 'record', @get 'model.content'
    else
      @set 'record', @get 'model'


  actions:
    editItem: ->
      @get('model').set('isInEditMode', true)

    saveItem: ->
      record = @get('record')
      record.set 'isInSaveMode', true
      @toSaveOrNotToSave().then ->
        record.set 'isInSaveMode', false
        record.set 'isInEditMode', false
        record.set 'viewOptions', false

    removeItem: ->
      record = @get('record')
      unless record.get('deleting')
        record.set 'deleting', true
        record.set 'isInEditMode', false
        record.set 'viewOptions', false
        if record.get('overrideDelete') #if the model has an override, use it.  This may include deleting links, etc
          record.overrideDelete()
        else
          record.destroyRecord()
          #@toDeleteOrDestroy()

  toSaveOrNotToSave: ->
    self = @
    notify = @get('notify')
    return new Ember.RSVP.Promise((resolve, reject) ->
      record = self.get('record')
      if record.get('overrideSave')
        record.overrideSave().then ->
          resolve record
      else
        record.save().then (->
          resolve record
        ), (error) ->
          notify.error 'There was an error when saving. The error was: ' + error
    )

  toDeleteOrDestroy: ->
    self = @
    notify = @get('notify')
    return new Ember.RSVP.Promise((resolve, reject) ->
      record = self.get('record')

      if record.get('isNew')
        record.deleteRecord()
        resolve record
      else
        record.destroyRecord().then (->
          resolve record
        ), (error) ->
          notify.error 'We encountered a problem: ' + error
    )
)

`export default EditSaveDeleteComponent`
