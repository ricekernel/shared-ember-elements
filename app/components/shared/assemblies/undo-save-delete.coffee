`import Ember from 'ember'`

EditSaveDeleteComponent = Ember.Component.extend(
  #pass model and modelName : using these to find item in store so that we can get around dramas where some models are a promise and others are not
  classNames: ['flex-row-nowrap']

  cantDelete: false


  actions:
    undo: ->
      @attrs.undo()
      return false

    save: ->
      @attrs.save()
      return false

    delete: ->
      @attrs.delete()
      return false

)

`export default EditSaveDeleteComponent`
