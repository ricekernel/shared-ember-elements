`import Ember from 'ember'`
`import accounting from 'accounting'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

PercetWithSupertextComponent = Ember.Component.extend(
  ###
  needs: value
  optional:
    currency (bool): (formats currency)
    percent (bool): (formats percentage)
    term (bool): formats a term count (months to yrs)
    date (bool): (formats date)
    placeholder=[string] (uses a standard input placeholder)
    helpMessage=[string] (for popovers)
    inputSize=[BOOL] (adds classes to match other input elements)
    style =['alert' :red text, 'success': green text, 'bold' : bold text]
  ###

  #classNames:[]
  classNameBindings: ['inputSize:input-container', 'noPlaceholder:no-placeholder', 'centre:text-centre', 'centre:flex-justify-centre', 'noPlaceholder:no-placeholder']

  inputSize: false
  centre: false


  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  style: ''

  displayValue: Ember.computed('value', ->
    return null unless @get('value')
    value = parseFloat(@get('value'))
    displayValue = (value * 100).toFixed(2)
    return displayValue
    )


)

`export default PercetWithSupertextComponent`
