`import Ember from 'ember'`
`import accounting from 'accounting'`
`import HelpMessagesPopoverMixin from '../../../mixins/shared/help-messages-popover-mixin'`

ElementsLabelWithPlaceholderOptionComponent = Ember.Component.extend(
  ###
  needs: value
  optional:
    currency (bool): (formats currency)
    percent (bool): (formats percentage)
    term (bool): formats a term count (months to yrs)
    date (bool): (formats date)
    placeholder=[string] (uses a standard input placeholder)
    helpMessage=[string] (for popovers)
    inputSize=[BOOL] (adds classes to match other input elements)
    style =['alert' :red text, 'success': green text, 'bold' : bold text]
  ###

  #classNames:[]
  classNameBindings: ['inputSize:input-container', 'noPlaceholder:no-placeholder', 'centre:text-centre', 'centre:flex-justify-centre', 'noPlaceholder:no-placeholder']

  inputSize: false
  centre: false


  noPlaceholder: Ember.computed('placeholder', ->
    unless @get('placeholder')
      return true
    )

  style: ''

  displayValue: Ember.computed('value', ->
    #return null unless @get('value')
    value = @get 'value'
    if @get('isEnum') && @get('value')
      value = value.replace(/_/g, ' ')
    if @get('currency')
      value = accounting.formatMoney(value, '$', 2)
    if @get('percent')
      value = (value * 100).toFixed(2) + '%'
    if @get('date')
      if value
        value = moment(value).format('DD MMM YY')
      else
        value = ''
    if @get('term')
      months = parseFloat(value) %12
      if months
        value = Math.floor(value / 12) + 'yrs ' + months + 'mths'
      else
        value = Math.floor(value / 12) + 'yrs'
    else
      return value
    )

  ###
  didReceiveAttrs: ->
    @_super()
    if @get('inputSize')
      @get('classNames').pushObject('input-container')
    if @get('centre')
      @get('classNames').pushObject('text-centre')
      @get('classNames').pushObject('flex-justify-centre')
      ###


)

`export default ElementsLabelWithPlaceholderOptionComponent`
