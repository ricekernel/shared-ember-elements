`import Ember from 'ember'`

TutorialReleasesPopoverComponent = Ember.Component.extend(

  classNames: ['tutorial-release-component']
  session: Ember.inject.service('session')

  tutorialRun: Ember.computed.alias('appLogin.tutorialRun')
  latestReleaseNotified: Ember.computed.alias('appLogin.latestReleaseNotified')
  selectedIndex: 0


  lastIndex: Ember.computed('model.demoPages.[]', ->
    return @get('model.demoPages.length') - 1
    )

  model: Ember.computed('tutorialRun', 'tutorial', 'latestReleaseNotified', ->
    if @get('registerTutorialRun')
      if @get('appLogin') && !@get('tutorialRun')
        return @get('tutorial')
      else
        #return @get('introModel')
        return null
    else
      return @get('tutorial')
      ###
      app = @get('appLogin.app')
      @store.query('release', {after: 12, app: app}).then (releases) ->
        releases.forEach (release) ->
          console.log release
          return null
          ###
          #run through releases, get the top 5 highest priority items and display them
          #set latestReleaseNotified to highest here
    )

  actions:
    close: ->
      if @get('registerTutorialRun')
        user = @get('appLogin.user')
        unless @get('tutorialRun')
          @set('tutorialRun', true)
        ###
        else
          newLatestReleaseNotified = @get('model').sortBy('release.id').get('lastObject')
          @set('latestReleaseNotified', newLatestReleaseNotified)
          ###
        user.save()
      else
        @set('closeAttribute', false)


)

`export default TutorialReleasesPopoverComponent`
