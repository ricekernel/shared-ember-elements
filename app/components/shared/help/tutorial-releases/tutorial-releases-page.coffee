`import Ember from 'ember'`

TutorialReleasesPageComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap']

  selected: Ember.computed('index', 'selectedIndex', ->
    if @get('index') == @get('selectedIndex')
      return true
    )

  past: Ember.computed('index', 'selectedIndex', ->
    if @get('index') < @get('selectedIndex')
      return true
    )

  actions:
    viewVideo: ->
      videoPath = @get('model.videoPath')
      window.open(videoPath)
      return false
  #session: Ember.inject.service('session')


)

`export default TutorialReleasesPageComponent`
