`import Ember from 'ember'`

ButtonsSvgCircleButtonComponent = Ember.Component.extend(

  ###
  A circle button (no border by default) that displays an svg icon, with options for a border and label

  Needs:
    iconName (string): the name of the icon in 'icons/'
    action: the action to perform when clicked

  Options:
    hasBorder (bool): adds a 2px circle border to button.  Black by default. Add altClasses to change colour
    altClasses (string): additional classes.  Use to change colour of border
    label (string): displays under the button
    title (string): title on hover
    loading (attr): link a loading attr to replace inner icon with loading circles while loading
    displayLabel (bool): used in block form. Yield is the label (for complex labels)
    smaller (bool):used if the icon looks too big in the circle
    hideUntilValid (bool): scale to 0 and disable until valid (for form completion) REQUIRES: unhideAttribute
    unhideAttribute: the validation value to unhide (above)
    addButton (bool): displays a plus sub-icon
    editButton (bool): displays a pencil sub-icon
    removeButton (bool): displays a minus sub-icon
    saveButton (bool): displays a disk sub-icon
  ###

  smaller: false
  altClasses: null
  hasBorder: false
  hideUntilValid: false
  unhideAttribute: null
  iconName: null
  tagName: 'button'

  #classNames: ['svg-circle-button']
  classNameBindings: [':svg-circle-button', 'altClasses', 'hasBorder', 'hide', 'showLabel:show-label', 'large:large', 'largest:largest']

  attributeBindings: ['title']

  iconPath: Ember.computed('iconName', ->
    return 'icons/' + @get('iconName')
    )

  hide: Ember.computed('hideUntilValid', 'unhideAttribute', ->
    return false unless @get('hideUntilValid')
    return true unless @get('unhideAttribute')
    )
  showLabel: Ember.computed.or('label', 'displayLabel')


  iconClass: Ember.computed('smaller', ->
    if @get('smaller')
      return "svg-circle-button-icon smaller"
    if @get('large')
      return "svg-circle-button-icon large"
    if @get('largest')
      return "svg-circle-button-icon largest"
    else
      return "svg-circle-button-icon"
    )


  subIconClass: Ember.computed('large', ->
    if @get('largest')
      return 'svg-circle-button-add-circle largest'
    else
      return 'svg-circle-button-add-circle'
    )



  click: ->
    unless @get 'loading'
      if @get('onClick')
        @get('onClick')()
      else
        return true

  ###
  init: ->
    if @get('hasBorder')
      unless @get('classNames').includes('has-border')
        @get('classNames').pushObject('has-border')
    if @get('altClasses')
      altClasses = @get('altClasses')
      unless @get('classNames').includes(altClasses)
        @get('classNames').pushObject(altClasses)
    @_super()
    ###

)

`export default ButtonsSvgCircleButtonComponent`
