`import Ember from 'ember'`

ButtonsMasterDetailBackButtonComponent = Ember.Component.extend(

  classNames: ['tools-back-button flex-row-nowrap flex-align-centre']

)

`export default ButtonsMasterDetailBackButtonComponent`
