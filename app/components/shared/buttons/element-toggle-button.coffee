`import Ember from 'ember'`

ButtonsElementToggleButtonComponent = Ember.Component.extend(
  session: Ember.inject.service('session')
  classNames: ['.toggle-button-outer-container']
  class: null
  toggle: false

  mouseEnter: (e)->
    self = @
    if @get('helpMessage') and @get('session.helpMessagesOn')
      @get('tooltip').show()


  mouseLeave: ->
    if @get('tooltip')
      @get('tooltip').hide()

  init: ->
    @get('classNames').pushObject(@get('class'))
    @_super(arguments...)

  actions:
    toggleToggle: ->
      #@toggleProperty('toggle')
      @sendAction('action', true)

)

`export default ButtonsElementToggleButtonComponent`
