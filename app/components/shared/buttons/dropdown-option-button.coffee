`import Ember from 'ember'`

ButtonsSvgCircleButtonComponent = Ember.Component.extend(

  ###
  A dropdown button container that can be a svg-circle-button, a square-button, or a link


  Needs:
    options (array): list of models with a 'name' attribute
    optionSelected (action): the action to perform with the selected option
    buttonLabel (string): the label to show on the button
    type (string): 'svg', 'button', 'link'

  Options:
    For SVG:
      iconName (string): the name of the icon in 'icons/'
      hasBorder (bool): adds a 2px circle border to button.  Black by default. Add altClasses to change colour
      altClasses (string): additional classes.  Use to change colour of border
      displayLabel (bool): used in block form. Yield is the label (for complex labels)

    For SquareButton:
      altClasses (string): additional classes.  Use to change colour of border

    For all:
    title (string): title on hover
    loading (attr): link a loading attr to replace inner icon with loading circles while loading

  ###


  classNames: ['dropdown-button-container']
  #classNameBindings: [':svg-circle-button', 'altClasses', 'hasBorder', 'hide', 'showLabel:show-label', 'large:large']

  attributeBindings: ['title']

  linkLike: Ember.computed.equal('type', 'link')
  svgButton: Ember.computed.equal('type', 'svg')
  squareButton: Ember.computed.equal('type', 'button')

  type: 'button'

  iconPath: Ember.computed('iconName', ->
    return 'icons/' + @get('iconName')
    )


  actions:
    viewOptions: ->
      @toggleProperty('showOptions')
      return false

    selectOption: (option) ->
      @attrs.optionSelected(option)
      @set('showOptions', false)
      return false
)

`export default ButtonsSvgCircleButtonComponent`
