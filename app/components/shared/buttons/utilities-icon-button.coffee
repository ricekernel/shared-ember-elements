`import Ember from 'ember'`

UtilitiesButtonComponent = Ember.Component.extend(

  classNameBindings: [':utilities-button-container', ':hover-highlight', 'enlarged']

  showUtilities: false

  actions:
    click: (button) ->
      action = button.action
      @attrs[action]()

    cancel: ->
      @set 'showConfirmDelete', false

    toggleUtilities: ->
      @toggleProperty('showUtilities')
      return false


  removeDeleteMode: ->
    if @get('inDeleteMode') && @get('live')
      @set 'inDeleteMode', false

  willDestroyElement: ->
    @_super()
    @set('live', false)




)

`export default UtilitiesButtonComponent`
