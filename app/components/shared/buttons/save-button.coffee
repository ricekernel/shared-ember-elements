`import Ember from 'ember'`

ElementSaveButtonComponent = Ember.Component.extend(

  actions:
    save: ->
      @attrs.save()
      return false
)

`export default ElementSaveButtonComponent`
