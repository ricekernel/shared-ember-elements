`import Ember from 'ember'`

HelpButtonComponent = Ember.Component.extend(


  actions:
    displayHelp: ->
      helpModel = @get('dummyHelp')
      @set('model', helpModel)
      @set('showHelp', true)
      return false


  dummyHelp: {
    title: "Submitting the application to lenders"
    description: "The 'Approvals & Pricing' section is where you can submit each loan application to various lenders.  Socket will compile the loan application and related documents for you"
    imagePath: "https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/la/0.0.2+-+Intro+Tutorials/loan-app-approval-new.png"
    videoPath: "test"
  }


)

`export default HelpButtonComponent`
