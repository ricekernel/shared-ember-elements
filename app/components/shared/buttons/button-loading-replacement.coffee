`import Ember from 'ember'`

ElementsButtonLoadingReplacementComponent = Ember.Component.extend(
  ###
  Use as a button that will show a button frame with loading circles in place when loading

  Needs:
  loadingAttribute (attr): an attribute that is true while loading and false once done (in the function called)
  label (string): to show on button
  customClass: such as 'accent-darker-bg', for the button
  alternateClass: for the button frame
  ###
  classNames: ['relative']

  action:
    buttonClicked: ->
      @send('action')


)

`export default ElementsButtonLoadingReplacementComponent`
