`import Ember from 'ember'`

ElementSaveButtonComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-nowrap flex-justify-centre']

  loanTypes: Ember.computed('value', ->
    loanTypes = []
    value = @get('value')
    @store.peekAll('liabilityType').filterBy('isSecured').forEach (type) ->
      if value == type then selected = true else selected = false
      typeFirstWord = type.get('name').split(' ')[0].toLowerCase()
      typeObj = {
        name: typeFirstWord
        type: type
        selected: selected
        iconPath: "icons/home-loan-#{typeFirstWord}"
      }
      loanTypes.pushObject(typeObj)
    return loanTypes
    )



  actions:
    select: (type) ->
      @set('value', type)
      return false


)

`export default ElementSaveButtonComponent`
