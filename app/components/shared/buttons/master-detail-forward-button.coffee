`import Ember from 'ember'`

ButtonsMasterDetailForwardButtonComponent = Ember.Component.extend(

  classNames: ['master-detail-fwd-button flex-row-nowrap flex-align-centre']

  click: ->
    @attrs.onClick()
    return false

)

`export default ButtonsMasterDetailForwardButtonComponent`
