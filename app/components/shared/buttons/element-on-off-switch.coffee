`import Ember from 'ember'`

ButtonsElementOnOffSwitchComponent = Ember.Component.extend(
  ###
    Requires:
      value (obvious)

    Options:
      trueValue & falseValue: will display a label to the side based on switch on/off
      disabled (bool): prevents interaction and makes opacity 0.25
  ###

  classNames:['buttons-on-off-outer-container']
  value: false
  trueValue: null
  falseValue: null

  toggleLabel: Ember.computed('value', 'trueValue', 'falseValue', ->
    if @get('value') and @get ('trueValue')
      return @get('trueValue')
    else
      return @get('falseValue')
      )

  actions:
    toggle: ->
      unless @get('disabled')
        @toggleProperty('value')
        return false

    click: ->
      return false


)

`export default ButtonsElementOnOffSwitchComponent`
