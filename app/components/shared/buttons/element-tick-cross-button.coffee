`import Ember from 'ember'`

ButtonsElementToggleButtonComponent = Ember.Component.extend(
  session: Ember.inject.service('session')
  classNameBindings: [':svg-circle-button', 'class']
  class: null
  toggle: false

  click: (e) ->
    @toggleProperty('isTick')
    return true

)

`export default ButtonsElementToggleButtonComponent`
