`import Ember from 'ember'`

AddToTableTypeSelectorComponent = Ember.Component.extend(
  #Pass to component: bgColor (so can match table), optionTypes (list of options to display),
  addTableItemMenuInView: false
  bgColour: null

  actions:
    toggleAddTableItemTypeMenu: ->
      unless @get('addTableItemMenuInView') #if we are opening this menu, check others and close any that are already open
        $('.type-selector-add-button').each ->
          if $(this).hasClass('is-open')
            $(this).click()
      @toggleProperty('addTableItemMenuInView')
      return false

    addItem: (type) ->
      @sendAction('action', type)
      @set 'addTableItemMenuInView', false



)

`export default AddToTableTypeSelectorComponent`
