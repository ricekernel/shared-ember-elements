`import Ember from 'ember'`

ElementSaveButtonComponent = Ember.Component.extend(

  actions:
    save: ->
      @sendAction()
)

`export default ElementSaveButtonComponent`
