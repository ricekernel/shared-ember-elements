`import Ember from 'ember'`

SwiperHomeNextButtonsComponent = Ember.Component.extend(
  #pass the current and previous section
  #TODO: create an array of each section in order, and pass this to components instead

  actions:
    nextSection: ->
      dockButton = @get('nextSection') + 'Dock'
      $('#' + dockButton).click()

    previousSection: ->
      dockButton = @get('previousSection') + 'Dock'
      $('#' + dockButton).click()

)

`export default SwiperHomeNextButtonsComponent`
