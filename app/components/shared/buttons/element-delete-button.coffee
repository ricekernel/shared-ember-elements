`import Ember from 'ember'`

ElementDeleteButtonComponent = Ember.Component.extend(

  inDeleteMode: false
  deleting: false
  live: true
  showConfirmDelete: false
  requireDeleteConfirm: false

  actions:
    cancel: ->
      @set 'showConfirmDelete', false

    delete: ->
      self = this
      if @get('inDeleteMode') or @get('showConfirmDelete')
        @set 'deleting', true
        @sendAction()
      else
        if @get('requireDeleteConfirm')
          @set 'showConfirmDelete', true
        else
          @set 'inDeleteMode', true
          Ember.run.later(this, 'removeDeleteMode', 5000)
      return false

  removeDeleteMode: ->
    if @get('inDeleteMode') && @get('live')
      @set 'inDeleteMode', false

  willDestroyElement: ->
    @_super()
    @set('live', false)




)

`export default ElementDeleteButtonComponent`
