`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'square-start', 'Integration | Component | square start', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{square-start}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#square-start}}
      template block text
    {{/square-start}}
  """

  assert.equal @$().text().trim(), 'template block text'
