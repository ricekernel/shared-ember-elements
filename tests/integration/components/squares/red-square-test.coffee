`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'squares/red-square', 'Integration | Component | squares/red square', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{squares/red-square}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#squares/red-square}}
      template block text
    {{/squares/red-square}}
  """

  assert.equal @$().text().trim(), 'template block text'
