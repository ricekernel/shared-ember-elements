`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'squares/role-square', 'Integration | Component | squares/role square', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{squares/role-square}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#squares/role-square}}
      template block text
    {{/squares/role-square}}
  """

  assert.equal @$().text().trim(), 'template block text'
