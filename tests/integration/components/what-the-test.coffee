`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'what-the', 'Integration | Component | what the', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{what-the}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#what-the}}
      template block text
    {{/what-the}}
  """

  assert.equal @$().text().trim(), 'template block text'
