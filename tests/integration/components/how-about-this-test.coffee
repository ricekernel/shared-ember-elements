`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'how-about-this', 'Integration | Component | how about this', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{how-about-this}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#how-about-this}}
      template block text
    {{/how-about-this}}
  """

  assert.equal @$().text().trim(), 'template block text'
