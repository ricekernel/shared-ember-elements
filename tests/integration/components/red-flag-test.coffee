`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'red-flag', 'Integration | Component | red flag', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{red-flag}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#red-flag}}
      template block text
    {{/red-flag}}
  """

  assert.equal @$().text().trim(), 'template block text'
