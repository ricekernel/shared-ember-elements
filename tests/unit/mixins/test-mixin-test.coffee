`import Ember from 'ember'`
`import TestMixinMixin from '../../../mixins/test-mixin'`
`import { module, test } from 'qunit'`

module 'Unit | Mixin | test mixin'

# Replace this with your real tests.
test 'it works', (assert) ->
  TestMixinObject = Ember.Object.extend TestMixinMixin
  subject = TestMixinObject.create()
  assert.ok subject
